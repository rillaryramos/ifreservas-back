package com.ifnmg.ifreserva.api.controller;

import com.ifnmg.ifreserva.api.event.RecursoCriadoEvent;
import com.ifnmg.ifreserva.api.exceptionhandler.IfreservaExceptionHandler.Erro;
import com.ifnmg.ifreserva.api.model.ReservaDependencia;
import com.ifnmg.ifreserva.api.model.ReservaDependenciaRequest;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.repository.ReservaDependenciaRepository;
import com.ifnmg.ifreserva.api.service.ReservaDependenciaService;
import com.ifnmg.ifreserva.api.service.UsuarioService;
import com.ifnmg.ifreserva.api.service.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/reserva_dependencia")
public class ReservaDependenciaController {

    @Autowired
    private ReservaDependenciaRepository reservaDependenciaRepository;
    @Autowired
    private ReservaDependenciaService reservaDependenciaService;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private MessageSource messageSource;

    @PostMapping("/uso_breve")
    private ResponseEntity<ReservaDependencia> salvarReservaUsoBreve(@Valid @RequestBody ReservaDependencia reservaDependencia, HttpServletResponse response, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaDependencia reservaSalva = reservaDependenciaService.salvarReservaUsoBreve(reservaDependencia, usuarioLogado.get());
        publisher.publishEvent(new RecursoCriadoEvent(this, response, reservaSalva.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(reservaSalva);
    }

    @PostMapping("/uso_breve/more_one")
    private ResponseEntity<ReservaDependenciaRequest> salvarReservaUsoBreveLista(@Valid @RequestBody ReservaDependenciaRequest reservaDependencia, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaDependenciaRequest reservaSalva = reservaDependenciaService.salvarVariasReservasUsoBreve(reservaDependencia, usuarioLogado.get());
        return ResponseEntity.status(HttpStatus.CREATED).body(reservaSalva);
    }

    @PostMapping("/uso_continuo")
    private ResponseEntity<ReservaDependencia> salvarReservaUsoContinuo(@Valid @RequestBody ReservaDependencia reservaDependencia, HttpServletResponse response, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaDependencia reservaSalva = reservaDependenciaService.salvarReservaUsoContinuo(reservaDependencia, usuarioLogado.get(), true, true, true, true);
        publisher.publishEvent(new RecursoCriadoEvent(this, response, reservaSalva.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(reservaSalva);
    }

    @PutMapping("/uso_breve/{id}")
    public ResponseEntity<ReservaDependencia> atualizarReservaUsoBreve(@PathVariable Long id, @Valid @RequestBody ReservaDependencia reservaDependencia, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaDependencia reservaSalva = reservaDependenciaService.atualizarReservaUsoBreve(id, reservaDependencia, usuarioLogado.get());
        return ResponseEntity.ok(reservaSalva);
    }

    @PutMapping("/uso_continuo/{id}")
    public ResponseEntity<ReservaDependencia> atualizarReservaUsoContinuo(@PathVariable Long id, @Valid @RequestBody ReservaDependencia reservaDependencia, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaDependencia reservaSalva = reservaDependenciaService.atualizarReservaUsoContinuo(id, reservaDependencia, usuarioLogado.get());
        return ResponseEntity.ok(reservaSalva);
    }

    @GetMapping("/dependencia/{id}")
    public ResponseEntity<List<ReservaDependencia>> listarReservasPorDependencia(@PathVariable Long id, @RequestParam String tipo, OAuth2Authentication authentication){
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        List<ReservaDependencia> reservas = reservaDependenciaService.listarReservasPorDependencia(id, usuarioLogado.get(), tipo);
        return ResponseEntity.ok(reservas);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReservaDependencia> buscarPeloId(@PathVariable Long id) {
        ReservaDependencia reservaDependencia = reservaDependenciaRepository.findOne(id);
        return reservaDependencia != null ? ResponseEntity.ok(reservaDependencia) : ResponseEntity.notFound().build();
    }

    @PutMapping("/cancelar/{id}")
    public ResponseEntity<ReservaDependencia> cancelarReserva(@PathVariable Long id, String inicio, String fim, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        LocalDateTime dataInicial = null;
        LocalDateTime dataFinal = null;
        if (!inicio.equals("null")) {
//            inicio = inicio.replace("T", " ");
            dataInicial = LocalDateTime.parse(inicio, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
        if (!fim.equals("null")) {
//            fim = fim.replace("T", " ");
            dataFinal = LocalDateTime.parse(fim, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
        ReservaDependencia reservaDependencia = reservaDependenciaService.cancelarReserva(id, usuarioLogado.get(), dataInicial, false, dataFinal, null, null);
        return ResponseEntity.ok(reservaDependencia);
    }

    @GetMapping("/relatorio")
    public ResponseEntity<byte[]> relatorioReservas(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate inicio,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fim,
            @RequestParam String idDependencia,
            @RequestParam String idUsuario,
            @RequestParam String idTurma,
            @RequestParam String tipo) throws Exception {
        System.out.println("ID dependencia: " + idDependencia);
        Long dependencia, usuario, turma;
        String tipoReserva;
        dependencia = !idDependencia.equals("null") ? Long.parseLong(idDependencia) : null;
        usuario = !idUsuario.equals("null") ? Long.parseLong(idUsuario) : null;
        turma = !idTurma.equals("null") ? Long.parseLong(idTurma) : null;
        tipoReserva = !tipo.equals("null") ? tipo : null;
        byte[] relatorio = reservaDependenciaService.relatorioDependencia(inicio, fim, dependencia, usuario, turma, tipoReserva);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
                .body(relatorio);
    }

    @ExceptionHandler({ SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException.class })
    public ResponseEntity<Object> handleSolicitacaoReservaDependenciaComCompatibilidadeDeHorario(SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException ex) {
        return new ResponseEntity<>(ex.reservas, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({ DatasDiferentesParaReservaDeUsoBreveException.class })
    public ResponseEntity<Object> handleDatasDiferentesParaReservaDeUsoBreve(DatasDiferentesParaReservaDeUsoBreveException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.datas-diferentes", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ DataHorarioTerminoMenorQueDataHorarioInicioException.class })
    public ResponseEntity<Object> handleDataHorarioTerminoMenorQueDataHorarioInicio(DataHorarioTerminoMenorQueDataHorarioInicioException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.horario-termino-anterior-horario-inicio", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ DataHorarioInicioAnteriorADataAtualException.class })
    public ResponseEntity<Object> handleDataHorarioInicioAnteriorADataAtual(DataHorarioInicioAnteriorADataAtualException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.data-horario-inicio-anterior-atual", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ DataHorarioInicioAnteriorADataAtualUsoContinuoException.class })
    public ResponseEntity<Object> handleDataHorarioInicioAnteriorADataAtualUsoContinuo(DataHorarioInicioAnteriorADataAtualUsoContinuoException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.data-horario-inicio-anterior-atual-uso-continuo", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ DependenciaInativaException.class })
    public ResponseEntity<Object> handleDependenciaInativa(DependenciaInativaException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.dependencia-inativa", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ TurmaInativaException.class })
    public ResponseEntity<Object> handleTurmaInativa(TurmaInativaException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.turma-inativa", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ ReservaDependenciaUsoBreveException.class })
    public ResponseEntity<Object> handleReservaDependenciaUsoBreve(ReservaDependenciaUsoBreveException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.uso-breve", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ ReservaDependenciaUsoBreveComTurmaException.class })
    public ResponseEntity<Object> handleReservaDependenciaUsoBreveComTurma(ReservaDependenciaUsoBreveComTurmaException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.uso-breve-com-turma", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ DependenciaUsoBreveException.class })
    public ResponseEntity<Object> handleDependenciaUsoBreve(DependenciaUsoBreveException ex) {
        String mensagemUsuario = messageSource.getMessage("dependencia.uso-breve", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ DependenciaUsoContinuoException.class })
    public ResponseEntity<Object> handleDependenciaUsoContinuo(DependenciaUsoContinuoException ex) {
        String mensagemUsuario = messageSource.getMessage("dependencia.uso-continuo", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ ReservaDependenciaUsoContinuoException.class })
    public ResponseEntity<Object> handleReservaDependenciaUsoContinuo(ReservaDependenciaUsoContinuoException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.uso-continuo", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

//    @ExceptionHandler({ SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException.class })
//    public ResponseEntity<Object> handleSolicitacaoReservaVeiculoComCompatibilidadeDeHorario(SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException ex) {
//        String mensagemUsuario = messageSource.getMessage("reserva.compatiblidade-horario-veiculo", null, LocaleContextHolder.getLocale());
//        String mensagemDesenvolvedor = ex.toString();
//        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
//        return new ResponseEntity<>(erros, HttpStatus.CONFLICT);
//    }

    @ExceptionHandler({ UsuarioLogadoInativoException.class })
    public ResponseEntity<Object> handleUsuarioLogadoInativo(UsuarioLogadoInativoException ex) {
        String mensagemUsuario = messageSource.getMessage("usuario.logado-inativo", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ RelatorioVazioException.class })
    public ResponseEntity<Object> handleRelatorioVazio(RelatorioVazioException ex) {
        String mensagemUsuario = messageSource.getMessage("relatorio.vazio", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.NOT_FOUND);
    }
}
