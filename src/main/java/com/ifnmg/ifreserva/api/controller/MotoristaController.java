package com.ifnmg.ifreserva.api.controller;

import com.ifnmg.ifreserva.api.event.RecursoCriadoEvent;
import com.ifnmg.ifreserva.api.exceptionhandler.IfreservaExceptionHandler.Erro;
import com.ifnmg.ifreserva.api.model.Motorista;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.repository.MotoristaRepository;
import com.ifnmg.ifreserva.api.service.MotoristaService;
import com.ifnmg.ifreserva.api.service.UsuarioService;
import com.ifnmg.ifreserva.api.service.exception.CpfMotoristaJaCadastradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/motorista")
public class MotoristaController {

    @Autowired
    private MotoristaRepository motoristaRepository;
    @Autowired
    private MotoristaService motoristaService;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_MASTER','ROLE_APPROVER')")
    public ResponseEntity<Motorista> salvar(@Valid @RequestBody Motorista motorista, HttpServletResponse response, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        Motorista motoristaSalvo = motoristaService.salvar(motorista, usuarioLogado.get());
        publisher.publishEvent(new RecursoCriadoEvent(this, response, motoristaSalvo.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(motoristaSalvo);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Motorista> buscarPeloId(@PathVariable Long id) {
        Motorista motorista = motoristaRepository.findOne(id);
        return motorista != null ? ResponseEntity.ok(motorista) : ResponseEntity.notFound().build();
    }

    @GetMapping
    public ResponseEntity<List<Motorista>> motoristas(){
        List<Motorista> motoristas = motoristaRepository.findAll();
        return ResponseEntity.ok(motoristas);
    }

    @GetMapping("/ativo")
    public ResponseEntity<List<Motorista>> motoristasAtivos(){
        List<Motorista> motoristas = motoristaRepository.motoristasAtivos();
        return ResponseEntity.ok(motoristas);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_MASTER','ROLE_APPROVER')")
    public ResponseEntity<Motorista> atualizar(@PathVariable Long id, @Valid @RequestBody Motorista motorista, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        Motorista motoristaSalvo = motoristaService.atualizar(id, motorista, usuarioLogado.get());
        return ResponseEntity.ok(motoristaSalvo);
    }

    @PutMapping("/{id}/ativo")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyAuthority('ROLE_MASTER','ROLE_APPROVER')")
    public void atualizarPropriedadeAtivo(@PathVariable Long id, @RequestBody Boolean ativo, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        motoristaService.atualizarPropriedadeAtivo(id, ativo, usuarioLogado.get());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyAuthority('ROLE_MASTER','ROLE_APPROVER')")
    public void excluir(@PathVariable Long id, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        Motorista motorista = motoristaService.buscarMotoristaPeloId(id);
        motoristaRepository.delete(id);
        motoristaService.salvarTrilhaAuditoriaCadastro(motorista, usuarioLogado.get(), "Exclusão");
    }

    @ExceptionHandler({ CpfMotoristaJaCadastradoException.class })
    public ResponseEntity<Object> handleCpfMotoristaJaCadastrado(CpfMotoristaJaCadastradoException ex) {
        String mensagemUsuario = messageSource.getMessage("motorista.cpf-ja-cadastrado", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.CONFLICT);
    }
}
