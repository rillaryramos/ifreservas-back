package com.ifnmg.ifreserva.api.controller;

import com.ifnmg.ifreserva.api.event.RecursoCriadoEvent;
import com.ifnmg.ifreserva.api.exceptionhandler.IfreservaExceptionHandler.Erro;
import com.ifnmg.ifreserva.api.model.Dependencia;
import com.ifnmg.ifreserva.api.repository.DependenciaRepository;
import com.ifnmg.ifreserva.api.service.DependenciaService;
import com.ifnmg.ifreserva.api.service.exception.NomeDependenciaJaCadastradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/dependencia")
public class DependenciaController {

    @Autowired
    private DependenciaRepository dependenciaRepository;
    @Autowired
    private DependenciaService dependenciaService;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private MessageSource messageSource;

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public ResponseEntity<Dependencia> salvar(@Valid @RequestBody Dependencia dependencia, HttpServletResponse response) {
        Dependencia dependenciaSalva = dependenciaService.salvar(dependencia);
        publisher.publishEvent(new RecursoCriadoEvent(this, response, dependenciaSalva.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(dependenciaSalva);
    }

    @GetMapping
    public ResponseEntity<List<Dependencia>> listar() {
        List<Dependencia> dependencias = dependenciaRepository.findAll();
        return ResponseEntity.ok(dependencias);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Dependencia> buscarPeloId(@PathVariable Long id) {
        Dependencia dependencia = dependenciaRepository.findOne(id);
        return dependencia != null ? ResponseEntity.ok(dependencia) : ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public ResponseEntity<Dependencia> atualizar(@PathVariable Long id, @Valid @RequestBody Dependencia dependencia) {
        Dependencia dependenciaSalva = dependenciaService.atualizar(id, dependencia);
        return ResponseEntity.ok(dependenciaSalva);
    }

    @PutMapping("/{id}/ativa")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public void atualizarPropriedadeAtivo(@PathVariable Long id, @RequestBody Boolean ativo) {
        dependenciaService.atualizarPropriedadeAtivo(id, ativo);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public void excluir(@PathVariable Long id) {
        dependenciaRepository.delete(id);
    }

    @ExceptionHandler({ NomeDependenciaJaCadastradoException.class })
    public ResponseEntity<Object> handleNomeDependenciaJaCadastrado(NomeDependenciaJaCadastradoException ex) {
        String mensagemUsuario = messageSource.getMessage("dependencia.nome-ja-cadastrado", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.CONFLICT);
    }
}
