package com.ifnmg.ifreserva.api.controller;

import com.ifnmg.ifreserva.api.event.RecursoCriadoEvent;
import com.ifnmg.ifreserva.api.exceptionhandler.IfreservaExceptionHandler.Erro;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.model.Veiculo;
import com.ifnmg.ifreserva.api.repository.VeiculoRepository;
import com.ifnmg.ifreserva.api.service.UsuarioService;
import com.ifnmg.ifreserva.api.service.VeiculoService;
import com.ifnmg.ifreserva.api.service.exception.PlacaVeiculoJaCadastradaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/veiculo")
public class VeiculoController {

    @Autowired
    private VeiculoRepository veiculoRepository;
    @Autowired
    private VeiculoService veiculoService;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_MASTER','ROLE_APPROVER')")
    public ResponseEntity<Veiculo> salvar(@Valid @RequestBody Veiculo veiculo, HttpServletResponse response, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        Veiculo veiculoSalvo = veiculoService.salvar(veiculo, usuarioLogado.get());
        publisher.publishEvent(new RecursoCriadoEvent(this, response, veiculoSalvo.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(veiculoSalvo);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Veiculo> buscarPeloId(@PathVariable Long id) {
        Veiculo veiculo = veiculoRepository.findOne(id);
        return veiculo != null ? ResponseEntity.ok(veiculo) : ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_MASTER','ROLE_APPROVER')")
    public ResponseEntity<Veiculo> atualizar(@PathVariable Long id, @Valid @RequestBody Veiculo dependencia, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        Veiculo dependenciaSalva = veiculoService.atualizar(id, dependencia, usuarioLogado.get());
        return ResponseEntity.ok(dependenciaSalva);
    }

    @PutMapping("/{id}/ativo")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyAuthority('ROLE_MASTER','ROLE_APPROVER')")
    public void atualizarPropriedadeAtivo(@PathVariable Long id, @RequestBody Boolean ativo, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        veiculoService.atualizarPropriedadeAtivo(id, ativo, usuarioLogado.get());
    }

    @GetMapping
    public ResponseEntity<List<Veiculo>> veiculos(){
        List<Veiculo> veiculos = veiculoRepository.findAll();
        return ResponseEntity.ok(veiculos);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyAuthority('ROLE_MASTER','ROLE_APPROVER')")
    public void excluir(@PathVariable Long id, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        Veiculo veiculo = veiculoService.buscarVeiculoPeloId(id);
        veiculoRepository.delete(id);
        veiculoService.salvarTrilhaAuditoriaCadastro(veiculo, usuarioLogado.get(), "Exclusão");
    }

    @ExceptionHandler({ PlacaVeiculoJaCadastradaException.class })
    public ResponseEntity<Object> handlePlacaVeiculoJaCadastrada(PlacaVeiculoJaCadastradaException ex) {
        String mensagemUsuario = messageSource.getMessage("veiculo.placa-ja-cadastrada", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.CONFLICT);
    }
}
