package com.ifnmg.ifreserva.api.controller;

import com.ifnmg.ifreserva.api.model.TrilhaAuditoria;
import com.ifnmg.ifreserva.api.repository.TrilhaAuditoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/trilha_auditoria")
public class TrilhaAuditoriaController {

    @Autowired
    private TrilhaAuditoriaRepository trilhaAuditoriaRepository;

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public List<TrilhaAuditoria> listar() {
        return trilhaAuditoriaRepository.findAllByOrderByDataDesc();
    }
}
