package com.ifnmg.ifreserva.api.controller;

import com.ifnmg.ifreserva.api.event.RecursoCriadoEvent;
import com.ifnmg.ifreserva.api.exceptionhandler.IfreservaExceptionHandler.Erro;
import com.ifnmg.ifreserva.api.model.Turma;
import com.ifnmg.ifreserva.api.repository.TurmaRepository;
import com.ifnmg.ifreserva.api.service.TurmaService;
import com.ifnmg.ifreserva.api.service.exception.NomeTurmaJaCadastradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/turma")
public class TurmaController {

    @Autowired
    private TurmaRepository turmaRepository;
    @Autowired
    private TurmaService turmaService;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private MessageSource messageSource;

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public ResponseEntity<Turma> salvar(@Valid @RequestBody Turma turma, HttpServletResponse response) {
        Turma turmaSalva = turmaService.salvar(turma);
        publisher.publishEvent(new RecursoCriadoEvent(this, response, turmaSalva.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(turmaSalva);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Turma> buscarPeloId(@PathVariable Long id) {
        Turma turma = turmaRepository.findOne(id);
        return turma != null ? ResponseEntity.ok(turma) : ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public ResponseEntity<Turma> atualizar(@PathVariable Long id, @Valid @RequestBody Turma turma) {
        Turma turmaSalva = turmaService.atualizar(id, turma);
        return ResponseEntity.ok(turmaSalva);
    }

    @PutMapping("/{id}/ativa")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public void atualizarPropriedadeAtivo(@PathVariable Long id, @RequestBody Boolean ativo) {
        turmaService.atualizarPropriedadeAtivo(id, ativo);
    }

    @GetMapping("/ativa")
    public ResponseEntity<List<Turma>> turmasAtivas(){
        List<Turma> turmas = turmaRepository.turmasAtivas();
        return ResponseEntity.ok(turmas);
    }

    @GetMapping
    public ResponseEntity<List<Turma>> veiculos(){
        List<Turma> turmas = turmaRepository.findAll();
        return ResponseEntity.ok(turmas);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public void excluir(@PathVariable Long id) {
        turmaRepository.delete(id);
    }

    @ExceptionHandler({ NomeTurmaJaCadastradoException.class })
    public ResponseEntity<Object> handleNomeTurmaJaCadastrado(NomeTurmaJaCadastradoException ex) {
        String mensagemUsuario = messageSource.getMessage("turma.nome-ja-cadastrado", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.CONFLICT);
    }
}
