package com.ifnmg.ifreserva.api.controller;

import com.ifnmg.ifreserva.api.event.RecursoCriadoEvent;
import com.ifnmg.ifreserva.api.exceptionhandler.IfreservaExceptionHandler.Erro;
import com.ifnmg.ifreserva.api.model.ReservaVeiculo;
import com.ifnmg.ifreserva.api.model.AprovacaoReservaVeiculoRequest;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.repository.ReservaVeiculoRepository;
import com.ifnmg.ifreserva.api.security.UsuarioSistema;
import com.ifnmg.ifreserva.api.service.ReservaVeiculoService;
import com.ifnmg.ifreserva.api.service.UsuarioService;
import com.ifnmg.ifreserva.api.service.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/reserva_veiculo")
public class ReservaVeiculoController {

    @Autowired
    private ReservaVeiculoService reservaVeiculoService;
    @Autowired
    private ReservaVeiculoRepository reservaVeiculoRepository;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private MessageSource messageSource;

    @PostMapping
    private ResponseEntity<ReservaVeiculo> salvar(@Valid @RequestBody ReservaVeiculo reservaVeiculo, HttpServletResponse response, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaVeiculo reservaSalva = reservaVeiculoService.salvar(reservaVeiculo, usuarioLogado.get());
        publisher.publishEvent(new RecursoCriadoEvent(this, response, reservaSalva.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(reservaSalva);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ReservaVeiculo> atualizar(@PathVariable Long id, @Valid @RequestBody ReservaVeiculo reservaVeiculo, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaVeiculo reservaSalva = reservaVeiculoService.atualizar(id, reservaVeiculo, usuarioLogado.get());
        return ResponseEntity.ok(reservaSalva);
    }

    @GetMapping
    private ResponseEntity<List<ReservaVeiculo>> listarReservas() {
        List<ReservaVeiculo> reservas = reservaVeiculoRepository.findAll();
        return ResponseEntity.ok(reservas);
    }

    @GetMapping("/veiculo/{id}")
    private ResponseEntity<List<ReservaVeiculo>> listarReservasPorVeiculo(@PathVariable Long id, @RequestParam String tipo, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        List<ReservaVeiculo> reservas = reservaVeiculoService.listarReservasPorVeiculo(id, usuarioLogado.get(), tipo);
        return ResponseEntity.ok(reservas);
    }

    @GetMapping("/em_aprovacao")
    private Page<ReservaVeiculo> listarReservasEmAprovacao(@RequestParam Long idStatus, @RequestParam String idVeiculo, Pageable pageable) {
        if (idVeiculo.equals("null")) {
            if (idStatus == 2) {
                return reservaVeiculoRepository.findByStatusIdOrderByStart(idStatus,pageable);
            } else {
                return reservaVeiculoRepository.findByStatusIdOrderByStartDesc(idStatus, pageable);
            }
        } else {
            if (idStatus == 2) {
                return reservaVeiculoRepository.findByStatusIdAndVeiculoIdOrderByStart(idStatus,Long.parseLong(idVeiculo),pageable);
            } else {
                return reservaVeiculoRepository.findByStatusIdAndVeiculoIdOrderByStartDesc(idStatus,Long.parseLong(idVeiculo),pageable);
            }
        }

//        return reservaVeiculoRepository.listarReservasEmAprovacao(pageable);
    }

    @GetMapping("/aprovadas")
    private List<ReservaVeiculo> listarReservasAprovadas() { //ALTERAR PRA RETORNAR SOMENTE AS COM DATA MAIOR QUE ATUAL
        List<ReservaVeiculo> reservas = reservaVeiculoRepository.listarReservasAprovadas();
        return reservas;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReservaVeiculo> buscarPeloId(@PathVariable Long id) {
        ReservaVeiculo reserva = reservaVeiculoRepository.findOne(id);
        return reserva != null ? ResponseEntity.ok(reserva) : ResponseEntity.notFound().build();
    }

    @PutMapping("/cancelar/{id}")
    public ResponseEntity<ReservaVeiculo> cancelarReserva(@PathVariable Long id, @RequestBody String motivoCancelamento, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaVeiculo reservaVeiculo = reservaVeiculoService.cancelarReserva(id, usuarioLogado.get());
        return ResponseEntity.ok(reservaVeiculo);
    }

    // ACESSÍVEL SOMENTE PARA O APPROVER
    @PutMapping("/cancelar/{id}/administrador")
    public ResponseEntity<ReservaVeiculo> cancelarReservaAprovada(@PathVariable Long id, @RequestBody String motivoCancelamento, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaVeiculo reservaVeiculo = reservaVeiculoService.cancelarReservaAprovada(id, motivoCancelamento, usuarioLogado.get());
        return ResponseEntity.ok(reservaVeiculo);
    }

    // ACESSÍVEL SOMENTE PARA O APPROVER
    @PutMapping("/{idReserva}/motorista")
    public ResponseEntity<ReservaVeiculo> alterarMotorista(@PathVariable Long idReserva, @RequestBody String idMotorista, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaVeiculo reservaVeiculo = reservaVeiculoService.alterarMotorista(idReserva, idMotorista, usuarioLogado.get());
        return ResponseEntity.ok(reservaVeiculo);
    }

    @PutMapping("/reprovar/{id}")
    public ResponseEntity<ReservaVeiculo> reprovarReserva(@PathVariable Long id, @RequestBody String motivoReprovacao, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaVeiculo reservaSalva = reservaVeiculoService.reprovarReserva(id, motivoReprovacao, usuarioLogado.get());
        return ResponseEntity.ok(reservaSalva);
    }

    @GetMapping("/em_aprovacao/verificar_compatibilidade/{id}")
    public ResponseEntity<List<ReservaVeiculo>> verificarCompatibilidadeReservasEmAprovacao(@PathVariable Long id, @RequestParam String start, @RequestParam String end) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        List<ReservaVeiculo> reservas = reservaVeiculoService.verificarCompatibilidadeReservasEmAprovacao(id, LocalDateTime.parse(start.replaceAll("T", " "), formatter), LocalDateTime.parse(end.replaceAll("T", " "), formatter));
        return ResponseEntity.ok(reservas);
    }

    @PutMapping("/aprovar/{id}")
    public ResponseEntity<ReservaVeiculo> aprovarReserva(@PathVariable Long id, @RequestBody AprovacaoReservaVeiculoRequest aprovacaoReservaVeiculoRequest, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaVeiculo reservaSalva = reservaVeiculoService.aprovarReserva(id, aprovacaoReservaVeiculoRequest, usuarioLogado.get());
        return ResponseEntity.ok(reservaSalva);
    }

    @PutMapping("/{idReserva}/status/{idStatus}")
    public ResponseEntity<ReservaVeiculo> atualizarPropriedadeStatus(@PathVariable Long idReserva, @PathVariable Long idStatus, OAuth2Authentication authentication) {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        ReservaVeiculo reservaVeiculo = reservaVeiculoService.atualizarPropriedadeStatus(idReserva, idStatus, usuarioLogado.get());
        return ResponseEntity.ok(reservaVeiculo);
    }

    @GetMapping("/relatorio/minhas_reservas")
    public ResponseEntity<byte[]> relatorioMinhasReservas(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate inicio,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fim,
            @RequestParam String idVeiculo,
            @RequestParam String idStatus,
            OAuth2Authentication authentication) throws Exception {
        Optional<Usuario> usuarioLogado = usuarioService.buscarUsuarioPeloLogin(authentication.getPrincipal().toString());
        Long veiculo, status;
        veiculo = !idVeiculo.equals("null") ? Long.parseLong(idVeiculo) : null;
        status = !idStatus.equals("null") ? Long.parseLong(idStatus) : null;
        byte[] relatorio = reservaVeiculoService.relatorioMinhasReservas(inicio, fim, veiculo, status, usuarioLogado.get());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
                .body(relatorio);
    }

    @GetMapping("/relatorio/aprovadas")
    public ResponseEntity<byte[]> relatorioReservasAprovadas(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate inicio,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fim,
            @RequestParam String idVeiculo,
            @RequestParam String idUsuario,
            @RequestParam String idMotorista) throws Exception {
        Long veiculo, usuario, motorista;
        veiculo = !idVeiculo.equals("null") ? Long.parseLong(idVeiculo) : null;
        usuario = !idUsuario.equals("null") ? Long.parseLong(idUsuario) : null;
        motorista = !idMotorista.equals("null") ? Long.parseLong(idMotorista) : null;
        byte[] relatorio = reservaVeiculoService.relatorioReservasAprovadas(inicio, fim, veiculo, usuario, motorista);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
                .body(relatorio);
    }

    @ExceptionHandler({ UsuarioLogadoInativoException.class })
    public ResponseEntity<Object> handleUsuarioLogadoInativo(UsuarioLogadoInativoException ex) {
        String mensagemUsuario = messageSource.getMessage("usuario.logado-inativo", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ DataHorarioInicioAnteriorADataAtualException.class })
    public ResponseEntity<Object> handleDataHorarioInicioAnteriorADataAtual(DataHorarioInicioAnteriorADataAtualException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.data-horario-inicio-anterior-atual", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ DataHorarioTerminoMenorQueDataHorarioInicioException.class })
    public ResponseEntity<Object> handleDataHorarioTerminoMenorQueDataHorarioInicio(DataHorarioTerminoMenorQueDataHorarioInicioException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.horario-termino-anterior-horario-inicio", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException.class })
    public ResponseEntity<Object> handleSolicitacaoReservaVeiculoComCompatibilidadeDeHorario(SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException ex) {
//        String mensagemUsuario = messageSource.getMessage("reserva.compatiblidade-horario-veiculo", null, LocaleContextHolder.getLocale());
//        String mensagemDesenvolvedor = ex.toString();
//        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(ex.reservas, HttpStatus.CONFLICT);
    }


    @ExceptionHandler({ SemUsuariosAprovadoresException.class })
    public ResponseEntity<Object> handleSemUsuariosAprovadores(SemUsuariosAprovadoresException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.sem-usuarios-aprovadores", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ SemUsuariosAprovadoresAtivoException.class })
    public ResponseEntity<Object> handleSemUsuariosAprovadoresAtivo(SemUsuariosAprovadoresAtivoException ex) {
        String mensagemUsuario = messageSource.getMessage("reserva.sem-usuarios-aprovadores-ativo", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ VeiculoInativoException.class })
    public ResponseEntity<Object> handleVeiculoInativo(VeiculoInativoException ex) {
        String mensagemUsuario = messageSource.getMessage("veiculo.inativo", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ RelatorioVazioException.class })
    public ResponseEntity<Object> handleRelatorioVazio(RelatorioVazioException ex) {
        String mensagemUsuario = messageSource.getMessage("relatorio.vazio", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.NOT_FOUND);
    }
}
