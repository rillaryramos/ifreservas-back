package com.ifnmg.ifreserva.api.controller;

import com.ifnmg.ifreserva.api.event.RecursoCriadoEvent;
import com.ifnmg.ifreserva.api.exceptionhandler.IfreservaExceptionHandler.Erro;
import com.ifnmg.ifreserva.api.model.Bloco;
import com.ifnmg.ifreserva.api.repository.BlocoRepository;
import com.ifnmg.ifreserva.api.service.BlocoService;
import com.ifnmg.ifreserva.api.service.exception.NomeBlocoJaCadastradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/bloco")
public class BlocoController {

    @Autowired
    private BlocoService blocoService;
    @Autowired
    private BlocoRepository blocoRepository;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private MessageSource messageSource;

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public ResponseEntity<Bloco> salvar(@Valid @RequestBody Bloco bloco, HttpServletResponse response) {
        Bloco blocoSalvo = blocoService.salvar(bloco);
        publisher.publishEvent(new RecursoCriadoEvent(this, response, blocoSalvo.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(blocoSalvo);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public List<Bloco> listar() {
        List<Bloco> blocos = blocoRepository.findAll();
        return blocos;
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public ResponseEntity<Bloco> buscarPeloId(@PathVariable Long id) {
        Bloco bloco = blocoRepository.findOne(id);
        return bloco != null ? ResponseEntity.ok(bloco) : ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public ResponseEntity<Bloco> atualizar(@PathVariable Long id, @Valid @RequestBody Bloco bloco) {
        Bloco blocoSalvo = blocoService.atualizar(id, bloco);
        return ResponseEntity.ok(blocoSalvo);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public void excluir(@PathVariable Long id) {
        blocoRepository.delete(id);
    }

    @ExceptionHandler({ NomeBlocoJaCadastradoException.class })
    public ResponseEntity<Object> handleNomeBlocoJaCadastrado(NomeBlocoJaCadastradoException ex) {
        String mensagemUsuario = messageSource.getMessage("bloco.nome-ja-cadastrado", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.CONFLICT);
    }
}
