package com.ifnmg.ifreserva.api.controller;

import com.ifnmg.ifreserva.api.event.RecursoCriadoEvent;
import com.ifnmg.ifreserva.api.exceptionhandler.IfreservaExceptionHandler.Erro;
import com.ifnmg.ifreserva.api.model.EmailResponse;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.repository.UsuarioRepository;
import com.ifnmg.ifreserva.api.service.RecuperacaoSenhaService;
import com.ifnmg.ifreserva.api.service.UsuarioService;
import com.ifnmg.ifreserva.api.service.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RecuperacaoSenhaService recuperacaoSenhaService;

    //SALVAR SERVIDOR -> ACESSÍVEL SOMENTE PARA O MASTER
    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public ResponseEntity<Usuario> salvar(@Valid @RequestBody Usuario usuario, HttpServletResponse response) {
        //pegar usuario logado e verificar se existe
        Long idUsuarioLogado = null;
        Usuario usuarioSalvo = usuarioService.salvar(usuario, idUsuarioLogado);
        publisher.publishEvent(new RecursoCriadoEvent(this, response, usuarioSalvo.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(usuarioSalvo);
    }

    @GetMapping("/ativo")
    public ResponseEntity<List<Usuario>> usuariosAtivos(){
        List<Usuario> usuarios = usuarioRepository.usuariosAtivos();
        return ResponseEntity.ok(usuarios);
    }

    @GetMapping("/todos")
    public List<Usuario> usuarios() {
        return usuarioRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Usuario> buscarPeloId(@PathVariable Long id) {
        Usuario usuario = usuarioRepository.findOne(id);
        return usuario != null ? ResponseEntity.ok(usuario) : ResponseEntity.notFound().build();
    }

    @GetMapping("/email")
    public ResponseEntity<Usuario> buscarPeloEmail(@RequestParam String email) {
        Optional<Usuario> usuario = usuarioService.buscarUsuarioPeloEmail(email);
        return usuario != null ? ResponseEntity.ok(usuario.get()) : ResponseEntity.notFound().build();
    }

    @GetMapping("/login")
    public ResponseEntity<Usuario> buscarPeloLogin(@RequestParam String login) {
        Optional<Usuario> usuario = usuarioService.buscarUsuarioPeloLogin(login);
        return usuario != null ? ResponseEntity.ok(usuario.get()) : ResponseEntity.notFound().build();
    }
    //ATUALIZANDO USUARIO -> ACESSÍVEL SOMENTE PARA O MASTER
    @PutMapping("/{id}")
    public ResponseEntity<Usuario> atualizar(@PathVariable Long id, @Valid @RequestBody Usuario usuario) {
        Usuario usuarioSalvo = usuarioService.atualizar(id, usuario);
        return ResponseEntity.ok(usuarioSalvo);
    }

    @PutMapping("/{id}/roleAtivo")
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public ResponseEntity<Usuario> atualizarRoleAtivo(@PathVariable Long id, @RequestParam String role, @RequestParam Boolean ativo) {
        Usuario usuarioSalvo = usuarioService.atualizarRoleAtivo(id, role, ativo);
        return ResponseEntity.ok(usuarioSalvo);
    }

    //INATIVAR/ATIVAR SERVIDOR -> ACESSÍVEL SOMENTE PARA O MASTER
    @PutMapping("/{id}/ativo")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public void atualizarPropriedadeAtivo(@PathVariable Long id, @RequestBody Boolean ativo) {
        usuarioService.atualizarPropriedadeAtivo(id, ativo);
    }

    //ALTERAR SENHA -> ACESSÍVEL SOMENTE PARA O PRÓPRIO USUARIO
    @PutMapping("/{id}/alterarSenha")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void alterarSenha(@PathVariable Long id, @Param("senhaAtual") String senhaAtual, @Param("novaSenha") String novaSenha) {
        usuarioService.alterarPropriedadeSenha(senhaAtual, novaSenha, id);
    }

    //RECUPERAR SENHA
    @GetMapping("/enviarLinkParaAlteracaoDeSenha")
    @ResponseStatus(HttpStatus.OK)
    public void enviarLinkParaAlteracaoDeSenha(@Param("email") String email) {
        System.out.println("Email: " + email);
        usuarioService.enviarLinkParaAlteracaoDeSenha(email);
    }

    @GetMapping("/validarToken")
    public ResponseEntity<EmailResponse> validarToken(@Param("token") String token) {
        String emailRecuperacao = recuperacaoSenhaService.validarToken(token);
        EmailResponse emailResponse = new EmailResponse();
        emailResponse.setEmail(emailRecuperacao);
        return ResponseEntity.ok(emailResponse);
    }

    @PutMapping("/recuperarSenha/alterar")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void recuperarSenha(@Param("token") String token, @Param("email") String email, @Param("novaSenha") String novaSenha) {
        usuarioService.recuperarPropriedadeSenha(token, email, novaSenha);
    }

    //LISTAR TODOS OS USUÁRIOS, MENOS O MASTER
    @GetMapping
    public List<Usuario> listarUsuarios() {
        return usuarioRepository.findUsers();
    }

    //TROCAR MASTER -> ACESSÍVEL SOMENTE PARA O MASTER
    @PutMapping("/trocarMaster")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void trocarUserMaster(@Param("idNovoMaster") Long idNovoMaster, @Param("antigoMasterIsNovoAdmin") Boolean antigoMasterIsNovoAdmin) {
        usuarioService.trocarUserMaster(idNovoMaster, antigoMasterIsNovoAdmin);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('ROLE_MASTER')")
    public void excluir(@PathVariable Long id) {
        usuarioRepository.delete(id);
    }

    @ExceptionHandler({ SalvarUsuarioMasterException.class })
    public ResponseEntity<Object> handleSalvarUsuarioMaster(SalvarUsuarioMasterException ex) {
        String mensagemUsuario = messageSource.getMessage("usuario.master", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({ EmailJaCadastradoException.class })
    public ResponseEntity<Object> handleEmailJaCadastrado(EmailJaCadastradoException ex) {
        String mensagemUsuario = messageSource.getMessage("usuario.email-ja-cadastrado", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({ LoginJaCadastradoException.class })
    public ResponseEntity<Object> handleLoginJaCadastrado(LoginJaCadastradoException ex) {
        String mensagemUsuario = messageSource.getMessage("usuario.login-ja-cadastrado", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({ TokenInativoException.class })
    public ResponseEntity<Object> handleTokenInativo(TokenInativoException ex) {
        String mensagemUsuario = messageSource.getMessage("usuario.token-inativo", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ NovaSenhaInvalidaException.class })
    public ResponseEntity<Object> handleNovaSenhaInvalida(NovaSenhaInvalidaException ex) {
        String mensagemUsuario = messageSource.getMessage("usuario.nova-senha", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ SenhaAtualInvalidaException.class })
    public ResponseEntity<Object> handleSenhaAtualInvalida(SenhaAtualInvalidaException ex) {
        String mensagemUsuario = messageSource.getMessage("usuario.senha-atual", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ NovoMasterNaoEstaAtivoException.class })
    public ResponseEntity<Object> handleNovoMasterNaoEstaAtivo(NovoMasterNaoEstaAtivoException ex) {
        String mensagemUsuario = messageSource.getMessage("usuario.novo-master-inativo", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ ParametrosParaTrocarMasterException.class })
    public ResponseEntity<Object> handleParametrosParaTrocarMaster(ParametrosParaTrocarMasterException ex) {
        String mensagemUsuario = messageSource.getMessage("parametros.trocar-master", null, LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
    }
}
