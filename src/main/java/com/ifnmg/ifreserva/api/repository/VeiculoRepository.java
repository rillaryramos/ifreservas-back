package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.Veiculo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VeiculoRepository extends JpaRepository<Veiculo, Long> {

    public Veiculo findByPlaca(String placa);

    @Query(value = "SELECT * FROM veiculo AS v WHERE v.placa = ?1 AND v.id <> ?2", nativeQuery = true)
    public Veiculo findByPlacaUpdate(String placa, Long id);
}
