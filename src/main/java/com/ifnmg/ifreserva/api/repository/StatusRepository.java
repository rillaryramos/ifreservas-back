package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

    public Status findByNome(String nome);
}
