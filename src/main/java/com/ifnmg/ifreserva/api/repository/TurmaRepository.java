package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.Turma;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TurmaRepository extends JpaRepository<Turma, Long> {

    public Turma findByNome(String nome);

    @Query(value = "SELECT * FROM turma AS t WHERE t.nome = ?1 AND t.id <> ?2", nativeQuery = true)
    public Turma findByNomeUpdate(String nome, Long id);

    @Query(value = "SELECT * FROM turma AS t WHERE t.ativo = 1", nativeQuery = true)
    public List<Turma> turmasAtivas();
}
