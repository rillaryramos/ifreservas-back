package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.RecuperacaoSenha;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface RecuperacaoSenhaRepository extends JpaRepository<RecuperacaoSenha, Long> {

    public RecuperacaoSenha findByToken(String token);

    public RecuperacaoSenha findByUsuarioId(Long idUsuario);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM recuperacao_senha WHERE data_expiracao <= NOW()", nativeQuery = true)
    public void removerRecuperacoesExpiradas();

}
