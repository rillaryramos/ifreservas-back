package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.Dependencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DependenciaRepository extends JpaRepository<Dependencia, Long> {

    public Dependencia findByNomeAndBlocoId(String nome, Long idBloco);

    @Query(value = "SELECT * FROM dependencia AS d WHERE d.nome = ?1 AND d.id_bloco = ?2 AND d.id <> ?3", nativeQuery = true)
    public Dependencia findByNomeUpdate(String nome, Long idBloco, Long id);
}
