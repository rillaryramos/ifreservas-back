package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.ReservaDependencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface ReservaDependenciaRepository extends JpaRepository<ReservaDependencia, Long> {

    @Query(value = "SELECT * FROM reserva_dependencia AS RD WHERE RD.ID_DEPENDENCIA = ?1 AND RD.ID_STATUS = 1", nativeQuery = true)
    public List<ReservaDependencia> listarReservasPorDependencia(Long idDependencia);

    @Query(value = "SELECT * FROM reserva_dependencia AS RD WHERE RD.ID_DEPENDENCIA = ?1 AND (RD.ID_USUARIO_SOLICITANTE = ?2 OR RD.ID_USUARIO_UTILIZADOR = ?2)", nativeQuery = true)
    public List<ReservaDependencia> listarMinhasReservasPorDependencia(Long idDependencia, Long idUsuario);

    @Query(value = "SELECT RD.* FROM reserva_dependencia AS RD \n" +
            "LEFT JOIN horario_reserva AS HR ON RD.ID = HR.ID_RESERVA_DEPENDENCIA\n" +
            "WHERE RD.ID_STATUS = 1 AND RD.ID_DEPENDENCIA = ?1 AND\n" +
            "CASE WHEN (((?2 >= RD.START AND ?2 < RD.END)\n" +
            "\t\t\t\t\tOR (?3 > RD.START AND ?3 <= RD.END))\n" +
            "\t\t\t\t\tOR (RD.START >= ?2 AND RD.START < ?3))\n" +
            "\t\t  THEN\n" +
            "\t\t  \t\tCASE WHEN RD.USO_CONTINUO = 1\n" +
            "\t\t\t\t  \t\tTHEN ((((CONVERT(?2,TIME) >= HR.HORARIO_INICIO AND CONVERT(?2,TIME) < HR.HORARIO_TERMINO)\n" +
            "\t\t\t\t\t\t\t\tOR (CONVERT(?3,TIME) > HR.HORARIO_INICIO AND CONVERT(?3,TIME) <= HR.HORARIO_TERMINO))\n" +
            "\t\t\t\t\t\t\t\tOR (HR.HORARIO_INICIO >= CONVERT(?2,TIME) AND HR.HORARIO_INICIO < CONVERT(?3,TIME)))\n" +
            "\t\t\t\t\t\t\t\tAND UPPER(HR.DIA_SEMANA) = ?4)\n" + //AQUI ESTOU CONSIDERANDO QUE A RESERVA BREVE SÓ PODE TER A DATA DE INÍCIO E DE TÉRMINO NO MSM DIA
            "\t\t\t\t\t  ELSE\n" +
            "\t\t\t\t\t  \t\tTRUE\n" +
            "\t\t\t\t\t  END\n" +
            "\t\t\tEND\n" +
            "GROUP BY ID", nativeQuery = true)
    public List<ReservaDependencia> verificarCompatibilidaDeHorarioReservaUsoBreve(Long idDependencia, LocalDateTime start, LocalDateTime end, String diaSemana);

    @Query(value = "SELECT RD.* FROM reserva_dependencia AS RD \n" +
            "LEFT JOIN horario_reserva AS HR ON RD.ID = HR.ID_RESERVA_DEPENDENCIA\n" +
            "WHERE RD.ID_STATUS = 1 AND RD.ID_DEPENDENCIA = ?1 AND RD.ID <> ?5 AND\n" +
            "CASE WHEN (((?2 >= RD.START AND ?2 < RD.END)\n" +
            "\t\t\t\t\tOR (?3 > RD.START AND ?3 <= RD.END))\n" +
            "\t\t\t\t\tOR (RD.START >= ?2 AND RD.START < ?3))\n" +
            "\t\t  THEN\n" +
            "\t\t  \t\tCASE WHEN RD.USO_CONTINUO = 1\n" +
            "\t\t\t\t  \t\tTHEN ((((CONVERT(?2,TIME) >= HR.HORARIO_INICIO AND CONVERT(?2,TIME) < HR.HORARIO_TERMINO)\n" +
            "\t\t\t\t\t\t\t\tOR (CONVERT(?3,TIME) > HR.HORARIO_INICIO AND CONVERT(?3,TIME) <= HR.HORARIO_TERMINO))\n" +
            "\t\t\t\t\t\t\t\tOR (HR.HORARIO_INICIO >= CONVERT(?2,TIME) AND HR.HORARIO_INICIO < CONVERT(?3,TIME)))\n" +
            "\t\t\t\t\t\t\t\tAND UPPER(HR.DIA_SEMANA) = ?4)\n" + //AQUI ESTOU CONSIDERANDO QUE A RESERVA BREVE SÓ PODE TER A DATA DE INÍCIO E DE TÉRMINO NO MSM DIA
            "\t\t\t\t\t  ELSE\n" +
            "\t\t\t\t\t  \t\tTRUE\n" +
            "\t\t\t\t\t  END\n" +
            "\t\t\tEND\n" +
            "GROUP BY ID", nativeQuery = true)
    public List<ReservaDependencia> verificarCompatibilidaDeHorarioReservaUsoBreveEdicao(Long idDependencia, LocalDateTime start, LocalDateTime end, String diaSemana, Long idReserva);

    @Query(value = "SELECT RD.* FROM reserva_dependencia AS RD \n" +
            "LEFT JOIN horario_reserva AS HR ON RD.ID = HR.ID_RESERVA_DEPENDENCIA\n" +
            "WHERE RD.ID_STATUS = 1 AND RD.ID_DEPENDENCIA = ?1 AND\n" +
            "CASE WHEN (((?2 >= RD.START AND ?2 < RD.END)\n" +
            "\t\t\t\t\tOR (?3 > RD.START AND ?3 <= RD.END))\n" +
            "\t\t\t\t\tOR (RD.START >= ?2 AND RD.START < ?3))\n" +
            "\t\t  THEN\n" +
            "\t\t  \t\tCASE RD.USO_CONTINUO\n" +
            "\t\t\t\t  WHEN 0\n" +
            "\t\t\t\t  \t\tTHEN (UPPER((SUBSTRING(DAYNAME(RD.START), 1, 3)) = ?4) AND ((CONVERT(?5,TIME) >= CONVERT(RD.START,TIME) AND CONVERT(?5,TIME) < CONVERT(RD.END,TIME))\n" +
            "\t\t\t\t\t\t  OR (CONVERT(?6,TIME) > CONVERT(RD.START,TIME) AND CONVERT(?6,TIME) <= CONVERT(RD.END,TIME))\n" +
            "\t\t\t\t\t\t  OR (CONVERT(RD.START,TIME) >= CONVERT(?5,TIME) AND CONVERT(RD.START,TIME) < CONVERT(?6,TIME))))\n" +
            "\t\t  \t\t  WHEN 1\n" +
            "\t\t\t\t  \t\tTHEN (UPPER(HR.DIA_SEMANA) = ?4 AND (((CONVERT(?5,TIME) >= HR.HORARIO_INICIO AND CONVERT(?5,TIME) < HR.HORARIO_TERMINO)\n" +
            "            \t\t\t\tOR (CONVERT(?6,TIME) > HR.HORARIO_INICIO AND CONVERT(?6,TIME) <= HR.HORARIO_TERMINO))\n" +
            "\t\t\t\t\t\t\t\tOR (HR.HORARIO_INICIO >= CONVERT(?5,TIME) AND HR.HORARIO_INICIO < CONVERT(?6,TIME))))\n" +
            "\t\t\t\tEND\n" +
            "\t\t\tEND\n" +
            "GROUP BY ID", nativeQuery = true)
    public List<ReservaDependencia> verificarCompatibilidaDeHorarioReservaUsoContinuo(Long idDependencia, LocalDateTime start, LocalDateTime end, String diaSemana, LocalTime horarioInicio, LocalTime horarioTermino);

    @Query(value = "SELECT RD.* FROM reserva_dependencia AS RD \n" +
            "LEFT JOIN horario_reserva AS HR ON RD.ID = HR.ID_RESERVA_DEPENDENCIA\n" +
            "WHERE RD.ID_STATUS = 1 AND RD.ID_DEPENDENCIA = ?1 AND RD.ID <> ?7 AND\n" +
            "CASE WHEN (((?2 >= RD.START AND ?2 < RD.END)\n" +
            "\t\t\t\t\tOR (?3 > RD.START AND ?3 <= RD.END))\n" +
            "\t\t\t\t\tOR (RD.START >= ?2 AND RD.START < ?3))\n" +
            "\t\t  THEN\n" +
            "\t\t  \t\tCASE RD.USO_CONTINUO\n" +
            "\t\t\t\t  WHEN 0\n" +
            "\t\t\t\t  \t\tTHEN (UPPER((SUBSTRING(DAYNAME(RD.START), 1, 3)) = ?4) AND ((CONVERT(?5,TIME) >= CONVERT(RD.START,TIME) AND CONVERT(?5,TIME) < CONVERT(RD.END,TIME))\n" +
            "\t\t\t\t\t\t  OR (CONVERT(?6,TIME) > CONVERT(RD.START,TIME) AND CONVERT(?6,TIME) <= CONVERT(RD.END,TIME))\n" +
            "\t\t\t\t\t\t  OR (CONVERT(RD.START,TIME) >= CONVERT(?5,TIME) AND CONVERT(RD.START,TIME) < CONVERT(?6,TIME))))\n" +
            "\t\t  \t\t  WHEN 1\n" +
            "\t\t\t\t  \t\tTHEN (UPPER(HR.DIA_SEMANA) = ?4 AND (((CONVERT(?5,TIME) >= HR.HORARIO_INICIO AND CONVERT(?5,TIME) < HR.HORARIO_TERMINO)\n" +
            "            \t\t\t\tOR (CONVERT(?6,TIME) > HR.HORARIO_INICIO AND CONVERT(?6,TIME) <= HR.HORARIO_TERMINO))\n" +
            "\t\t\t\t\t\t\t\tOR (HR.HORARIO_INICIO >= CONVERT(?5,TIME) AND HR.HORARIO_INICIO < CONVERT(?6,TIME))))\n" +
            "\t\t\t\tEND\n" +
            "\t\t\tEND\n" +
            "GROUP BY ID", nativeQuery = true)
    public List<ReservaDependencia> verificarCompatibilidaDeHorarioReservaUsoContinuoEdicao(Long idDependencia, LocalDateTime start, LocalDateTime end, String diaSemana, LocalTime horarioInicio, LocalTime horarioTermino, Long idReserva);

    @Query(value = "SELECT * FROM reserva_dependencia AS RD WHERE RD.ID_STATUS = 1 AND\n" +
            "CASE RD.USO_CONTINUO\n" +
            "\tWHEN 0\n" +
            "\t\tTHEN (CONVERT(RD.START, DATE) >= ?1 AND CONVERT(RD.END, DATE) <= ?2)\n" +
            "\tWHEN 1\n" +
            "\t\tTHEN (((?1 >= CONVERT(RD.START,DATE) AND ?1 <= CONVERT(RD.END,DATE)) \n" +
            "\t\t\tOR (?2 >= CONVERT(RD.START,DATE) AND ?2 <= CONVERT(RD.END,DATE)))\n" +
            "\t\t\tOR (CONVERT(RD.START,DATE) >= ?1 AND CONVERT(RD.START,DATE) < ?2)\n" +
            "\t\t\tOR (?1 = CONVERT(RD.START,DATE) AND ?1 = CONVERT(RD.END,DATE))) \n" +
            "\tEND\n" +
            "AND IF(?3 IS NOT NULL, RD.ID_DEPENDENCIA = ?3, TRUE)\n" +
            "AND IF(?4 IS NOT NULL, RD.ID_USUARIO_UTILIZADOR = ?4, TRUE)\n" +
            "AND IF(?5 IS NOT NULL, RD.ID_TURMA = ?5, TRUE)\n" +
            "AND IF(?6 = 1, RD.AULA = 1, IF(?6 is null, TRUE, RD.AULA = 0))", nativeQuery = true)
    public List<ReservaDependencia> relatorio(LocalDate inicio, LocalDate fim, Long idDependencia, Long idUsuario, Long idTurma, String tipo);
}
