package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.TrilhaAuditoria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrilhaAuditoriaRepository extends JpaRepository<TrilhaAuditoria, Long> {

    public List<TrilhaAuditoria> findAllByOrderByDataDesc();
}
