package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.ReservaVeiculo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ReservaVeiculoRepository extends JpaRepository<ReservaVeiculo, Long> {

    @Query(value = "SELECT * FROM reserva_veiculo AS RV WHERE RV.ID_VEICULO = ?1 AND RV.ID_STATUS IN (2,3)", nativeQuery = true)
    public List<ReservaVeiculo> listarReservasPorVeiculo(Long idVeiculo);

    @Query(value = "SELECT * FROM reserva_veiculo AS RV WHERE RV.ID_VEICULO = ?1 AND RV.ID_USUARIO = ?2", nativeQuery = true)
    public List<ReservaVeiculo> listarMinhasReservasPorVeiculo(Long idVeiculo, Long idUsuario);

//    @Query(value = "SELECT * FROM RESERVA_VEICULO AS RV WHERE RV.ID_STATUS = 2 ORDER BY RV.START", nativeQuery = true)
//    public Page<ReservaVeiculo> listarReservasEmAprovacao(Pageable pageable);

    public Page<ReservaVeiculo> findByStatusIdAndVeiculoIdOrderByStart(Long idStatus, Long idVeiculo, Pageable pageable);
    public Page<ReservaVeiculo> findByStatusIdAndVeiculoIdOrderByStartDesc(Long idStatus, Long idVeiculo, Pageable pageable);

    public Page<ReservaVeiculo> findByStatusIdOrderByStart(Long idStatus, Pageable pageable);
    public Page<ReservaVeiculo> findByStatusIdOrderByStartDesc(Long idStatus, Pageable pageable);

    @Query(value = "SELECT * FROM reserva_veiculo AS RV WHERE RV.ID_STATUS = 3 ORDER BY RV.START DESC", nativeQuery = true)
    public List<ReservaVeiculo> listarReservasAprovadas();

    @Query(value = "SELECT * FROM reserva_veiculo AS RV\n" +
            "WHERE RV.ID_STATUS = 3 AND RV.ID_VEICULO = ?1 AND\n" +
            "(((?2 >= RV.START AND ?2 < RV.END)\n" +
            "OR (?3 > RV.START AND ?3 <= RV.END))\n" +
            "OR (RV.START >= ?2 AND RV.START < ?3))", nativeQuery = true)
    public List<ReservaVeiculo> verificarCompatibilidaDeHorarioReservaAprovada(Long idVeiculo, LocalDateTime start, LocalDateTime end);

    @Query(value = "SELECT * FROM reserva_veiculo AS RV\n" +
            "WHERE RV.ID_STATUS = 2 AND RV.ID_VEICULO = ?1 AND RV.ID <> ?4 AND\n" +
            "(((?2 >= RV.START AND ?2 < RV.END)\n" +
            "OR (?3 > RV.START AND ?3 <= RV.END))\n" +
            "OR (RV.START >= ?2 AND RV.START < ?3))", nativeQuery = true)
    public List<ReservaVeiculo> verificarCompatibilidaDeHorarioReservaEmAprovacao(Long idVeiculo, LocalDateTime start, LocalDateTime end, Long idReserva);

    @Query(value = "SELECT * FROM reserva_veiculo AS RV \n" +
            "WHERE RV.ID_USUARIO = ?5 AND\n" +
            "(CONVERT(RV.START, DATE) >= ?1 AND CONVERT(RV.END, DATE) <= ?2)\n" +
            "AND IF(?3 IS NOT NULL, RV.ID_VEICULO = ?3, TRUE)\n" +
            "AND IF(?4 IS NOT NULL, RV.ID_STATUS = ?4, TRUE)", nativeQuery = true)
    public List<ReservaVeiculo> relatorioMinhasReservas(LocalDate inicio, LocalDate fim, Long idVeiculo, Long idStatus, Long idUsuario);

    @Query(value = "SELECT * FROM reserva_veiculo AS RV \n" +
            "WHERE RV.ID_STATUS = 3\n" +
            "AND (CONVERT(RV.START, DATE) >= ?1 AND CONVERT(RV.END, DATE) <= ?2)\n" +
            "AND IF(?3 IS NOT NULL, RV.ID_VEICULO = ?3, TRUE)\n" +
            "AND IF(?4 IS NOT NULL, RV.ID_USUARIO = ?4, TRUE)\n" +
            "AND IF(?5 IS NOT NULL, RV.ID_MOTORISTA = ?5, TRUE)", nativeQuery = true)
    public List<ReservaVeiculo> relatorioReservasAprovadas(LocalDate inicio, LocalDate fim, Long idVeiculo, Long idUsuario, Long idMotorista);

    @Modifying
    @Transactional
    @Query(value = "UPDATE reserva_veiculo SET ID_STATUS = 6 WHERE ID_STATUS IN (1,2) AND `START` <= NOW()", nativeQuery = true)
    public void expirarReservas();

    @Query(value = "SELECT * FROM reserva_veiculo WHERE ID_STATUS IN (1,2) AND `START` <= NOW()", nativeQuery = true)
    public List<ReservaVeiculo> findReservasExpiradas();
}
