package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.Motorista;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MotoristaRepository extends JpaRepository<Motorista, Long> {

    Motorista findByCpf(String cpf);

    @Query(value = "SELECT * FROM motorista AS m WHERE m.cpf = ?1 AND m.id <> ?2", nativeQuery = true)
    public Motorista findByCpfUpdate(String cpf, Long id);

    @Query(value = "SELECT * FROM motorista AS m WHERE m.ativo = 1", nativeQuery = true)
    public List<Motorista> motoristasAtivos();
}
