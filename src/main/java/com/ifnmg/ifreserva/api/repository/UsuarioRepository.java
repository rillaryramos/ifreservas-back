package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.TipoUsuario;
import com.ifnmg.ifreserva.api.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

//    public Usuario findByEmail(String email);

    public Optional<Usuario> findByEmail(String email);

    public Optional<Usuario> findByLogin(String login);

    @Query(value = "SELECT * FROM usuario AS u WHERE u.role = 'ROLE_MASTER'", nativeQuery = true)
    public Usuario findByUserMaster();

    @Query(value = "SELECT * FROM usuario AS u WHERE u.role <> 'ROLE_MASTER'", nativeQuery = true)
    public List<Usuario> findUsers();

    @Query(value = "SELECT * FROM usuario AS u WHERE u.role = 'ROLE_APPROVER'", nativeQuery = true)
    public List<Usuario> findAprovadoresAtivos();

    @Query(value = "SELECT * FROM usuario AS u WHERE u.ativo = 1", nativeQuery = true)
    public List<Usuario> usuariosAtivos();

//    @Transactional
//    @Modifying
//    @Query(value = "UPDATE USUARIO AS U SET ROLE = ?2 WHERE U.ROLE = 'MASTER';\n" +
//            " UPDATE USUARIO AS U SET ROLE = 'MASTER' WHERE U.ID = 1;", nativeQuery = true)
//    public void trocarUserMaster(Long idNovoMaster, String novaRole);
}
