package com.ifnmg.ifreserva.api.repository;

import com.ifnmg.ifreserva.api.model.Bloco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BlocoRepository extends JpaRepository<Bloco, Long> {

    public Bloco findByNome(String nome);

    @Query(value = "SELECT * FROM bloco AS b WHERE b.nome = ?1 AND b.id <> ?2", nativeQuery = true)
    public Bloco findByNomeUpdate(String nome, Long id);

}
