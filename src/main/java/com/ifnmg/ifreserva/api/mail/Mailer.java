package com.ifnmg.ifreserva.api.mail;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.ifnmg.ifreserva.api.model.RecuperacaoSenha;
import com.ifnmg.ifreserva.api.model.ReservaDependencia;
import com.ifnmg.ifreserva.api.model.ReservaVeiculo;
import com.ifnmg.ifreserva.api.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Component
public class Mailer {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private TemplateEngine thymeleaf;

//	@EventListener
//	private void teste(ApplicationReadyEvent event) {
//		this.enviarEmail("ifreservasinformativo@gmail.com",
//				Arrays.asList("rerr@aluno.ifnmg.edu.br"),
//				"Testando", "Olá!<br/>Teste ok.");
//		System.out.println("Terminado o envio de e-mail...");
//	}

    public void boasVindas(String login, String senha, String email) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("login", login);
        variaveis.put("senha", senha);
        this.enviarEmail("ifreservasinformativo@gmail.com", Arrays.asList(email), "Boas vindas - IF Reservas", "mail/boas-vindas", variaveis);
    }

    public void enviarParaAprovacao(ReservaVeiculo reservaVeiculo, List<Usuario> destinatarios) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("veiculo", reservaVeiculo.getVeiculo().getModelo() + " - " + reservaVeiculo.getVeiculo().getMarca() + " - " + reservaVeiculo.getVeiculo().getPlaca());
        variaveis.put("solicitante", reservaVeiculo.getUsuario().getNome() + " " + reservaVeiculo.getUsuario().getSobrenome());
        variaveis.put("start", reservaVeiculo.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("end", reservaVeiculo.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("saida", reservaVeiculo.getSaida());
        variaveis.put("destino", reservaVeiculo.getDestino());
        variaveis.put("motivo", reservaVeiculo.getMotivo());

        List<String> emails = destinatarios.stream()
                .map(u -> u.getEmail())
                .collect(Collectors.toList());
        this.enviarEmail("ifreservasinformativo@gmail.com", emails, "Nova aprovação pendente", "mail/enviar-para-aprovacao", variaveis);
    }

    public void reprovacaoReserva(ReservaVeiculo reservaVeiculo, Usuario usuarioLogado) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("veiculo", reservaVeiculo.getVeiculo().getModelo());
        variaveis.put("start", reservaVeiculo.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("end", reservaVeiculo.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("saida", reservaVeiculo.getSaida());
        variaveis.put("destino", reservaVeiculo.getDestino());
        variaveis.put("motivo", reservaVeiculo.getMotivoRejeicao());
        variaveis.put("administrador", usuarioLogado.getNome() + " " + usuarioLogado.getSobrenome());
        this.enviarEmail("ifreservasinformativo@gmail.com", Arrays.asList(reservaVeiculo.getUsuario().getEmail()), "Reserva de veículo reprovada", "mail/reprovacao-reserva", variaveis);
    }

    public void aprovacaoReserva(ReservaVeiculo reservaVeiculo, Usuario usuarioLogado) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("veiculo", reservaVeiculo.getVeiculo().getModelo());
        variaveis.put("start", reservaVeiculo.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("end", reservaVeiculo.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("saida", reservaVeiculo.getSaida());
        variaveis.put("destino", reservaVeiculo.getDestino());
        variaveis.put("motivo", reservaVeiculo.getMotivo());
        variaveis.put("motorista", reservaVeiculo.getMotorista() != null ? reservaVeiculo.getMotorista().getNome() + " " + reservaVeiculo.getMotorista().getSobrenome() : "");
        variaveis.put("administrador", usuarioLogado.getNome() + " " + usuarioLogado.getSobrenome());
        this.enviarEmail("ifreservasinformativo@gmail.com", Arrays.asList(reservaVeiculo.getUsuario().getEmail()), "Reserva de veículo aprovada", "mail/aprovacao-reserva", variaveis);
    }

    public void expiracaoReserva(ReservaVeiculo reservaVeiculo) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("veiculo", reservaVeiculo.getVeiculo().getModelo());
        variaveis.put("start", reservaVeiculo.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("end", reservaVeiculo.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("saida", reservaVeiculo.getSaida());
        variaveis.put("destino", reservaVeiculo.getDestino());
        variaveis.put("motivo", reservaVeiculo.getMotivo());
        this.enviarEmail("ifreservasinformativo@gmail.com", Arrays.asList(reservaVeiculo.getUsuario().getEmail()), "Reserva de veículo expirada", "mail/expirando-reserva-veiculo", variaveis);
    }

    public void cancelamentoReservaAprovadaAdministrador(ReservaVeiculo reservaVeiculo, Usuario usuarioLogado) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("veiculo", reservaVeiculo.getVeiculo().getModelo());
        variaveis.put("start", reservaVeiculo.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("end", reservaVeiculo.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("saida", reservaVeiculo.getSaida());
        variaveis.put("destino", reservaVeiculo.getDestino());
        variaveis.put("motivo", reservaVeiculo.getMotivoRejeicao());
        variaveis.put("motorista", reservaVeiculo.getMotorista() != null ? reservaVeiculo.getMotorista().getNome() + " " + reservaVeiculo.getMotorista().getSobrenome() : "");
        variaveis.put("administrador", usuarioLogado.getNome() + " " + usuarioLogado.getSobrenome());
        this.enviarEmail("ifreservasinformativo@gmail.com", Arrays.asList(reservaVeiculo.getUsuario().getEmail()), "Reserva de veículo cancelada", "mail/cancelamento-reserva", variaveis);
    }

    public void cancelamentoReservaAprovadaUsuario(ReservaVeiculo reservaVeiculo, List<Usuario> destinatarios) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("servidor", reservaVeiculo.getUsuario().getNome() + " " + reservaVeiculo.getUsuario().getSobrenome());
        variaveis.put("veiculo", reservaVeiculo.getVeiculo().getModelo());
        variaveis.put("start", reservaVeiculo.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("end", reservaVeiculo.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("saida", reservaVeiculo.getSaida());
        variaveis.put("destino", reservaVeiculo.getDestino());
        variaveis.put("motorista", reservaVeiculo.getMotorista() != null ? reservaVeiculo.getMotorista().getNome() + " " + reservaVeiculo.getMotorista().getSobrenome() : "");
        variaveis.put("motivo", reservaVeiculo.getMotivoRejeicao());
        List<String> emails = destinatarios.stream()
                .map(u -> u.getEmail())
                .collect(Collectors.toList());
        this.enviarEmail("ifreservasinformativo@gmail.com", emails, "Reserva de veículo cancelada", "mail/cancelamento-reserva-usuario", variaveis);
    }

    public void cancelamentoReservaDependencia(ReservaDependencia reservaDependencia, String horarios) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("dependencia", reservaDependencia.getDependencia().getNome());
        variaveis.put("bloco", reservaDependencia.getDependencia().getBloco().getNome());
        variaveis.put("start", horarios != null ? reservaDependencia.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) : reservaDependencia.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("end", horarios != null ? reservaDependencia.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) : reservaDependencia.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("horarios", horarios);
        this.enviarEmail("ifreservasinformativo@gmail.com", Arrays.asList(reservaDependencia.getUsuarioUtilizador().getEmail()), "Reserva de dependência cancelada", "mail/cancelamento-reserva-dependencia", variaveis);
    }

    public void alteracaoReserva(ReservaVeiculo reservaVeiculo, Usuario usuarioLogado) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("veiculo", reservaVeiculo.getVeiculo().getModelo());
        variaveis.put("start", reservaVeiculo.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("end", reservaVeiculo.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        variaveis.put("saida", reservaVeiculo.getSaida());
        variaveis.put("destino", reservaVeiculo.getDestino());
        variaveis.put("motivo", reservaVeiculo.getMotivo());
        variaveis.put("motorista", reservaVeiculo.getMotorista() != null ? reservaVeiculo.getMotorista().getNome() + " " + reservaVeiculo.getMotorista().getSobrenome() : "");
        variaveis.put("administrador", usuarioLogado.getNome() + " " + usuarioLogado.getSobrenome());
        this.enviarEmail("ifreservasinformativo@gmail.com", Arrays.asList(reservaVeiculo.getUsuario().getEmail()), "Motorista da reserva de veículo alterado", "mail/alteracao-reserva", variaveis);
    }

    public void enviarLinkParaAlteracaoDeSenha(RecuperacaoSenha recuperacaoSenha) {
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("url", "http://localhost:4200/recuperar-senha/" + recuperacaoSenha.getToken());
        this.enviarEmail("ifreservasinformativo@gmail.com", Arrays.asList(recuperacaoSenha.getUsuario().getEmail()), "Alteração de senha - IF Reservas", "mail/recuperacao-senha", variaveis);
    }

    public void enviarEmail(String remetente,
                            List<String> destinatarios, String assunto, String template,
                            Map<String, String> variaveis) {
        Context context = new Context(new Locale("pt", "BR"));
        variaveis.entrySet().forEach(
                e -> context.setVariable(e.getKey(), e.getValue()));
        String mensagem = thymeleaf.process(template, context);
        this.enviarEmail(remetente, destinatarios, assunto, mensagem);
    }

    public void enviarEmail(String remetente,
                            List<String> destinatarios, String assunto, String mensagem) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "UTF-8");
            helper.setFrom(remetente);
            helper.setTo(destinatarios.toArray(new String[destinatarios.size()]));
            helper.setSubject(assunto);
            helper.setText(mensagem, true);

            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            throw new RuntimeException("Problemas com o envio de e-mail!", e);
        }
    }
}
