package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.sendgrid.EmailData;

public interface ISendgridService {

    public void sendEmail(EmailData emailData);

}
