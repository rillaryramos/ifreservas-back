package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.model.Status;
import com.ifnmg.ifreserva.api.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatusService {

    @Autowired
    private StatusRepository statusRepository;

    public Status buscarPorNome(String nome) {
        return statusRepository.findByNome(nome);
    }

    public Status buscarStatusPeloId(Long id) {
        return statusRepository.findOne(id);
    }

}
