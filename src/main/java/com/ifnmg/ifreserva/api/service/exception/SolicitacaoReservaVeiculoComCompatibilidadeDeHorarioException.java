package com.ifnmg.ifreserva.api.service.exception;

import com.ifnmg.ifreserva.api.model.ReservaVeiculo;

import java.util.List;

public class SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    public List<ReservaVeiculo> reservas;

    public SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException(List<ReservaVeiculo> reservasVeiculo) {
        this.reservas = reservasVeiculo;
    }

}
