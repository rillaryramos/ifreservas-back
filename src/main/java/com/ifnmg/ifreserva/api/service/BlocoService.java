package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.model.Bloco;
import com.ifnmg.ifreserva.api.repository.BlocoRepository;
import com.ifnmg.ifreserva.api.service.exception.NomeBlocoJaCadastradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class BlocoService {

    @Autowired
    private BlocoRepository blocoRepository;

    public Bloco salvar(Bloco bloco) {
        if(blocoRepository.findByNome(bloco.getNome()) != null) {
            throw new NomeBlocoJaCadastradoException();
        }
        return blocoRepository.save(bloco);
    }

    public Bloco atualizar(Long id, Bloco bloco) {
        if(blocoRepository.findByNomeUpdate(bloco.getNome(), id) != null) {
            throw new NomeBlocoJaCadastradoException();
        }
        Bloco blocoSalvo = buscarBlocoPeloId(id);
        blocoSalvo.setNome(bloco.getNome());
        return blocoRepository.save(blocoSalvo);
    }

    public Bloco buscarBlocoPeloId(Long id) {
        Bloco blocoSalvo = blocoRepository.findOne(id);
        if (blocoSalvo == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return blocoSalvo;
    }
}
