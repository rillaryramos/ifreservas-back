package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.model.RecuperacaoSenha;
import com.ifnmg.ifreserva.api.repository.RecuperacaoSenhaRepository;
import com.ifnmg.ifreserva.api.service.exception.TokenInativoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
public class RecuperacaoSenhaService {

    @Autowired
    private RecuperacaoSenhaRepository recuperacaoSenhaRepository;

    public RecuperacaoSenha buscarRecuperacaoPeloToken(String token) {
        RecuperacaoSenha recuperacaoSenha = recuperacaoSenhaRepository.findByToken(token);
        return recuperacaoSenha;
    }

    public RecuperacaoSenha buscarRecuperacaoPeloIdUsuario(Long idUsuario) {
        return recuperacaoSenhaRepository.findByUsuarioId(idUsuario);
    }

    public String validarToken(String token) {
        RecuperacaoSenha recuperacaoSenha = buscarRecuperacaoPeloToken(token);
        if (recuperacaoSenha == null || recuperacaoSenha.getDataExpiracao().isBefore(LocalDateTime.now(ZoneId.of("UTC-3")))) {
            throw new TokenInativoException();
        }
        return recuperacaoSenha.getUsuario().getEmail();
    }

    public void salvar(RecuperacaoSenha recuperacaoSenha) {
        recuperacaoSenhaRepository.save(recuperacaoSenha);
    }

    public void excluir(RecuperacaoSenha recuperacaoSenha) {
        recuperacaoSenhaRepository.delete(recuperacaoSenha);
    }
}
