package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.dto.RelatorioMinhasReservasVeiculo;
import com.ifnmg.ifreserva.api.dto.RelatorioReservasVeiculoAprovadas;
import com.ifnmg.ifreserva.api.mail.Mailer;
import com.ifnmg.ifreserva.api.model.*;
import com.ifnmg.ifreserva.api.repository.ReservaVeiculoRepository;
import com.ifnmg.ifreserva.api.service.exception.*;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.sql.Date;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class ReservaVeiculoService {

    @Autowired
    private ReservaVeiculoRepository reservaVeiculoRepository;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private VeiculoService veiculoService;
    @Autowired
    private MotoristaService motoristaService;
    @Autowired
    private Mailer mailer;
    @Autowired
    private TrilhaAuditoriaService trilhaAuditoriaService;
    @Autowired
    private StatusService statusService;

    public byte[] relatorioMinhasReservas(LocalDate inicio, LocalDate fim, Long idVeiculo, Long idStatus, Usuario usuarioLogado) throws Exception {
        List<ReservaVeiculo> reservas = reservaVeiculoRepository.relatorioMinhasReservas(inicio, fim, idVeiculo, idStatus, usuarioLogado.getId());
        List<RelatorioMinhasReservasVeiculo> dados = new ArrayList<>();
        for (ReservaVeiculo reserva : reservas) {
            RelatorioMinhasReservasVeiculo relatorioMinhasReservasVeiculo = new RelatorioMinhasReservasVeiculo();
            relatorioMinhasReservasVeiculo.setDiaInicio(reserva.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
            relatorioMinhasReservasVeiculo.setDiaTermino(reserva.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
            relatorioMinhasReservasVeiculo.setVeiculo(reserva.getVeiculo().getModelo() + " | " + reserva.getVeiculo().getMarca() + " | " + reserva.getVeiculo().getPlaca());
            relatorioMinhasReservasVeiculo.setSaida(reserva.getSaida());
            relatorioMinhasReservasVeiculo.setDestino(reserva.getDestino());
            relatorioMinhasReservasVeiculo.setMotivo(reserva.getMotivo());
            relatorioMinhasReservasVeiculo.setStatus(reserva.getStatus().getNome());
            relatorioMinhasReservasVeiculo.setStart(reserva.getStart());
            dados.add(relatorioMinhasReservasVeiculo);
        }
        Collections.sort(dados, ordernarStart);
        if (dados.isEmpty()) {
            throw new RelatorioVazioException();
        }
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("DT_INICIO", Date.valueOf(inicio));
        parametros.put("DT_FIM", Date.valueOf(fim));
        parametros.put("DT_EMISSAO", LocalDateTime.now(ZoneId.of("UTC-3")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        parametros.put("SERVIDOR", usuarioLogado.getNome() + " " + usuarioLogado.getSobrenome());
        InputStream inputStream = this.getClass().getResourceAsStream("/relatorios/minhas-reservas-veiculo.jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parametros,
                new JRBeanCollectionDataSource(dados));
        return JasperExportManager.exportReportToPdf(jasperPrint);
    }

    public byte[] relatorioReservasAprovadas(LocalDate inicio, LocalDate fim, Long idVeiculo, Long idUsuario, Long idMotorista) throws Exception {
        List<ReservaVeiculo> reservas = reservaVeiculoRepository.relatorioReservasAprovadas(inicio, fim, idVeiculo, idUsuario, idMotorista);
        List<RelatorioReservasVeiculoAprovadas> dados = new ArrayList<>();
        for (ReservaVeiculo reserva : reservas) {
            RelatorioReservasVeiculoAprovadas relatorioReservasVeiculoAprovadas = new RelatorioReservasVeiculoAprovadas();
            relatorioReservasVeiculoAprovadas.setDiaInicio(reserva.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
            relatorioReservasVeiculoAprovadas.setDiaTermino(reserva.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
            relatorioReservasVeiculoAprovadas.setVeiculo(reserva.getVeiculo().getModelo() + " | " + reserva.getVeiculo().getMarca() + " | " + reserva.getVeiculo().getPlaca());
            relatorioReservasVeiculoAprovadas.setServidor(reserva.getUsuario().getNome() + " " + reserva.getUsuario().getSobrenome());
            relatorioReservasVeiculoAprovadas.setMotorista(reserva.getMotorista() != null ? reserva.getMotorista().getNome() + " " + reserva.getMotorista().getSobrenome() : "");
            relatorioReservasVeiculoAprovadas.setStart(reserva.getStart());
            dados.add(relatorioReservasVeiculoAprovadas);
        }
        Collections.sort(dados, ordernarStartAprovadas);
        if (dados.isEmpty()) {
            throw new RelatorioVazioException();
        }
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("DT_INICIO", Date.valueOf(inicio));
        parametros.put("DT_FIM", Date.valueOf(fim));
        parametros.put("DT_EMISSAO", LocalDateTime.now(ZoneId.of("UTC-3")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        InputStream inputStream = this.getClass().getResourceAsStream("/relatorios/reservas-veiculo-aprovadas.jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parametros,
                new JRBeanCollectionDataSource(dados));
        return JasperExportManager.exportReportToPdf(jasperPrint);
    }

    public ReservaVeiculo salvar(ReservaVeiculo reservaVeiculo, Usuario usuarioLogado) {
        if(!usuarioLogado.getAtivo()) {
            throw new UsuarioLogadoInativoException();
        }
        if(reservaVeiculo.getStart().isBefore(LocalDateTime.now(ZoneId.of("UTC-3")))) {
            throw new DataHorarioInicioAnteriorADataAtualException();
        }
        if(reservaVeiculo.getEnd().isBefore(reservaVeiculo.getStart())) {
            throw new DataHorarioTerminoMenorQueDataHorarioInicioException();
        }
        //ESTÁ COM PROBLEMA NA COMPATIBILIDADE. EX.: HORA_FINAL DE UMA RESERVA 10:30; HORA INICIAL DE OUTRA RESERVA 10:30
        List<ReservaVeiculo> reservasComCompatibilidadeDeHorario = reservaVeiculoRepository.verificarCompatibilidaDeHorarioReservaAprovada(reservaVeiculo.getVeiculo().getId(), reservaVeiculo.getStart(), reservaVeiculo.getEnd());
        if(!reservasComCompatibilidadeDeHorario.isEmpty()) {
            throw new SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
        }
        reservaVeiculo.setUsuario(usuarioLogado);
        Status statusAberto = new Status();
        statusAberto.setId(1l);
        reservaVeiculo.setStatus(statusAberto);
        ReservaVeiculo reservaSalva = reservaVeiculoRepository.save(reservaVeiculo);
        this.salvarTrilhaAuditoriaCadastro(reservaSalva, usuarioLogado);
        return reservaSalva;
    }

    public ReservaVeiculo atualizar(Long id, ReservaVeiculo reservaVeiculo, Usuario usuarioLogado) {
        //VERIFICAR SE O USUÁRIO ESTÁ ATIVO
        if(reservaVeiculo.getStart().isBefore(LocalDateTime.now(ZoneId.of("UTC-3")))) {
            throw new DataHorarioInicioAnteriorADataAtualException();
        }
        if(reservaVeiculo.getEnd().isBefore(reservaVeiculo.getStart())) {
            throw new DataHorarioTerminoMenorQueDataHorarioInicioException();
        }
        List<ReservaVeiculo> reservasComCompatibilidadeDeHorario = reservaVeiculoRepository.verificarCompatibilidaDeHorarioReservaAprovada(reservaVeiculo.getVeiculo().getId(), reservaVeiculo.getStart(), reservaVeiculo.getEnd());
        if(!reservasComCompatibilidadeDeHorario.isEmpty()) {
            throw new SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
        }
        ReservaVeiculo reservaAntiga = new ReservaVeiculo();
        ReservaVeiculo reservaSalva = buscarReservaPeloId(id);
        BeanUtils.copyProperties(reservaSalva, reservaAntiga);
        reservaSalva.setTitle(reservaVeiculo.getTitle());
        reservaSalva.setStart(reservaVeiculo.getStart());
        reservaSalva.setEnd(reservaVeiculo.getEnd());
        reservaSalva.setSaida(reservaVeiculo.getSaida());
        reservaSalva.setDestino(reservaVeiculo.getDestino());
        reservaSalva.setMotivo(reservaVeiculo.getMotivo());
        ReservaVeiculo reserva = reservaVeiculoRepository.save(reservaSalva);
        this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reserva, usuarioLogado);
        return reserva;
    }

    public void salvarTrilhaAuditoriaCadastro(ReservaVeiculo reservaSalva, Usuario usuarioLogado) {
        String descricao = "";
        descricao += "Título: " + reservaSalva.getTitle() + "; ";
        Veiculo veiculo = veiculoService.buscarVeiculoPeloId(reservaSalva.getVeiculo().getId());
        descricao += "Veículo: " + veiculo.getModelo() + " - " + veiculo.getMarca() + " - " + veiculo.getPlaca() + "; ";
        descricao += "Início: " + reservaSalva.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "; ";
        descricao += "Término: " + reservaSalva.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "; ";
        descricao += "Saída: " + reservaSalva.getSaida() + "; ";
        descricao += "Destino: " + reservaSalva.getDestino() + "; ";
        descricao += "Motivo: " + reservaSalva.getMotivo() + "; ";
        Status status = statusService.buscarStatusPeloId(reservaSalva.getStatus().getId());
        descricao += "Status: " + status.getNome() + ".";
        trilhaAuditoriaService.salvar(descricao, "Cadastro", TipoTrilhaAuditoria.RESERVA_VEICULO, reservaSalva.getId(), usuarioLogado);
    }

    public void salvarTrilhaAuditoriaEdicao(ReservaVeiculo reservaAntiga, ReservaVeiculo reservaSalva, Usuario usuarioLogado) {
        String descricao = "";
        if (!reservaAntiga.getTitle().equals(reservaSalva.getTitle())) {
            descricao += "Título: " + reservaSalva.getTitle() + "; ";
        }
        if (!reservaAntiga.getStart().equals(reservaSalva.getStart())) {
            descricao += "Início: " + reservaSalva.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "; ";
        }
        if (!reservaAntiga.getEnd().equals(reservaSalva.getEnd())) {
            descricao += "Término: " + reservaSalva.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "; ";
        }
        if (!reservaAntiga.getSaida().equals(reservaSalva.getSaida())) {
            descricao += "Saída: " + reservaSalva.getSaida() + "; ";
        }
        if (!reservaAntiga.getDestino().equals(reservaSalva.getDestino())) {
            descricao += "Destino: " + reservaSalva.getDestino() + "; ";
        }
        if (!reservaAntiga.getMotivo().equals(reservaSalva.getMotivo())) {
            descricao += "Motivo: " + reservaSalva.getMotivo() + "; ";
        }
        if (reservaAntiga.getMotorista() != reservaSalva.getMotorista()) {
            if (reservaSalva.getMotorista() != null) {
                Motorista motorista = motoristaService.buscarMotoristaPeloId(reservaSalva.getMotorista().getId());
                descricao += "Motorista: " + motorista.getNome() + " " + motorista.getSobrenome() + "; ";
            } else {
                descricao += "Motorista: " + " ; ";
            }
        }
        if (reservaAntiga.getMotivoRejeicao() != (reservaSalva.getMotivoRejeicao())) {
            descricao += "Motivo reprovação/cancelamento: " + reservaSalva.getMotivoRejeicao() + "; ";
        }
        if (reservaAntiga.getStatus().getId() != reservaSalva.getStatus().getId()) {
            Status status = statusService.buscarStatusPeloId(reservaSalva.getStatus().getId());
            descricao += "Status: " + status.getNome() + ".";
        }
        trilhaAuditoriaService.salvar(descricao, "Edição", TipoTrilhaAuditoria.RESERVA_VEICULO, reservaSalva.getId(), usuarioLogado);
    }

    public List<ReservaVeiculo> listarReservasPorVeiculo(Long idVeiculo, Usuario usuarioLogado, String tipo) {
        Veiculo veiculo = veiculoService.buscarVeiculoPeloId(idVeiculo);
        List<ReservaVeiculo> reservas = null;
        if (tipo.equals("all")) {
            reservas = reservaVeiculoRepository.listarReservasPorVeiculo(veiculo.getId());
        } else {
            reservas = reservaVeiculoRepository.listarMinhasReservasPorVeiculo(veiculo.getId(), usuarioLogado.getId());
        }
        return reservas;
    }

    public List<ReservaVeiculo> verificarCompatibilidadeReservasEmAprovacao(Long idReserva, LocalDateTime start, LocalDateTime end) {
        ReservaVeiculo reservaVeiculo = buscarReservaPeloId(idReserva);
        List<ReservaVeiculo> reservasAprovadasComCompatibilidadeDeHorario = reservaVeiculoRepository.verificarCompatibilidaDeHorarioReservaAprovada(reservaVeiculo.getVeiculo().getId(), start, end);
        if(!reservasAprovadasComCompatibilidadeDeHorario.isEmpty()) {
            throw new SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException(reservasAprovadasComCompatibilidadeDeHorario);
        }
        List<ReservaVeiculo> reservasEmAprovacaoComCompatibilidadeDeHorario = reservaVeiculoRepository.verificarCompatibilidaDeHorarioReservaEmAprovacao(reservaVeiculo.getVeiculo().getId(), start, end, reservaVeiculo.getId());
        return reservasEmAprovacaoComCompatibilidadeDeHorario;
    }

    public ReservaVeiculo cancelarReserva(Long idReserva, Usuario usuarioLogado) {
        ReservaVeiculo reservaAntiga = new ReservaVeiculo();
        ReservaVeiculo reservaSalva = buscarReservaPeloId(idReserva);
        BeanUtils.copyProperties(reservaSalva, reservaAntiga);
        Status statusCancelada = new Status();
        statusCancelada.setId(4l);
        reservaSalva.setStatus(statusCancelada);
        ReservaVeiculo reserva = reservaVeiculoRepository.save(reservaSalva);
        this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reserva, usuarioLogado);
        return reserva;
    }

    public ReservaVeiculo cancelarReservaAprovada(Long idReserva, String motivoCancelamento, Usuario usuarioLogado) {
        //disparar exception se o usuário logado não for o usuário da reserva ou se não for aprovador de veículo
        ReservaVeiculo reservaAntiga = new ReservaVeiculo();
        ReservaVeiculo reservaSalva = buscarReservaPeloId(idReserva);
        BeanUtils.copyProperties(reservaSalva, reservaAntiga);
        Status statusCancelada = new Status();
        statusCancelada.setId(5l);
        reservaSalva.setStatus(statusCancelada);
        reservaSalva.setMotivoRejeicao(motivoCancelamento);
        if (usuarioLogado.getId() != reservaSalva.getUsuario().getId()) {
            reservaSalva.setAdministradorReserva(usuarioLogado);
            mailer.cancelamentoReservaAprovadaAdministrador(reservaSalva, usuarioLogado);
        } else {
            List<Usuario> usuariosAprovadores = usuarioService.findAprovadoresAtivos();
            if (!usuariosAprovadores.isEmpty()) {
                mailer.cancelamentoReservaAprovadaUsuario(reservaSalva, usuariosAprovadores);
            }
        }
        ReservaVeiculo reserva = reservaVeiculoRepository.save(reservaSalva);
        this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reserva, usuarioLogado);
        return reserva;
    }

    public ReservaVeiculo reprovarReserva(Long idReserva, String motivoReprovacao, Usuario usuarioLogado) {
        ReservaVeiculo reservaAntiga = new ReservaVeiculo();
        ReservaVeiculo reservaSalva = buscarReservaPeloId(idReserva);
        BeanUtils.copyProperties(reservaSalva, reservaAntiga);
        reservaSalva.setAdministradorReserva(usuarioLogado);
        Status statusReprovada = new Status();
        statusReprovada.setId(4l);
        reservaSalva.setStatus(statusReprovada);
        reservaSalva.setMotivoRejeicao(motivoReprovacao);
        mailer.reprovacaoReserva(reservaSalva, usuarioLogado);
        ReservaVeiculo reserva = reservaVeiculoRepository.save(reservaSalva);
        this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reserva, usuarioLogado);
        return reserva;
    }

    public ReservaVeiculo aprovarReserva(Long idReserva, AprovacaoReservaVeiculoRequest aprovacaoReservaVeiculoRequest, Usuario usuarioLogado) {
        ReservaVeiculo reservaAntiga = new ReservaVeiculo();
        ReservaVeiculo reservaSalva = buscarReservaPeloId(idReserva);
        if (!reservaSalva.getVeiculo().getAtivo()) {
            throw new VeiculoInativoException();
        }
        BeanUtils.copyProperties(reservaSalva, reservaAntiga);
        reservaSalva.setAdministradorReserva(usuarioLogado);
        Status statusAprovada = new Status();
        statusAprovada.setId(3l);
        reservaSalva.setStatus(statusAprovada);
        reservaSalva.setStart(aprovacaoReservaVeiculoRequest.getReservaParaAprovar().getStart());
        reservaSalva.setEnd(aprovacaoReservaVeiculoRequest.getReservaParaAprovar().getEnd());
        reservaSalva.setMotorista(aprovacaoReservaVeiculoRequest.getReservaParaAprovar().getMotorista());
        mailer.aprovacaoReserva(reservaSalva, usuarioLogado);
        if(!aprovacaoReservaVeiculoRequest.getReservasParaReprovar().isEmpty()) {
            for (ReservaVeiculo reservaVeiculo : aprovacaoReservaVeiculoRequest.getReservasParaReprovar()) {
                reprovarReserva(reservaVeiculo.getId(), "Veículo já está reservado", usuarioLogado);
            }
        }
        ReservaVeiculo reserva = reservaVeiculoRepository.save(reservaSalva);
        this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reserva, usuarioLogado);
        return reserva;
    }

    public ReservaVeiculo alterarMotorista(Long idReserva, String idMotorista, Usuario usuarioLogado) {
        ReservaVeiculo reservaAntiga = new ReservaVeiculo();
        ReservaVeiculo reservaSalva = buscarReservaPeloId(idReserva);
        BeanUtils.copyProperties(reservaSalva, reservaAntiga);
        Motorista motoristaAntigo = reservaSalva.getMotorista();
//        SÓ PERMITIR SE A RESERVA ESTIVER APROVADA
        if (!idMotorista.equals("null")) {
            Motorista motorista = motoristaService.buscarMotoristaPeloId(Long.parseLong(idMotorista));
            reservaSalva.setMotorista(motorista);
        } else {
            reservaSalva.setMotorista(null);
        }
        if (motoristaAntigo != reservaSalva.getMotorista()) {
            mailer.alteracaoReserva(reservaSalva, usuarioLogado);
        }
        ReservaVeiculo reserva = reservaVeiculoRepository.save(reservaSalva);
        this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reserva, usuarioLogado);
        return reserva;
    }

    public ReservaVeiculo atualizarPropriedadeStatus(Long idReserva, Long idStatus, Usuario usuarioLogado) {
        ReservaVeiculo reservaAntiga = new ReservaVeiculo();
        ReservaVeiculo reservaSalva = buscarReservaPeloId(idReserva);
        BeanUtils.copyProperties(reservaSalva, reservaAntiga);
        List<Usuario> usuariosAprovadores = usuarioService.findAprovadoresAtivos();
        List<Usuario> usuariosAtivos = new ArrayList<>();
        if (idStatus == 2) {
            if (!reservaSalva.getVeiculo().getAtivo()) {
                throw new VeiculoInativoException();
            }
            List<ReservaVeiculo> reservasComCompatibilidadeDeHorario = reservaVeiculoRepository.verificarCompatibilidaDeHorarioReservaAprovada(reservaSalva.getVeiculo().getId(), reservaSalva.getStart(), reservaSalva.getEnd());
            if(!reservasComCompatibilidadeDeHorario.isEmpty()) {
                throw new SolicitacaoReservaVeiculoComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
            }
            if (usuariosAprovadores.isEmpty()) {
                throw new SemUsuariosAprovadoresException();
            } else {
                for (Usuario usuario : usuariosAprovadores) {
                    if (usuario.getAtivo()) {
                        usuariosAtivos.add(usuario);
                    }
                }
                if (usuariosAtivos.isEmpty()) {
                    throw new SemUsuariosAprovadoresAtivoException();
                } else {
                    reservaSalva.setDataEnvioAprovacao(LocalDateTime.now(ZoneId.of("UTC-3")));
                }
            }
        } else {
            reservaSalva.setDataEnvioAprovacao(null);
        }
        Status status = new Status();
        status.setId(idStatus);
        reservaSalva.setStatus(status);
        ReservaVeiculo reservaVeiculo = reservaVeiculoRepository.save(reservaSalva);
        if (idStatus == 2) {
            mailer.enviarParaAprovacao(reservaSalva, usuariosAprovadores);
        }
        this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reservaVeiculo, usuarioLogado);
        return reservaVeiculo;
    }

    public ReservaVeiculo buscarReservaPeloId(Long id) {
        ReservaVeiculo reservaSalva = reservaVeiculoRepository.findOne(id);
        if (reservaSalva == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return reservaSalva;
    }

    private final Comparator<RelatorioMinhasReservasVeiculo> ordernarStart = new Comparator<RelatorioMinhasReservasVeiculo>() {
        @Override
        public int compare(final RelatorioMinhasReservasVeiculo a, final RelatorioMinhasReservasVeiculo b) {
            try {
                final LocalDateTime valorA = a.getStart();
                final LocalDateTime valorB = b.getStart();
                if(valorA.compareTo(valorB) == 0) {
                    return 0;
                }

                return valorA.compareTo(valorB) < 0 ? -1 : 1;
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return -1;
        }
    };

    private final Comparator<RelatorioReservasVeiculoAprovadas> ordernarStartAprovadas = new Comparator<RelatorioReservasVeiculoAprovadas>() {
        @Override
        public int compare(final RelatorioReservasVeiculoAprovadas a, final RelatorioReservasVeiculoAprovadas b) {
            try {
                final LocalDateTime valorA = a.getStart();
                final LocalDateTime valorB = b.getStart();
                if(valorA.compareTo(valorB) == 0) {
                    return 0;
                }

                return valorA.compareTo(valorB) < 0 ? -1 : 1;
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return -1;
        }
    };
}
