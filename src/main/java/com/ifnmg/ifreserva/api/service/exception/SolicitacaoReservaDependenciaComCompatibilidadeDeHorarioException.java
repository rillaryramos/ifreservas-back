package com.ifnmg.ifreserva.api.service.exception;

import com.ifnmg.ifreserva.api.model.ReservaDependencia;

import java.util.List;

public class SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    public List<ReservaDependencia> reservas;

    public SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(List<ReservaDependencia> reservasDependencia) {
        this.reservas = reservasDependencia;
    }

}
