package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.dto.RelatorioReservaDependencia;
import com.ifnmg.ifreserva.api.mail.Mailer;
import com.ifnmg.ifreserva.api.model.*;
import com.ifnmg.ifreserva.api.repository.ReservaDependenciaRepository;
import com.ifnmg.ifreserva.api.service.exception.*;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.InputStream;
import java.time.*;
import java.sql.Date;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class ReservaDependenciaService {

    @Autowired
    private ReservaDependenciaRepository reservaDependenciaRepository;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private DependenciaService dependenciaService;
    @Autowired
    private TurmaService turmaService;
    @Autowired
    private StatusService statusService;
    @Autowired
    private Mailer mailer;
    @Autowired
    private TrilhaAuditoriaService trilhaAuditoriaService;
//    @Autowired
//    private EntityManager entityManager;

    public byte[] relatorioDependencia(LocalDate inicio, LocalDate fim, Long idDependencia, Long idUsuario, Long idTurma, String tipoReserva) throws Exception {
        List<ReservaDependencia> reservas = reservaDependenciaRepository.relatorio(inicio, fim, idDependencia, idUsuario, idTurma, tipoReserva);
        List<RelatorioReservaDependencia> dados = new ArrayList<>();
        for (ReservaDependencia reserva : reservas) {
            if (reserva.getUsoContinuo()) {
                LocalDateTime horarioInicio = reserva.getStart();
                Boolean continua = true;
                if (reserva.getStart().toLocalDate().isBefore(inicio)) {
                    List<HorarioReserva> novosHorarios = new ArrayList<>();
                    LocalTime horarioZerado = LocalTime.of(00,00,00);
                    LocalDateTime novoHorarioInicio = LocalDateTime.of(inicio, horarioZerado);
                    while(!novoHorarioInicio.toLocalDate().isAfter(fim) && novosHorarios.isEmpty()) {
                        for (HorarioReserva hr : reserva.getHorarioReserva()) {
                            if (hr.getDiaSemana().toUpperCase().equals((novoHorarioInicio.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                novosHorarios.add(hr);
                            }
                        }
                        if (novosHorarios.isEmpty()) {
                            novoHorarioInicio = novoHorarioInicio.plusDays(1);
                        }
                    }
                    if (!novosHorarios.isEmpty()) {
                        Collections.sort(novosHorarios, ordernarHorarioInicial);
                        LocalTime novo = novosHorarios.get(0).getHorarioInicio();
                        novoHorarioInicio.withHour(novo.getHour()).withMinute(novo.getMinute()).withSecond(00).withNano(00);
                        horarioInicio = novoHorarioInicio;
                    } else {
                        continua = false;
                    }
                }
                if (continua) {
                    for (HorarioReserva horario : reserva.getHorarioReserva()) {
                        LocalDateTime horarioInicial = horarioInicio;
                        while (horarioInicial.isBefore(reserva.getEnd()) && !horarioInicial.toLocalDate().isAfter(fim)) { //VERIFICAR
                            if (horario.getDiaSemana().toUpperCase().equals(horarioInicial.toLocalDate().getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                System.out.println("Passou horário inicio: " + horarioInicial);
                                RelatorioReservaDependencia relatorioReservaDependencia = new RelatorioReservaDependencia();
                                relatorioReservaDependencia.setAula(reserva.getAula() ? "Sim" : "Não");
                                relatorioReservaDependencia.setDependencia(reserva.getDependencia().getNome() + " - " + reserva.getDependencia().getBloco().getNome());
                                relatorioReservaDependencia.setUtilizador(reserva.getUsuarioUtilizador() != null ? reserva.getUsuarioUtilizador().getNome() + " " + reserva.getUsuarioUtilizador().getSobrenome() : reserva.getTurma().getNome());
                                relatorioReservaDependencia.setDiaHorario(horarioInicial.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")));
                                relatorioReservaDependencia.setStart(horarioInicial.withHour(horario.getHorarioInicio().getHour()).withMinute(horario.getHorarioInicio().getMinute()).withSecond(00).withNano(00));
                                dados.add(relatorioReservaDependencia);
                            }
                            horarioInicial = horarioInicial.plusDays(1);
                        }
                    }
                }
            } else {
                RelatorioReservaDependencia relatorioReservaDependencia = new RelatorioReservaDependencia();
                relatorioReservaDependencia.setAula(reserva.getAula() ? "Sim" : "Não");
                relatorioReservaDependencia.setDependencia(reserva.getDependencia().getNome() + " - " + reserva.getDependencia().getBloco().getNome());
                relatorioReservaDependencia.setUtilizador(reserva.getUsuarioUtilizador() != null ? reserva.getUsuarioUtilizador().getNome() + " " + reserva.getUsuarioUtilizador().getSobrenome() : reserva.getTurma().getNome());
                relatorioReservaDependencia.setStart(reserva.getStart());
                relatorioReservaDependencia.setDiaHorario(reserva.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + reserva.getStart().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + reserva.getEnd().format(DateTimeFormatter.ofPattern("HH:mm")));
                dados.add(relatorioReservaDependencia);
            }
        }
        Collections.sort(dados, ordernarStart);
        if (dados.isEmpty()) {
            System.out.println("ENTROU AQUI");
            throw new RelatorioVazioException();
        }

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("DT_INICIO", Date.valueOf(inicio));
        parametros.put("DT_FIM", Date.valueOf(fim));
        parametros.put("DT_EMISSAO", LocalDateTime.now(ZoneId.of("UTC-3")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
//        parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));

        InputStream inputStream = this.getClass().getResourceAsStream("/relatorios/reservas-dependencia.jasper");


        JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parametros,
                new JRBeanCollectionDataSource(dados));

        return JasperExportManager.exportReportToPdf(jasperPrint);
    }

//    @Transactional
    public ReservaDependencia salvarReservaUsoBreve(ReservaDependencia reservaDependencia, Usuario usuarioLogado) {
        if(reservaDependencia.getStart().isBefore(LocalDateTime.now(ZoneId.of("UTC-3")))) {
            throw new DataHorarioInicioAnteriorADataAtualException();
        }
        String diaSemana = (reservaDependencia.getStart().getDayOfWeek()).toString().substring(0,3).toUpperCase();
        List<ReservaDependencia> reservasComCompatibilidadeDeHorario = reservaDependenciaRepository.verificarCompatibilidaDeHorarioReservaUsoBreve(reservaDependencia.getDependencia().getId(), reservaDependencia.getStart(), reservaDependencia.getEnd(), diaSemana);
        if (!reservasComCompatibilidadeDeHorario.isEmpty()) {
            if (!reservaDependencia.getAula()) {
                throw new SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
            } else {
                Boolean compatibilidadeComAula = false;
                for (ReservaDependencia reserva : reservasComCompatibilidadeDeHorario) {
                    if (reserva.getAula()) {
                        compatibilidadeComAula = true;
                        throw new SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
                    }
                }
                if (!compatibilidadeComAula) {
                    for (ReservaDependencia reserva : reservasComCompatibilidadeDeHorario) {
                        this.cancelarReserva(reserva.getId(), usuarioLogado, reservaDependencia.getStart(), true, reservaDependencia.getEnd(), reservaDependencia, reserva);
                    }
                }
            }
        }
        reservaDependencia.setUsuarioSolicitante(usuarioLogado);
        reservaDependencia.setUsuarioUtilizador(usuarioLogado);
        reservaDependencia.setUsoContinuo(false);
        Status statusAberto = new Status();
        statusAberto.setId(1l);
        reservaDependencia.setStatus(statusAberto);
        ReservaDependencia reservaSalva = reservaDependenciaRepository.save(reservaDependencia);
//        entityManager.refresh(reservaSalva);
        this.salvarTrilhaAuditoriaCadastro(reservaSalva, usuarioLogado);
        return reservaSalva;
    }

    public void salvarTrilhaAuditoriaCadastro(ReservaDependencia reservaSalva, Usuario usuarioLogado) {
        String descricao = "";
        descricao += "Título: " + reservaSalva.getTitle() + "; ";
        Dependencia dependencia = dependenciaService.buscarDependenciaPeloId(reservaSalva.getDependencia().getId());
        descricao += "Dependência: " + dependencia.getNome() + " - " + dependencia.getBloco().getNome() + "; ";
        if (reservaSalva.getUsuarioUtilizador() != null) {
            Usuario utilizador = usuarioService.buscarUsuarioPeloId(reservaSalva.getUsuarioUtilizador().getId());
            descricao += "Utilizador: " + utilizador.getLogin() + "; ";
        } else {
            Turma turma = turmaService.buscarTurmaPeloId(reservaSalva.getTurma().getId());
            descricao += "Utilizador: " + turma.getNome() + "; ";
        }
        if (reservaSalva.getUsoContinuo()) {
            descricao += "Início: " + reservaSalva.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "; ";
            descricao += "Término: " + reservaSalva.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "; ";
            descricao += "Horários da reserva:";
            for (HorarioReserva horario : reservaSalva.getHorarioReserva()) {
                descricao += " " + horario.getDiaSemana() + " - " + horario.getHorarioInicio() + " - " + horario.getHorarioTermino() + " |";
            }
            descricao += "; ";
        } else {
            descricao += "Início: " + reservaSalva.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "; ";
            descricao += "Término: " + reservaSalva.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "; ";
        }
        if (reservaSalva.getAula()) {
            descricao += "Aula: sim; ";
        } else {
            descricao += "Aula: não; ";
        }
        Status status = statusService.buscarStatusPeloId(reservaSalva.getStatus().getId());
        descricao += "Status: " + status.getNome() + "; ";
        if (reservaSalva.getObservacao() != null) {
            descricao += "Observação: " + reservaSalva.getObservacao() + "; ";
        } else {
            descricao += "Observação: ; ";
        }
        trilhaAuditoriaService.salvar(descricao, "Cadastro", TipoTrilhaAuditoria.RESERVA_DEPENDENCIA, reservaSalva.getId(), usuarioLogado);
    }

    public void salvarTrilhaAuditoriaEdicao(ReservaDependencia reservaAntiga, ReservaDependencia reservaSalva, Usuario usuarioLogado) {
        String descricao = "";
        if (!reservaAntiga.getTitle().equals(reservaSalva.getTitle())) {
            descricao += "Título: " + reservaSalva.getTitle() + "; ";
        }
        if (reservaSalva.getUsuarioUtilizador() != null) {
            if (reservaAntiga.getUsuarioUtilizador().getId() != reservaSalva.getUsuarioUtilizador().getId()) {
                Usuario utilizador = usuarioService.buscarUsuarioPeloId(reservaSalva.getUsuarioUtilizador().getId());
                descricao += "Utilizador: " + utilizador.getLogin() + "; ";
            }
        }
        if (reservaSalva.getTurma() != null) {
            if (reservaAntiga.getTurma().getId() != reservaSalva.getTurma().getId()) {
                Turma turma = turmaService.buscarTurmaPeloId(reservaSalva.getTurma().getId());
                descricao += "Utilizador: " + turma.getNome() + "; ";
            }
        }
        if (reservaSalva.getUsoContinuo()) {
            if (!reservaAntiga.getStart().equals(reservaSalva.getStart())) {
                descricao += "Início: " + reservaSalva.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "; ";
            }
            if (!reservaAntiga.getEnd().equals(reservaSalva.getEnd())) {
                descricao += "Término: " + reservaSalva.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "; ";
            }
            Boolean alteracao = false;
            if (reservaSalva.getHorarioReserva().size() == reservaAntiga.getHorarioReserva().size()) {
                int i = 0;
                for (HorarioReserva horario : reservaSalva.getHorarioReserva()) {
                    for (HorarioReserva horarioAntigo : reservaAntiga.getHorarioReserva()) {
                        if (horario.getDiaSemana().equals(horarioAntigo.getDiaSemana())
                                && horario.getHorarioInicio().equals(horarioAntigo.getHorarioInicio())
                                && horario.getHorarioTermino().equals(horarioAntigo.getHorarioTermino())) {
                            i++;
                            break;
                        }
                    }
                }
                if (i != reservaSalva.getHorarioReserva().size()) {
                    alteracao = true;
                }
                if (!alteracao) {
                    int j = 0;
                    for (HorarioReserva horarioAntigo : reservaAntiga.getHorarioReserva()) {
                        Boolean continuar = true;
                        for (HorarioReserva horario : reservaSalva.getHorarioReserva()) {
                            if (horario.getDiaSemana().equals(horarioAntigo.getDiaSemana())
                                    && horario.getHorarioInicio().equals(horarioAntigo.getHorarioInicio())
                                    && horario.getHorarioTermino().equals(horarioAntigo.getHorarioTermino())) {
                                j++;
                                break;
                            }
                        }
                    }
                    if (j != reservaAntiga.getHorarioReserva().size()) {
                        alteracao = true;
                    }
                }
            } else {
                alteracao = true;
            }
            if (alteracao) {
                descricao += "Horários da reserva:";
                for (HorarioReserva horario : reservaSalva.getHorarioReserva()) {
                    descricao += " " + horario.getDiaSemana() + " - " + horario.getHorarioInicio() + " - " + horario.getHorarioTermino() + " |";
                }
                descricao += "; ";
            }
        } else {
            if (!reservaAntiga.getStart().equals(reservaSalva.getStart())) {
                descricao += "Início: " + reservaSalva.getStart().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "; ";
            }
            if (!reservaAntiga.getEnd().equals(reservaSalva.getEnd())) {
                descricao += "Término: " + reservaSalva.getEnd().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "; ";
            }
        }
        if (reservaAntiga.getStatus().getId() != reservaSalva.getStatus().getId()) {
            Status status = statusService.buscarStatusPeloId(reservaSalva.getStatus().getId());
            descricao += "Status: " + status.getNome() + "; ";
        }
        if (reservaAntiga.getObservacao() == null) {
            if (reservaSalva.getObservacao() != null) {
                descricao += "Observação: " + reservaSalva.getObservacao() + "; ";
            }
        } else if (!reservaAntiga.getObservacao().equals(reservaSalva.getObservacao())){
            descricao += "Observação: " + reservaSalva.getObservacao() + "; ";
        }
        trilhaAuditoriaService.salvar(descricao, "Edição", TipoTrilhaAuditoria.RESERVA_DEPENDENCIA, reservaSalva.getId(), usuarioLogado);
    }

    public ReservaDependenciaRequest salvarVariasReservasUsoBreve(ReservaDependenciaRequest reservasDependencia, Usuario usuarioLogado) {
        for (ReservaDependencia reservaDependencia : reservasDependencia.getReservas()) {
            if(reservaDependencia.getStart().isBefore(LocalDateTime.now(ZoneId.of("UTC-3")))) {
                throw new DataHorarioInicioAnteriorADataAtualException();
            }
            String diaSemana = (reservaDependencia.getStart().getDayOfWeek()).toString().substring(0,3).toUpperCase();
            List<ReservaDependencia> reservasComCompatibilidadeDeHorario = reservaDependenciaRepository.verificarCompatibilidaDeHorarioReservaUsoBreve(reservaDependencia.getDependencia().getId(), reservaDependencia.getStart(), reservaDependencia.getEnd(), diaSemana);
            if(!reservasComCompatibilidadeDeHorario.isEmpty()) {
                throw new SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
            }
        }
        for (ReservaDependencia reservaDependencia : reservasDependencia.getReservas()) {
            reservaDependencia.setUsuarioSolicitante(usuarioLogado);
            if (!reservaDependencia.getAula()) {
                reservaDependencia.setUsuarioUtilizador(usuarioLogado);
            }
            reservaDependencia.setUsoContinuo(false);
            Status statusAberto = new Status();
            statusAberto.setId(1l);
            reservaDependencia.setStatus(statusAberto);
            reservaDependenciaRepository.save(reservaDependencia);
        }
        return reservasDependencia;
    }

    public ReservaDependencia salvarReservaUsoContinuo(ReservaDependencia reservaDependencia, Usuario usuarioLogado, Boolean verificarCompatibilidade, Boolean salvarStatusAberto, Boolean pegarUsuarioLogado, Boolean verificarHorarioInicial) {
        if (verificarHorarioInicial) {
            System.out.println(ChronoUnit.DAYS.between(reservaDependencia.getStart().toLocalDate(), reservaDependencia.getEnd().toLocalDate()));
            if (reservaDependencia.getStart().toLocalDate().equals(LocalDate.now(ZoneId.of("UTC-3")))) {
                if (ChronoUnit.DAYS.between(reservaDependencia.getStart().toLocalDate(), reservaDependencia.getEnd().toLocalDate()) <= 6) {
                    for (HorarioReserva horario : reservaDependencia.getHorarioReserva()) {
                        if (horario.getDiaSemana().toUpperCase().equals(reservaDependencia.getStart().getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                            if (horario.getHorarioInicio().isBefore(LocalTime.now(ZoneId.of("UTC-3")))) {
                                throw new DataHorarioInicioAnteriorADataAtualUsoContinuoException();
                            }
                        }
                    }
                }
            }
        }
        ReservaDependencia reservaHorariosAdequados = this.adequarHorario(reservaDependencia);
        reservaDependencia.setStart(reservaHorariosAdequados.getStart());
        reservaDependencia.setEnd(reservaHorariosAdequados.getEnd());
        if (verificarCompatibilidade) {
            this.verificarCompatibilidadeHorario(reservaDependencia, usuarioLogado, true);
        }
        if (pegarUsuarioLogado) {
            reservaDependencia.setUsuarioSolicitante(usuarioLogado);
            if (!reservaDependencia.getAula()) {
                reservaDependencia.setUsuarioUtilizador(usuarioLogado);
            }
        }
        reservaDependencia.setUsoContinuo(true);
        if (salvarStatusAberto) {
            Status statusAberto = new Status();
            statusAberto.setId(1l);
            reservaDependencia.setStatus(statusAberto);
        }
        reservaDependencia.getHorarioReserva().forEach(hr -> hr.setReservaDependencia(reservaDependencia));
        ReservaDependencia reservaSalva = reservaDependenciaRepository.save(reservaDependencia);
        this.salvarTrilhaAuditoriaCadastro(reservaSalva, usuarioLogado);
        return reservaSalva;
    }

    public ReservaDependencia adequarHorario(ReservaDependencia reservaDependencia) {
        LocalDate diaAtual = LocalDate.now(ZoneId.of("UTC-3"));
        LocalTime horarioAtual = LocalTime.now(ZoneId.of("UTC-3"));
        LocalDateTime diasParaPercorrer = reservaDependencia.getStart();
        reservaDependencia.setEnd(reservaDependencia.getEnd().withHour(23).withMinute(59).withSecond(59).withNano(00));
        for (int i = 0; i <= 7; i++) {
            LocalDateTime dia = diasParaPercorrer.plusDays(i);

            if (!dia.isBefore(reservaDependencia.getStart()) && !dia.toLocalDate().isAfter(reservaDependencia.getEnd().toLocalDate())) {
                System.out.println("Dia: " + dia);
                String diaSemana = dia.getDayOfWeek().toString().substring(0, 3).toUpperCase();
                List<HorarioReserva> horarioReservas = new ArrayList<>();
                for (HorarioReserva horarioReserva : reservaDependencia.getHorarioReserva()) {
                    if (horarioReserva.getDiaSemana().toUpperCase().equals(diaSemana)) {
                        horarioReservas.add(horarioReserva);
                    }
                }
                if (!horarioReservas.isEmpty()) {
                    if (dia.toLocalDate().equals(diaAtual)) {
                        List<HorarioReserva> horariosParaOrdenar = new ArrayList<>();
                        for (HorarioReserva horario : horarioReservas) {
                            if (!horario.getHorarioInicio().isBefore(horarioAtual)) {
                                horariosParaOrdenar.add(horario);
                            }
                        }
                        if (!horariosParaOrdenar.isEmpty()) {
                            Collections.sort(horariosParaOrdenar, ordernarHorarioInicial);
                            LocalTime menorHorario = horariosParaOrdenar.get(0).getHorarioInicio();
                            reservaDependencia.setStart(reservaDependencia.getStart().withHour(menorHorario.getHour()).withMinute(menorHorario.getMinute()).withSecond(00).withNano(00));
                            break;
                        }
                    } else {
                        Collections.sort(horarioReservas, ordernarHorarioInicial);
                        LocalTime menorHorario = horarioReservas.get(0).getHorarioInicio();
                        reservaDependencia.setStart(dia.withHour(menorHorario.getHour()).withMinute(menorHorario.getMinute()).withSecond(00).withNano(00));
                        break;
                    }
                }
            }
        }
        return reservaDependencia;
    }

    public void verificarCompatibilidadeHorario(ReservaDependencia reservaDependencia, Usuario usuarioLogado, Boolean novaReserva) {
        reservaDependencia.getHorarioReserva().forEach(hr -> {
            List<ReservaDependencia> reservasComCompatibilidadeDeHorario;
            if (novaReserva) {
                reservasComCompatibilidadeDeHorario = reservaDependenciaRepository.verificarCompatibilidaDeHorarioReservaUsoContinuo(reservaDependencia.getDependencia().getId(), reservaDependencia.getStart(), reservaDependencia.getEnd(), hr.getDiaSemana(), hr.getHorarioInicio(), hr.getHorarioTermino());
//                reservasComCompatibilidadeDeHorario = reservaDependenciaRepository.verificarCompatibilidaDeHorarioReservaUsoContinuo(reservaDependencia.getDependencia().getId(), reservaDependencia.getStart(), reservaDependencia.getStart().withHour(hr.getHorarioInicio().getHour()).withMinute(hr.getHorarioInicio().getMinute()).withSecond(00).withNano(00), hr.getDiaSemana(), hr.getHorarioInicio(), hr.getHorarioTermino());
            } else {
                reservasComCompatibilidadeDeHorario = reservaDependenciaRepository.verificarCompatibilidaDeHorarioReservaUsoContinuoEdicao(reservaDependencia.getDependencia().getId(), reservaDependencia.getStart(), reservaDependencia.getEnd(), hr.getDiaSemana(), hr.getHorarioInicio(), hr.getHorarioTermino(), reservaDependencia.getId());
//                reservasComCompatibilidadeDeHorario = reservaDependenciaRepository.verificarCompatibilidaDeHorarioReservaUsoContinuoEdicao(reservaDependencia.getDependencia().getId(), reservaDependencia.getStart(), reservaDependencia.getStart().withHour(hr.getHorarioInicio().getHour()).withMinute(hr.getHorarioInicio().getMinute()).withSecond(00).withNano(00), hr.getDiaSemana(), hr.getHorarioInicio(), hr.getHorarioTermino(), reservaDependencia.getId());
            }
            if (!reservasComCompatibilidadeDeHorario.isEmpty()) {
                if (!reservaDependencia.getAula()) {
                    throw new SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
                } else {
                    Boolean compatibilidadeComAula = false;
                    for (ReservaDependencia reserva : reservasComCompatibilidadeDeHorario) {
                        if (reserva.getAula()) {
                            compatibilidadeComAula = true;
                            throw new SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
                        }
                    }
                    if (!compatibilidadeComAula) {
                        for (ReservaDependencia reserva : reservasComCompatibilidadeDeHorario) {
                            System.out.println("ENTROU");
                            this.cancelarReserva(reserva.getId(), usuarioLogado, reservaDependencia.getStart(), true, null, reservaDependencia, reserva);
                        }
//                        if (reservaDependencia.getStart().toLocalDate().equals(LocalDate.now(ZoneId.of("UTC-3")))) {
//                            LocalTime horasAtual = LocalTime.now(ZoneId.of("UTC-3"));
//                            reservaDependencia.setStart(reservaDependencia.getStart().withHour(horasAtual.getHour()).withMinute(horasAtual.getMinute()).withSecond(00).withNano(00));
//                        }
                    }
                }
            }
        });
    }

    public ReservaDependencia atualizarReservaUsoBreve(Long id, ReservaDependencia reservaDependencia, Usuario usuarioLogado) {
        ReservaDependencia reservaAntiga = new ReservaDependencia();
        ReservaDependencia reservaSalva = buscarPeloId(id);
        BeanUtils.copyProperties(reservaSalva, reservaAntiga);
        String diaSemana = (reservaDependencia.getStart().getDayOfWeek()).toString().substring(0,3).toUpperCase();
        List<ReservaDependencia> reservasComCompatibilidadeDeHorario = reservaDependenciaRepository.verificarCompatibilidaDeHorarioReservaUsoBreveEdicao(reservaDependencia.getDependencia().getId(), reservaDependencia.getStart(), reservaDependencia.getEnd(), diaSemana, reservaSalva.getId());
        if (!reservasComCompatibilidadeDeHorario.isEmpty()) {
            if (!reservaDependencia.getAula()) {
                throw new SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
            } else {
                Boolean compatibilidadeComAula = false;
                for (ReservaDependencia reserva : reservasComCompatibilidadeDeHorario) {
                    if (reserva.getAula()) {
                        compatibilidadeComAula = true;
                        throw new SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
                    }
                }
                if (!compatibilidadeComAula) {
                    for (ReservaDependencia reserva : reservasComCompatibilidadeDeHorario) {
                        this.cancelarReserva(reserva.getId(), usuarioLogado, reservaDependencia.getStart(), true, reservaDependencia.getEnd(), reservaDependencia, reserva);
                    }
                }
            }
        }
        if (!reservaDependencia.getAula()) {
            reservaDependencia.setUsuarioUtilizador(reservaSalva.getUsuarioUtilizador());
        }
        BeanUtils.copyProperties(reservaDependencia, reservaSalva, "id", "dependencia", "usuarioSolicitante", "turma", "usoContinuo", "aula", "horarioReserva", "status");
        ReservaDependencia reserva = reservaDependenciaRepository.save(reservaSalva);
        this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reserva, usuarioLogado);
        return reserva;
    }

    public ReservaDependencia atualizarReservaUsoContinuo(Long id, ReservaDependencia reservaDependencia, Usuario usuarioLogado) {
        ReservaDependencia reservaAntiga = new ReservaDependencia();
        ReservaDependencia reservaSalva = buscarPeloId(id);
        List<HorarioReserva> horariosAntigos = new ArrayList<>();
        BeanUtils.copyProperties(reservaSalva, reservaAntiga, "horarioReserva");
        for (HorarioReserva horarioReserva : reservaSalva.getHorarioReserva()) {
            HorarioReserva novoHorario = new HorarioReserva();
            novoHorario.setDiaSemana(horarioReserva.getDiaSemana());
            novoHorario.setHorarioInicio(horarioReserva.getHorarioInicio());
            novoHorario.setHorarioTermino(horarioReserva.getHorarioTermino());
            horariosAntigos.add(novoHorario);
        }
        reservaAntiga.setHorarioReserva(horariosAntigos);
        if (!reservaSalva.getStart().isAfter(LocalDateTime.now(ZoneId.of("UTC-3")))) { //VERIFICAR SE ALTERO PRA NEGAÇÃO DE ISAFTER
//            Dispara exception se a reservaSalva.getStart() === reservaDependencia.getStart()
            if(reservaDependencia.getStart().toLocalDate().equals(LocalDate.now(ZoneId.of("UTC-3")))) {
                List<HorarioReserva> horarios = new ArrayList<>();
                reservaSalva.getHorarioReserva().forEach(hr ->  {
                    if (hr.getDiaSemana().toUpperCase().equals((LocalDateTime.now(ZoneId.of("UTC-3")).getDayOfWeek()).toString().substring(0,3).toUpperCase())) {
                        if (!hr.getHorarioInicio().isAfter(LocalTime.now(ZoneId.of("UTC-3")))) {
                            horarios.add(hr);
                        }
                    }
                } );
                if(!horarios.isEmpty()) {
                    Collections.sort(horarios, ordernarHorarioFinal);
                    LocalTime maiorHorario = horarios.get(horarios.size() - 1).getHorarioTermino();
                    reservaSalva.setEnd(LocalDateTime.now(ZoneId.of("UTC-3")).withHour(maiorHorario.getHour()).withMinute(maiorHorario.getMinute()).withSecond(maiorHorario.getSecond()).withNano(maiorHorario.getNano()));
                    reservaDependencia.setStart(reservaDependencia.getStart().withHour(maiorHorario.getHour()).withMinute(maiorHorario.getMinute() + 1).withSecond(maiorHorario.getSecond()).withNano(maiorHorario.getNano()));
                } else {
                    reservaSalva.setEnd(reservaDependencia.getStart().minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(00));
                }
            } else {
                reservaSalva.setEnd(reservaDependencia.getStart().minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(00));
            }
            ReservaDependencia reservaAlterada = reservaDependenciaRepository.save(reservaSalva);
            this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reservaAlterada, usuarioLogado);
            reservaDependencia.setId(null);
            ReservaDependencia reserva = salvarReservaUsoContinuo(reservaDependencia, usuarioLogado, true, true, true, true);
            return reserva;
        } else {
            ReservaDependencia reservaHorariosAdequados = this.adequarHorario(reservaDependencia);
            reservaDependencia.setStart(reservaHorariosAdequados.getStart());
            reservaDependencia.setEnd(reservaHorariosAdequados.getEnd());
            this.verificarCompatibilidadeHorario(reservaDependencia, usuarioLogado, false);
            if (!reservaDependencia.getAula()) {
                reservaDependencia.setUsuarioUtilizador(reservaSalva.getUsuarioUtilizador());
            }
            reservaSalva.getHorarioReserva().clear();
            reservaSalva.getHorarioReserva().addAll(reservaDependencia.getHorarioReserva());
            reservaSalva.getHorarioReserva().forEach(hr -> hr.setReservaDependencia(reservaSalva));

            BeanUtils.copyProperties(reservaDependencia, reservaSalva, "id", "dependencia", "usuarioSolicitante", "turma", "usoContinuo", "aula", "horarioReserva", "status");
            ReservaDependencia reserva = reservaDependenciaRepository.save(reservaSalva);
            this.salvarTrilhaAuditoriaEdicao(reservaAntiga, reserva, usuarioLogado);
            return reserva;
        }
    }

    public ReservaDependencia cancelarReserva(Long id, Usuario usuarioLogado, LocalDateTime dataCancelamento, Boolean cancelarPorAula, LocalDateTime dataCancelamentoFim, ReservaDependencia reservaAula, ReservaDependencia reservaCompatibilidade) {
        ReservaDependencia reservaSemAlteracao = new ReservaDependencia();
        ReservaDependencia reservaSalva = buscarPeloId(id);
        BeanUtils.copyProperties(reservaSalva, reservaSemAlteracao);
        ReservaDependencia reservaCancelada = null;
        Status statusCancelado = new Status();
        statusCancelado.setId(5l);
        Boolean exclusao = false;
        if (!reservaSalva.getUsoContinuo()) {
            reservaSalva.setStatus(statusCancelado);
            reservaCancelada = reservaDependenciaRepository.save(reservaSalva);
            if (cancelarPorAula) {
                mailer.cancelamentoReservaDependencia(reservaSalva, null);
            }
        } else {
            if (reservaAula != null) {
                List<HorarioReserva> horariosComCompatibilidade = new ArrayList<>();
                List<HorarioReserva> horariosSemCompatibilidade = new ArrayList<>();
                if (reservaCompatibilidade.getUsoContinuo()) {
                    if (reservaAula.getUsoContinuo()) {
                        for (HorarioReserva horario : reservaCompatibilidade.getHorarioReserva()) {
                            Boolean entrou = false;
                            for (HorarioReserva horarioNovo : reservaAula.getHorarioReserva()) {
                                if (horario.getDiaSemana().equals(horarioNovo.getDiaSemana())
                                        && (((!horario.getHorarioInicio().isBefore(horarioNovo.getHorarioInicio()) && horario.getHorarioInicio().isBefore(horarioNovo.getHorarioTermino()))
                                        || (horario.getHorarioTermino().isAfter(horarioNovo.getHorarioInicio()) && !horario.getHorarioTermino().isAfter(horarioNovo.getHorarioTermino())))
                                        || (!horarioNovo.getHorarioInicio().isBefore(horario.getHorarioInicio()) && horarioNovo.getHorarioInicio().isBefore(horario.getHorarioTermino())))) {
                                    entrou = true;
                                    HorarioReserva horarioReserva = new HorarioReserva();
                                    horarioReserva.setDiaSemana(horario.getDiaSemana());
                                    horarioReserva.setHorarioInicio(horario.getHorarioInicio());
                                    horarioReserva.setHorarioTermino(horario.getHorarioTermino());
                                    horariosComCompatibilidade.add(horarioReserva); //com compatibilidade
                                }
                            }

                            if (!entrou) {
                                //sem compatibilidade
                                HorarioReserva horarioReserva = new HorarioReserva();
                                horarioReserva.setDiaSemana(horario.getDiaSemana());
                                horarioReserva.setHorarioInicio(horario.getHorarioInicio());
                                horarioReserva.setHorarioTermino(horario.getHorarioTermino());
                                horariosSemCompatibilidade.add(horarioReserva);
                            }
                        }
                    } else {
                        for (HorarioReserva horario : reservaCompatibilidade.getHorarioReserva()) {
                            Boolean entrou = false;
                            if (horario.getDiaSemana().toUpperCase().equals((reservaAula.getStart().toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())
                                    && (((!horario.getHorarioInicio().isBefore(reservaAula.getStart().toLocalTime()) && horario.getHorarioInicio().isBefore(reservaAula.getEnd().toLocalTime()))
                                    || (horario.getHorarioTermino().isAfter(reservaAula.getStart().toLocalTime()) && !horario.getHorarioTermino().isAfter(reservaAula.getEnd().toLocalTime())))
                                    || (!reservaAula.getStart().toLocalTime().isBefore(horario.getHorarioInicio()) && reservaAula.getStart().toLocalTime().isBefore(horario.getHorarioTermino())))) {
                                entrou = true;
                                HorarioReserva horarioReserva = new HorarioReserva();
                                horarioReserva.setDiaSemana(horario.getDiaSemana());
                                horarioReserva.setHorarioInicio(horario.getHorarioInicio());
                                horarioReserva.setHorarioTermino(horario.getHorarioTermino());
                                horariosComCompatibilidade.add(horarioReserva); //com compatibilidade
                            }

                            if (!entrou) {
                                //sem compatibilidade
                                HorarioReserva horarioReserva = new HorarioReserva();
                                horarioReserva.setDiaSemana(horario.getDiaSemana());
                                horarioReserva.setHorarioInicio(horario.getHorarioInicio());
                                horarioReserva.setHorarioTermino(horario.getHorarioTermino());
                                horariosSemCompatibilidade.add(horarioReserva);
                            }
                        }
                    }
                    ReservaDependencia novaReservaComCompatibilidade = new ReservaDependencia();
                    BeanUtils.copyProperties(reservaCompatibilidade, novaReservaComCompatibilidade, "id", "start", "end", "horarioReserva");
                    novaReservaComCompatibilidade.setStart(reservaAula.getStart().withHour(07).withMinute(15).withSecond(00).withNano(00));
                    if (reservaCompatibilidade.getEnd().toLocalDate().isAfter(reservaAula.getEnd().toLocalDate())) {
                        novaReservaComCompatibilidade.setEnd(reservaAula.getEnd().withHour(23).withMinute(59).withSecond(59).withNano(00));
                    } else {
                        novaReservaComCompatibilidade.setEnd(reservaCompatibilidade.getEnd().withHour(23).withMinute(59).withSecond(59).withNano(00));
                    }
                    novaReservaComCompatibilidade.setHorarioReserva(horariosComCompatibilidade);
                    novaReservaComCompatibilidade.setStatus(statusCancelado);
                    ReservaDependencia nova = this.salvarReservaUsoContinuo(novaReservaComCompatibilidade, usuarioLogado, false, false, false, false);
                    String descricaoDomingo = "";
                    int entrouDomingo = 0;
                    String descricaoSegunda = "";
                    int entrouSegunda = 0;
                    String descricaoTerca = "";
                    int entrouTerca = 0;
                    String descricaoQuarta = "";
                    int entrouQuarta = 0;
                    String descricaoQuinta = "";
                    int entrouQuinta = 0;
                    String descricaoSexta = "";
                    int entrouSexta = 0;
                    String descricaoSabado = "";
                    String descricaoHorarios = "";
                    int entrouSabado = 0;
                    for (HorarioReserva horario : nova.getHorarioReserva()) {
                        if (horario.getDiaSemana().equals("Sun")) {
                            if (entrouDomingo == 0) {
                                descricaoDomingo += "Domingo: " + horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            } else {
                                descricaoDomingo += horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            }
                            entrouDomingo += 1;
                        } else if (horario.getDiaSemana().equals("Mon")) {
                            if (entrouSegunda == 0) {
                                descricaoSegunda += "Segunda: " + horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            } else {
                                descricaoSegunda += horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            }
                            entrouSegunda += 1;
                        } else if (horario.getDiaSemana().equals("Tue")) {
                            if (entrouTerca == 0) {
                                descricaoTerca += "Terça: " + horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            } else {
                                descricaoTerca += horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            }
                            entrouTerca += 1;
                        } else if (horario.getDiaSemana().equals("Wed")) {
                            if (entrouQuarta == 0) {
                                descricaoQuarta += "Quarta: " + horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            } else {
                                descricaoQuarta += horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            }
                            entrouQuarta += 1;
                        } else if (horario.getDiaSemana().equals("Thu")) {
                            if (entrouQuinta == 0) {
                                descricaoQuinta += "Quinta: " + horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            } else {
                                descricaoQuinta += horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            }
                            entrouQuinta += 1;
                        } else if (horario.getDiaSemana().equals("Fri")) {
                            if (entrouSexta == 0) {
                                descricaoSexta += "Sexta: " + horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            } else {
                                descricaoSexta += horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            }
                            entrouSexta += 1;
                        } else {
                            if (entrouSabado == 0) {
                                descricaoSabado += "Sábado: " + horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            } else {
                                descricaoSabado += horario.getHorarioInicio().format(DateTimeFormatter.ofPattern("HH:mm")) + " - " + horario.getHorarioTermino().format(DateTimeFormatter.ofPattern("HH:mm")) + "; ";
                            }
                            entrouSabado += 1;
                        }
                    }
                    descricaoHorarios += descricaoDomingo + "" + descricaoSegunda + "" + descricaoTerca + "" + descricaoQuarta + "" + descricaoQuinta + "" + descricaoSexta + "" + descricaoSabado;
                    mailer.cancelamentoReservaDependencia(novaReservaComCompatibilidade, descricaoHorarios);

                    ReservaDependencia novaReservaSemCompatibilidade = new ReservaDependencia();
                    BeanUtils.copyProperties(reservaCompatibilidade, novaReservaSemCompatibilidade, "id", "start", "end", "horarioReserva");
                    novaReservaSemCompatibilidade.setStart(reservaAula.getStart().withHour(07).withMinute(15).withSecond(00).withNano(00));
                    if (reservaCompatibilidade.getEnd().toLocalDate().isAfter(reservaAula.getEnd().toLocalDate())) {
                        novaReservaSemCompatibilidade.setEnd(reservaAula.getEnd().withHour(23).withMinute(59).withSecond(59).withNano(00));
                    } else {
                        novaReservaSemCompatibilidade.setEnd(reservaCompatibilidade.getEnd().withHour(23).withMinute(59).withSecond(59).withNano(00));
                    }
                    LocalDateTime diasParaPercorrer1 = novaReservaSemCompatibilidade.getStart();
                    if (ChronoUnit.DAYS.between(novaReservaSemCompatibilidade.getStart().toLocalDate(), novaReservaSemCompatibilidade.getEnd().toLocalDate()) <= 6) {
                        List<HorarioReserva> novosHorarios = new ArrayList<>();
                        for (HorarioReserva horario : horariosSemCompatibilidade) {
                            for (int i = 0; i <= 7; i++) {
                                LocalDateTime dia = diasParaPercorrer1.plusDays(i);
                                if (!dia.toLocalDate().isAfter(novaReservaSemCompatibilidade.getEnd().toLocalDate())) {
                                    if (horario.getDiaSemana().toUpperCase().equals((dia.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                        novosHorarios.add(horario);
                                        break;
                                    }
                                }
                            }
                        }
                        novaReservaSemCompatibilidade.setHorarioReserva(novosHorarios);
                    } else {
                        novaReservaSemCompatibilidade.setHorarioReserva(horariosSemCompatibilidade);
                    }
                    this.salvarReservaUsoContinuo(novaReservaSemCompatibilidade, usuarioLogado, false, true, false, false);

                    if (reservaCompatibilidade.getEnd().isAfter(reservaAula.getEnd())) {
                        ReservaDependencia reservaPosCompatibilidade = new ReservaDependencia();
                        BeanUtils.copyProperties(reservaCompatibilidade, reservaPosCompatibilidade, "id", "start", "horarioReserva");
                        reservaPosCompatibilidade.setStart(reservaAula.getEnd().plusDays(1)); //verificar
                        List<HorarioReserva> horariosPosCompatibilidade = new ArrayList<>();
                        LocalDateTime diasParaPercorrer = reservaAula.getEnd().plusDays(1);
                        if (ChronoUnit.DAYS.between(reservaAula.getEnd().toLocalDate().plusDays(1), reservaSalva.getEnd().toLocalDate()) <= 6) {
                            List<HorarioReserva> horariosRemover = new ArrayList<>();
                            for (HorarioReserva horarioReserva : reservaSalva.getHorarioReserva()) {
                                Boolean entrou = false;
                                for (int i = 0; i <= 7; i++) {
                                    LocalDateTime dia = diasParaPercorrer.plusDays(i);
                                    if (!dia.toLocalDate().isAfter(reservaSalva.getEnd().toLocalDate())) {
                                        if (horarioReserva.getDiaSemana().toUpperCase().equals((dia.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                            entrou = true;
                                            break;
                                        }
                                    }
                                }
                                if (entrou) {
                                    HorarioReserva novoHorario = new HorarioReserva();
                                    novoHorario.setDiaSemana(horarioReserva.getDiaSemana());
                                    novoHorario.setHorarioInicio(horarioReserva.getHorarioInicio());
                                    novoHorario.setHorarioTermino(horarioReserva.getHorarioTermino());
                                    horariosPosCompatibilidade.add(novoHorario);
                                }
                            }

                        } else {
                            for (HorarioReserva horarioReserva : reservaCompatibilidade.getHorarioReserva()) {
                                HorarioReserva novoHorario = new HorarioReserva();
                                novoHorario.setDiaSemana(horarioReserva.getDiaSemana());
                                novoHorario.setHorarioInicio(horarioReserva.getHorarioInicio());
                                novoHorario.setHorarioTermino(horarioReserva.getHorarioTermino());
                                horariosPosCompatibilidade.add(novoHorario);
                            }
                        }
                        reservaPosCompatibilidade.setHorarioReserva(horariosPosCompatibilidade);
                        this.salvarReservaUsoContinuo(reservaPosCompatibilidade, usuarioLogado, false, true, false, true);
                    }

                    if (reservaCompatibilidade.getStart().toLocalDate().isBefore(reservaAula.getStart().toLocalDate())) {
                        reservaCompatibilidade.setEnd(reservaAula.getStart().minusDays(1));

                        LocalDateTime diasParaPercorrer2 = reservaCompatibilidade.getStart();
                        if (ChronoUnit.DAYS.between(reservaCompatibilidade.getStart().toLocalDate(), reservaCompatibilidade.getEnd().toLocalDate()) <= 6) {
                            List<HorarioReserva> horariosRemover = new ArrayList<>();
                            for (HorarioReserva horario : reservaCompatibilidade.getHorarioReserva()) {
                                Boolean entrou = false;
                                for (int i = 0; i <= 7; i++) {
                                    LocalDateTime dia = diasParaPercorrer2.plusDays(i);
                                    if (!dia.toLocalDate().isAfter(reservaCompatibilidade.getEnd().toLocalDate())) {
                                        if (horario.getDiaSemana().toUpperCase().equals((dia.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                            entrou = true;
                                            break;
                                        }
                                    }
                                }
                                if (!entrou) {
                                    horariosRemover.add(horario);
                                }
                            }
                            if (!horariosRemover.isEmpty()) {
                                for (HorarioReserva horario : horariosRemover) {
                                    reservaCompatibilidade.getHorarioReserva().remove(horario);
                                }
                            }
                        }

                        LocalDateTime diasParaPercorrer = reservaCompatibilidade.getEnd();
                        for (int i = 0; i <= 7; i++) {
                            LocalDateTime dia = diasParaPercorrer.minusDays(i);
                            Boolean entrou = false;
                            for (HorarioReserva horarioReserva : reservaCompatibilidade.getHorarioReserva()) {
                                if (!dia.toLocalDate().isBefore(reservaCompatibilidade.getStart().toLocalDate())) {
                                    if (horarioReserva.getDiaSemana().toUpperCase().equals((dia.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                        reservaCompatibilidade.setEnd(dia.withHour(23).withMinute(59).withSecond(59).withNano(00));
                                        entrou = true;
                                        break;
                                    }
                                }
                            }
                            if (entrou) {
                                break;
                            }
                        }
                        reservaCancelada = reservaDependenciaRepository.save(reservaCompatibilidade);
                    } else if (reservaCompatibilidade.getStart().toLocalDate().equals(reservaAula.getStart().toLocalDate())
                                && reservaCompatibilidade.getStart().toLocalDate().equals(LocalDate.now(ZoneId.of("UTC-3")).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                        List<HorarioReserva> horariosRemover = new ArrayList<>();
                        for (HorarioReserva horario : reservaCompatibilidade.getHorarioReserva()) {
                            if (horario.getDiaSemana().toUpperCase().equals((reservaCompatibilidade.getStart().toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                if (!horario.getHorarioInicio().isBefore(nova.getStart().toLocalTime())) {
                                    horariosRemover.add(horario);
                                }
                            } else {
                                horariosRemover.add(horario);
                            }
                        }
                        for (HorarioReserva horario : horariosRemover) {
                            reservaCompatibilidade.getHorarioReserva().remove(horario);
                        }
                        reservaCompatibilidade.setEnd(reservaCompatibilidade.getStart().withHour(23).withMinute(59).withSecond(59).withNano(00));
                        if (!reservaCompatibilidade.getHorarioReserva().isEmpty()) {
                            reservaCancelada = reservaDependenciaRepository.save(reservaCompatibilidade);
                        } else {
                            reservaDependenciaRepository.delete(reservaCompatibilidade.getId());
                            exclusao = true;
                        }
                    } else {
                        reservaDependenciaRepository.delete(reservaCompatibilidade.getId());
                        exclusao = true;
                    }
                }
//                else {
//                    reservaCompatibilidade.setStatus(statusCancelado);
//                    reservaCancelada = reservaDependenciaRepository.save(reservaCompatibilidade);
//                }

            } else {
                if (dataCancelamentoFim != null) {
                    ReservaDependencia reservaUnicaCancelar = new ReservaDependencia();
                    BeanUtils.copyProperties(reservaSalva, reservaUnicaCancelar, "id", "start", "end", "usoContinuo", "horarioReserva");
                    reservaUnicaCancelar.setUsoContinuo(false);
                    reservaUnicaCancelar.setStart(dataCancelamento);
                    reservaUnicaCancelar.setEnd(dataCancelamentoFim);
                    reservaUnicaCancelar.setStatus(statusCancelado);
                    reservaDependenciaRepository.save(reservaUnicaCancelar); //salvar na trilha de auditoria

                    if (dataCancelamentoFim.toLocalDate().isBefore(reservaSalva.getEnd().toLocalDate())) {
                        ReservaDependencia novaReserva = new ReservaDependencia();
                        List<HorarioReserva> horarios = new ArrayList<>();
                        BeanUtils.copyProperties(reservaSalva, novaReserva, "id", "horarioReserva");
                        for (HorarioReserva horarioReserva : reservaSalva.getHorarioReserva()) {
                            HorarioReserva novoHorario = new HorarioReserva();
                            Boolean continuar = true;
                            if (horarioReserva.getDiaSemana().toUpperCase().equals((dataCancelamento.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())
                                    && horarioReserva.getHorarioInicio().equals(dataCancelamento.toLocalTime()) && horarioReserva.getHorarioTermino().equals(dataCancelamentoFim.toLocalTime())) {
                                LocalDate diasParaPercorrer = dataCancelamento.toLocalDate();
                                LocalDate dia = diasParaPercorrer.plusDays(7);
                                if (dia.isAfter(reservaSalva.getEnd().toLocalDate())) {
                                    continuar = false;
                                }
                            }

                            if (continuar) {
                                novoHorario.setDiaSemana(horarioReserva.getDiaSemana());
                                novoHorario.setHorarioInicio(horarioReserva.getHorarioInicio());
                                novoHorario.setHorarioTermino(horarioReserva.getHorarioTermino());
                                horarios.add(novoHorario);
                            }

                        }
                        novaReserva.setHorarioReserva(horarios);
                        novaReserva.getHorarioReserva().forEach(hr -> hr.setReservaDependencia(novaReserva));
                        novaReserva.setStart(dataCancelamentoFim.plusDays(1));
                        this.salvarReservaUsoContinuo(novaReserva, usuarioLogado, false, true, false, true);
                    }

                    if (reservaSalva.getStart().toLocalDate().equals(dataCancelamento.toLocalDate())) {
                        if (reservaSalva.getStart().equals(dataCancelamento)) {
                            reservaDependenciaRepository.delete(reservaSalva.getId());
                            exclusao = true;
                        } else {
                            reservaSalva.setEnd(dataCancelamento.withHour(23).withMinute(59).withSecond(59));
                            List<HorarioReserva> horariosRemover = new ArrayList<>();
                            for (HorarioReserva horarioReserva : reservaSalva.getHorarioReserva()) {
                                if (horarioReserva.getDiaSemana().toUpperCase().equals((dataCancelamento.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                    if (horarioReserva.getHorarioInicio().equals(dataCancelamento.toLocalTime()) && horarioReserva.getHorarioTermino().equals(dataCancelamentoFim.toLocalTime())) {
                                        horariosRemover.add(horarioReserva);
                                    }
                                } else {
                                    horariosRemover.add(horarioReserva);
                                }
                            }
                            for (HorarioReserva horario : horariosRemover) {
                                reservaSalva.getHorarioReserva().remove(horario);
                            }
                            reservaCancelada = reservaDependenciaRepository.save(reservaSalva);
                        }
                    } else {
                        int qtdHorarios = 0;
                        for (HorarioReserva horario : reservaSalva.getHorarioReserva()) {
                            if (horario.getDiaSemana().toUpperCase().equals((dataCancelamento.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                qtdHorarios += 1;
                            }
                        }

                        if (qtdHorarios > 1) {
                            ReservaDependencia reservaDiaCancelamento = new ReservaDependencia();
                            BeanUtils.copyProperties(reservaSalva, reservaDiaCancelamento, "id", "start", "end", "horarioReserva");
                            reservaDiaCancelamento.setStart(dataCancelamento.withHour(07).withMinute(15).withSecond(00).withNano(00));
                            reservaDiaCancelamento.setEnd(dataCancelamento.withHour(23).withMinute(59).withSecond(59));
                            List<HorarioReserva> horariosDiaCancelamento = new ArrayList<>();
                            for (HorarioReserva horario : reservaSalva.getHorarioReserva()) {
                                if (horario.getDiaSemana().toUpperCase().equals((dataCancelamento.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())
                                        && !horario.getHorarioInicio().equals(dataCancelamento.toLocalTime()) && !horario.getHorarioTermino().equals(dataCancelamentoFim.toLocalTime())) {
                                    HorarioReserva horarioReserva = new HorarioReserva();
                                    horarioReserva.setDiaSemana(horario.getDiaSemana());
                                    horarioReserva.setHorarioInicio(horario.getHorarioInicio());
                                    horarioReserva.setHorarioTermino(horario.getHorarioTermino());
                                    horariosDiaCancelamento.add(horarioReserva);
                                }
                            }
                            reservaDiaCancelamento.setHorarioReserva(horariosDiaCancelamento);
                            this.salvarReservaUsoContinuo(reservaDiaCancelamento, usuarioLogado, false, true, false, true);
                        }

                        reservaSalva.setEnd(dataCancelamento.minusDays(1));
                        LocalDate diasParaPercorrer = dataCancelamento.toLocalDate();
                        LocalDate dia = diasParaPercorrer.plusDays(7);
                        LocalDate diasAtras = diasParaPercorrer.minusDays(7);
                        if (diasAtras.isBefore(reservaSalva.getStart().toLocalDate())) {
                            if (dia.isAfter(reservaSalva.getEnd().toLocalDate())) {
                                for (HorarioReserva horarioReserva : reservaSalva.getHorarioReserva()) {
                                    if (horarioReserva.getDiaSemana().toUpperCase().equals((dataCancelamento.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())
                                            && horarioReserva.getHorarioInicio().equals(dataCancelamento.toLocalTime()) && horarioReserva.getHorarioTermino().equals(dataCancelamentoFim.toLocalTime())) {
                                        reservaSalva.getHorarioReserva().remove(horarioReserva);
                                        break;
                                    }
                                }
                            }
                        }

                        LocalDateTime diasParaPercorrer2 = reservaSalva.getEnd();
                        for (int i = 0; i <= 7; i++) {
                            LocalDateTime dia2 = diasParaPercorrer2.minusDays(i);
                            Boolean entrou = false;
                            for (HorarioReserva horarioReserva : reservaSalva.getHorarioReserva()) {
                                if (!dia2.toLocalDate().isBefore(reservaSalva.getStart().toLocalDate())) {
                                    if (horarioReserva.getDiaSemana().toUpperCase().equals((dia2.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                        if (horarioReserva.getDiaSemana().toUpperCase().equals((dataCancelamento.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                            if (!horarioReserva.getHorarioInicio().equals(dataCancelamento.toLocalTime()) && !horarioReserva.getHorarioTermino().equals(dataCancelamentoFim.toLocalTime())) { //VERIFICAR or data inicion diferente
                                                reservaSalva.setEnd(dia2.withHour(23).withMinute(59).withSecond(59).withNano(00));
                                                entrou = true;
                                                break;
                                            }
                                        } else {
                                            reservaSalva.setEnd(dia2.withHour(23).withMinute(59).withSecond(59).withNano(00));
                                            entrou = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (entrou) {
                                break;
                            }
                        }

//                    List<HorarioReserva> horarioParaOrdenar = new ArrayList<>();
//                    reservaSalva.getHorarioReserva().forEach(hr -> {
//                        if (hr.getDiaSemana().toUpperCase().equals((reservaSalva.getEnd().toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
//                            if (reservaSalva.getEnd().toLocalDate().equals(dataCancelamento.toLocalDate())) {
//                                if (hr.getHorarioInicio().isBefore(dataCancelamento.toLocalTime())) {
//                                    horarioParaOrdenar.add(hr);
//                                }
//                            } else {
//                                horarioParaOrdenar.add(hr);
//                            }
//                        }
//                    });
//                    if (!horarioParaOrdenar.isEmpty()) {
//                        Collections.sort(horarioParaOrdenar, ordernarHorarioFinal);
//                        LocalTime maiorHorario;
//                        maiorHorario = horarioParaOrdenar.get(horarioParaOrdenar.size() - 1).getHorarioInicio();
//                        reservaSalva.setEnd(reservaSalva.getEnd().withHour(maiorHorario.getHour()).withMinute(maiorHorario.getMinute()).withSecond(maiorHorario.getSecond()).withNano(maiorHorario.getNano()));
//                    }

                        reservaCancelada = reservaDependenciaRepository.save(reservaSalva);
                    }
                } else {
//            if (!reservaSalva.getStart().isAfter(LocalDateTime.now(ZoneId.of("UTC-3")))) { //VERIFICAR SE ISAFTER ESTÁ CORRETO
                    if (!reservaSalva.getStart().isAfter(LocalDateTime.now(ZoneId.of("UTC-3"))) && !reservaSalva.getStart().isAfter(dataCancelamento)) { //VERIFICAR SE ISAFTER ESTÁ CORRETO
                        ReservaDependencia novaReserva = new ReservaDependencia();
                        List<HorarioReserva> horarios = new ArrayList<>();
                        BeanUtils.copyProperties(reservaSalva, novaReserva, "id", "start", "status", "horarioReserva");
                        LocalDateTime diasParaPercorrer = LocalDateTime.now(ZoneId.of("UTC-3"));
                        if (ChronoUnit.DAYS.between(LocalDate.now(ZoneId.of("UTC-3")), reservaSalva.getEnd().toLocalDate()) <= 6) {
                            for (HorarioReserva horarioReserva : reservaSalva.getHorarioReserva()) {
                                Boolean entrou = false;
                                for (int i = 0; i <= 7; i++) {
                                    LocalDateTime dia = diasParaPercorrer.plusDays(i);
                                    if (!dia.toLocalDate().isAfter(reservaSalva.getEnd().toLocalDate())) {
                                        if (horarioReserva.getDiaSemana().toUpperCase().equals((dia.toLocalDate()).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                            if (horarioReserva.getDiaSemana().toUpperCase().equals(LocalDate.now(ZoneId.of("UTC-3")).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                                if (horarioReserva.getHorarioInicio().isAfter(LocalTime.now(ZoneId.of("UTC-3")))) { //verificar se é horarioReserva.getHorarioInicio() em vez de horarioReserva.getHorarioTermino() msm
                                                    entrou = true;
                                                    break;
                                                }
                                            } else {
                                                entrou = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (entrou) {
                                    HorarioReserva novoHorario = new HorarioReserva();
                                    novoHorario.setDiaSemana(horarioReserva.getDiaSemana());
                                    novoHorario.setHorarioInicio(horarioReserva.getHorarioInicio());
                                    novoHorario.setHorarioTermino(horarioReserva.getHorarioTermino());
                                    horarios.add(novoHorario);
                                }
                            }

                        } else {
                            for (HorarioReserva horarioReserva : reservaSalva.getHorarioReserva()) {
                                HorarioReserva novoHorario = new HorarioReserva();
                                novoHorario.setDiaSemana(horarioReserva.getDiaSemana());
                                novoHorario.setHorarioInicio(horarioReserva.getHorarioInicio());
                                novoHorario.setHorarioTermino(horarioReserva.getHorarioTermino());
                                horarios.add(novoHorario);
                            }
                        }

                        if (!horarios.isEmpty()) {
                            novaReserva.setHorarioReserva(horarios);
                            novaReserva.getHorarioReserva().forEach(hr -> hr.setReservaDependencia(novaReserva));
                            novaReserva.setStart(LocalDateTime.now(ZoneId.of("UTC-3")));
                            novaReserva.setStatus(statusCancelado);
                            this.salvarReservaUsoContinuo(novaReserva, usuarioLogado, false, false, false, false);
                        }

                        List<HorarioReserva> horariosAtuais = new ArrayList<>();
                        for (HorarioReserva horario : reservaSalva.getHorarioReserva()) {
                            if (horario.getDiaSemana().toUpperCase().equals((LocalDate.now(ZoneId.of("UTC-3"))).getDayOfWeek().toString().substring(0, 3).toUpperCase())) {
                                if (horario.getHorarioInicio().isBefore(LocalTime.now(ZoneId.of("UTC-3")))) {
                                    HorarioReserva horarioReserva = new HorarioReserva();
                                    horarioReserva.setDiaSemana(horario.getDiaSemana());
                                    horarioReserva.setHorarioInicio(horario.getHorarioInicio());
                                    horarioReserva.setHorarioTermino(horario.getHorarioTermino());
                                    horariosAtuais.add(horarioReserva);
                                }
                            }
                        }

                        if (!horariosAtuais.isEmpty()) {
                            ReservaDependencia reservaDiaAtual = new ReservaDependencia();
                            BeanUtils.copyProperties(reservaSalva, reservaDiaAtual, "id", "start", "end", "horarioReserva");
                            reservaDiaAtual.setStart(LocalDateTime.now(ZoneId.of("UTC-3")).withHour(7).withMinute(15).withSecond(00).withNano(00));
                            reservaDiaAtual.setEnd(LocalDateTime.now(ZoneId.of("UTC-3")).withHour(23).withMinute(59).withSecond(59).withNano(00));
                            reservaDiaAtual.setHorarioReserva(horariosAtuais);
                            this.salvarReservaUsoContinuo(reservaDiaAtual, usuarioLogado, false, true, false, false);
                        }

                        if (reservaSalva.getStart().toLocalDate().isBefore(LocalDate.now(ZoneId.of("UTC-3")))) {
                            ReservaDependencia reservaAntiga = reservaSalva;
                            reservaSalva.setEnd(LocalDateTime.now(ZoneId.of("UTC-3")).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(00));
                            reservaCancelada = reservaDependenciaRepository.save(reservaSalva);
                        } else {
                            reservaDependenciaRepository.delete(reservaSalva.getId());
                            exclusao = true;
                        }
                    } else {
                        reservaSalva.setStatus(statusCancelado);
                        reservaCancelada = reservaDependenciaRepository.save(reservaSalva);
                    }
                }
            }
        }
        if (!exclusao) {
            this.salvarTrilhaAuditoriaEdicao(reservaSemAlteracao, reservaCancelada, usuarioLogado);
        }
        return reservaCancelada;
    }

    public List<ReservaDependencia> listarReservasPorDependencia(Long idDependencia, Usuario usuarioLogado, String tipo) {
        Dependencia dependencia = dependenciaService.buscarDependenciaPeloId(idDependencia);
        List<ReservaDependencia> reservas = null;
        if (tipo.equals("all")) {
            reservas = reservaDependenciaRepository.listarReservasPorDependencia(dependencia.getId());
        } else {
            reservas = reservaDependenciaRepository.listarMinhasReservasPorDependencia(dependencia.getId(), usuarioLogado.getId());
        }
        return reservas;
    }

    public ReservaDependencia buscarPeloId(Long id) {
        ReservaDependencia reservaSalva = reservaDependenciaRepository.findOne(id);
        if (reservaSalva == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return reservaSalva;
    }

    private final Comparator<HorarioReserva> ordernarHorarioFinal = new Comparator<HorarioReserva>() {
        @Override
        public int compare(final HorarioReserva a, final HorarioReserva b) {
            try {
                final LocalTime valorA = a.getHorarioTermino();
                final LocalTime valorB = b.getHorarioTermino();
                if(valorA.compareTo(valorB) == 0) {
                    return 0;
                }

                return valorA.compareTo(valorB) < 0 ? -1 : 1;
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return -1;
        }
    };

    private final Comparator<HorarioReserva> ordernarHorarioInicial = new Comparator<HorarioReserva>() {
        @Override
        public int compare(final HorarioReserva a, final HorarioReserva b) {
            try {
                final LocalTime valorA = a.getHorarioInicio();
                final LocalTime valorB = b.getHorarioInicio();
                if(valorA.compareTo(valorB) == 0) {
                    return 0;
                }

                return valorA.compareTo(valorB) < 0 ? -1 : 1;
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return -1;
        }
    };

    private final Comparator<RelatorioReservaDependencia> ordernarStart = new Comparator<RelatorioReservaDependencia>() {
        @Override
        public int compare(final RelatorioReservaDependencia a, final RelatorioReservaDependencia b) {
            try {
                final LocalDateTime valorA = a.getStart();
                final LocalDateTime valorB = b.getStart();
                if(valorA.compareTo(valorB) == 0) {
                    return 0;
                }

                return valorA.compareTo(valorB) < 0 ? -1 : 1;
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return -1;
        }
    };
//    public ReservaDependencia salvarReservaUsoBreve(ReservaDependencia reservaDependencia, Long idUsuarioLogado) {
//        Usuario usuarioLogado = usuarioService.buscarUsuarioPeloId(idUsuarioLogado);
//        if(!usuarioLogado.getAtivo()) {
//            throw new UsuarioLogadoInativoException();
//        }
//        reservaDependencia.setUsuarioSolicitante(usuarioLogado);
//        reservaDependencia.setUsuarioUtilizador(usuarioLogado);
//        reservaDependencia.setTurma(null);
//        reservaDependencia.setUsoContinuo(false);
//        reservaDependencia.setDiaHorarioReserva(null);
//        Dependencia dependencia = dependenciaService.buscarDependenciaPeloId(reservaDependencia.getDependencia().getId());
//        if(dependencia.getTipo().name().equals("USO_CONTINUO")) {
//            throw new DependenciaUsoContinuoException();
//        }
//        if(reservaDependencia.getDataHorarioInicio().isBefore(LocalDateTime.now())) {
//            throw new DataHorarioInicioAnteriorADataAtualException();
//        }
//        if(reservaDependencia.getDataHorarioInicio().toLocalDate().compareTo(reservaDependencia.getDataHorarioTermino().toLocalDate()) != 0) {
//            throw new DatasDiferentesParaReservaDeUsoBreveException();
//        }
//        if(reservaDependencia.getDataHorarioTermino().isBefore(reservaDependencia.getDataHorarioInicio())) {
//            throw new DataHorarioTerminoMenorQueDataHorarioInicioException();
//        }
//        if(!dependencia.getAtivo()) {
//            throw new DependenciaInativaException();
//        }
//        //LIMITAR O TEMPO DA RESERVA (SE TRATANDO DE RESERVA DE USO BREVE)
//        List<ReservaDependencia> reservasComCompatibilidadeDeHorario = reservaDependenciaRepository.verificarCompatibilidaDeHorarioParaReservaBreve(reservaDependencia.getDataHorarioInicio(), reservaDependencia.getDataHorarioTermino(), reservaDependencia.getDataHorarioInicio().getDayOfWeek().toString());
//        if(!reservasComCompatibilidadeDeHorario.isEmpty()) {
//            throw new SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
//        }
//        ReservaDependencia reservaSalva = reservaDependenciaRepository.save(reservaDependencia);
//        return reservaSalva;
//    }
//
//    public ReservaDependencia salvarReservaUsoContinuo(ReservaDependencia reservaDependencia, Long idUsuarioLogado) {
//        Usuario usuarioLogado = usuarioService.buscarUsuarioPeloId(idUsuarioLogado);
//        if(!usuarioLogado.getAtivo()) {
//            throw new UsuarioLogadoInativoException();
//        }
//        reservaDependencia.setUsuarioSolicitante(usuarioLogado);
//        reservaDependencia.setUsoContinuo(true);
//        Dependencia dependencia = dependenciaService.buscarDependenciaPeloId(reservaDependencia.getDependencia().getId());
//        if(dependencia.getTipo().name().equals("USO_BREVE")) {
//            throw new DependenciaUsoBreveException();
//        }
//        if(reservaDependencia.getDataHorarioInicio().isBefore(LocalDateTime.now())) {
//            throw new DataHorarioInicioAnteriorADataAtualException();
//        }
//        if(reservaDependencia.getDataHorarioTermino().isBefore(reservaDependencia.getDataHorarioInicio())) {
//            throw new DataHorarioTerminoMenorQueDataHorarioInicioException();
//        }
//        if(!dependencia.getAtivo()) {
//            throw new DependenciaInativaException();
//        }
//        if(reservaDependencia.getTurma() != null) {
//            Turma turma = turmaService.buscarTurmaPeloId(reservaDependencia.getTurma().getId());
//            if(!turma.getAtivo()) {
//                throw new TurmaInativaException();
//            }
//        }
//        if(reservaDependencia.getDiaHorarioReserva() == null || reservaDependencia.getDiaHorarioReserva().isEmpty()) {
//            throw new ReservaDependenciaUsoContinuoException();
//        }
//        for(HorarioReserva horarioReserva : reservaDependencia.getDiaHorarioReserva()) {
//            if(horarioReserva.getHorarioTermino().isBefore(horarioReserva.getHorarioInicio())) {
//                //DISPARAR EXCEPTION
//            }
//        }
//        ReservaDependencia reservaSalva = reservaDependenciaRepository.save(reservaDependencia);
//        return reservaSalva;
//    }

//    public ReservaDependencia salvar(ReservaDependencia reservaDependencia, Long idUsuarioLogado) {
//        Dependencia dependencia = dependenciaService.buscarDependenciaPeloId(reservaDependencia.getDependencia().getId());
//        if(reservaDependencia.getTurma() != null) {
//            if(!reservaDependencia.getUsoContinuo()) {
//                throw new ReservaDependenciaUsoBreveComTurmaException();
//            }
//            Turma turma = turmaService.buscarTurmaPeloId(reservaDependencia.getTurma().getId());
//            if(!turma.getAtivo()) {
//                throw new TurmaInativaException();
//            }
//        }
//        if(dependencia.getTipo().name().equals("USO_BREVE") && reservaDependencia.getUsoContinuo()) {
//            throw new DependenciaUsoBreveException();
//        }
//        if(reservaDependencia.getUsoContinuo() && (reservaDependencia.getDiaHorarioReserva() == null || reservaDependencia.getDiaHorarioReserva().isEmpty())) {
//            throw new ReservaDependenciaUsoContinuoException();
//        }
//        if(!reservaDependencia.getUsoContinuo() && (reservaDependencia.getDiaHorarioReserva() != null)) {
//            throw new ReservaDependenciaUsoBreveException();
//        }
//        if(reservaDependencia.getDataHorarioInicio().isBefore(LocalDateTime.now())) {
//            throw new DataHorarioInicioAnteriorADataAtualException();
//        }
//        if(!reservaDependencia.getUsoContinuo() && (reservaDependencia.getDataHorarioInicio().toLocalDate().compareTo(reservaDependencia.getDataHorarioTermino().toLocalDate()) != 0)) {
//            throw new DatasDiferentesParaReservaDeUsoBreveException();
//        }
//        if(reservaDependencia.getDataHorarioTermino().isBefore(reservaDependencia.getDataHorarioInicio())) {
//            throw new DataHorarioTerminoMenorQueDataHorarioInicioException();
//        }
//        if(!dependencia.getAtivo()) {
//            throw new DependenciaInativaException();
//        }
//        //VERIFICAR SE O HORARIO FINAL É ANTERIOR AO HORÁRIO DE INÍCIO NO DIA_HORARIO_RESERVA
//        //LIMITAR O TEMPO DA RESERVA (SE TRATANDO DE RESERVA DE USO BREVE)
//        Usuario usuarioLogado = usuarioService.buscarUsuarioPeloId(idUsuarioLogado);
////        if(reservaDependencia.getUsoContinuo() && usuarioLogado.getRole().equals("USER")) {
////            throw new UsuarioSemPermissaoParaFazerReservaDeUsoContinuo();
////        }
//        List<ReservaDependencia> reservasComCompatibilidadeDeHorario = null;
//        if(!reservaDependencia.getUsoContinuo()) {
//            reservasComCompatibilidadeDeHorario = reservaDependenciaRepository.verificarCompatibilidaDeHorarioParaReservaBreve(reservaDependencia.getDataHorarioInicio(), reservaDependencia.getDataHorarioTermino(), reservaDependencia.getDataHorarioInicio().getDayOfWeek().toString());
//        } else {
//
//        }
//
//        if(!reservasComCompatibilidadeDeHorario.isEmpty()) {
//            throw new SolicitacaoReservaDependenciaComCompatibilidadeDeHorarioException(reservasComCompatibilidadeDeHorario);
//        }
//        // VERIFICAR SE OS USUÁRIOS ESTÃO ATIVOS
//        reservaDependencia.setUsuarioSolicitante(usuarioLogado);
//        if(!reservaDependencia.getUsoContinuo()) {
//            reservaDependencia.setUsuarioUtilizador(usuarioLogado);
//        }
//        ReservaDependencia reservaSalva = reservaDependenciaRepository.save(reservaDependencia);
//        return reservaSalva;
//    }
}
