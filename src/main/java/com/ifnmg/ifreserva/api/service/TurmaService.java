package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.model.Turma;
import com.ifnmg.ifreserva.api.repository.TurmaRepository;
import com.ifnmg.ifreserva.api.service.exception.NomeTurmaJaCadastradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class TurmaService {

    @Autowired
    private TurmaRepository turmaRepository;

    public Turma salvar(Turma turma) {
        if(turmaRepository.findByNome(turma.getNome()) != null) {
            throw new NomeTurmaJaCadastradoException();
        }
        turma.setAtivo(true);
        Turma turmaSalva = turmaRepository.save(turma);
        return turmaSalva;
    }

    public Turma atualizar(Long id, Turma turma) {
        if(turmaRepository.findByNomeUpdate(turma.getNome(), id) != null) {
            throw new NomeTurmaJaCadastradoException();
        }
        Turma turmaSalva = buscarTurmaPeloId(id);
        turmaSalva.setNome(turma.getNome());
        turmaSalva.setAtivo(turma.getAtivo());
        return turmaRepository.save(turmaSalva);
    }

    public void atualizarPropriedadeAtivo(Long id, Boolean ativo) {
        Turma turmaSalva = buscarTurmaPeloId(id);
        turmaSalva.setAtivo(ativo);
        turmaRepository.save(turmaSalva);
    }

    public Turma buscarTurmaPeloId(Long id) {
        Turma dependenciaSalva = turmaRepository.findOne(id);
        if (dependenciaSalva == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return dependenciaSalva;
    }

}
