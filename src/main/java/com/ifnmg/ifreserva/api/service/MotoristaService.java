package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.model.Motorista;
import com.ifnmg.ifreserva.api.model.TipoTrilhaAuditoria;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.repository.MotoristaRepository;
import com.ifnmg.ifreserva.api.service.exception.CpfMotoristaJaCadastradoException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class MotoristaService {

    @Autowired
    private MotoristaRepository motoristaRepository;
    @Autowired
    private TrilhaAuditoriaService trilhaAuditoriaService;

    public Motorista salvar(Motorista motorista, Usuario usuarioLogado) {
        if(motoristaRepository.findByCpf(motorista.getCpf()) != null) {
            throw new CpfMotoristaJaCadastradoException();
        }
        motorista.setAtivo(true);
        Motorista motoristaSalvo = motoristaRepository.save(motorista);
        this.salvarTrilhaAuditoriaCadastro(motoristaSalvo, usuarioLogado, "Cadastro");
        return motoristaSalvo;
    }

    public Motorista atualizar(Long id, Motorista motorista, Usuario usuarioLogado) {
        if(motoristaRepository.findByCpfUpdate(motorista.getCpf(), id) != null) {
            throw new CpfMotoristaJaCadastradoException();
        }
        Motorista motoristaAntigo = new Motorista();
        Motorista motoristaSalvo = buscarMotoristaPeloId(id);
        BeanUtils.copyProperties(motoristaSalvo, motoristaAntigo);
        motoristaSalvo.setNome(motorista.getNome());
        motoristaSalvo.setSobrenome(motorista.getSobrenome());
        motoristaSalvo.setCpf(motorista.getCpf());
        motoristaSalvo.setAtivo(motorista.getAtivo());
        Motorista motoristaAlterado = motoristaRepository.save(motoristaSalvo);
        this.salvarTrilhaAuditoriaEdicao(motoristaAntigo, motoristaAlterado, usuarioLogado);
        return motoristaAlterado;
    }

    public void atualizarPropriedadeAtivo(Long id, Boolean ativo, Usuario usuarioLogado) {
        Motorista motoristaAntigo = new Motorista();
        Motorista motoristaSalvo = buscarMotoristaPeloId(id);
        BeanUtils.copyProperties(motoristaSalvo, motoristaAntigo);
        motoristaSalvo.setAtivo(ativo);
        Motorista motoristaAlterado = motoristaRepository.save(motoristaSalvo);
        this.salvarTrilhaAuditoriaEdicao(motoristaAntigo, motoristaAlterado, usuarioLogado);
    }

    public void salvarTrilhaAuditoriaCadastro(Motorista motoristaSalvo, Usuario usuarioLogado, String categoria) {
        String descricao = "";
        descricao += "Nome: " + motoristaSalvo.getNome() + "; ";
        descricao += "Sobrenome: " + motoristaSalvo.getSobrenome() + "; ";
        descricao += "CPF: " + motoristaSalvo.getCpf() + "; ";
        if (motoristaSalvo.getAtivo()) {
            descricao += "Status: ativo; ";
        } else {
            descricao += "Status: inativo; ";
        }
        trilhaAuditoriaService.salvar(descricao, categoria, TipoTrilhaAuditoria.MOTORISTA, motoristaSalvo.getId(), usuarioLogado);
    }
    public void salvarTrilhaAuditoriaEdicao(Motorista motoristaAntigo, Motorista motoristaSalvo, Usuario usuarioLogado) {
        String descricao = "";
        if (!motoristaAntigo.getNome().equals(motoristaSalvo.getNome())) {
            descricao += "Nome: " + motoristaSalvo.getNome() + "; ";
        }
        if (!motoristaAntigo.getSobrenome().equals(motoristaSalvo.getSobrenome())) {
            descricao += "Sobrenome: " + motoristaSalvo.getSobrenome() + "; ";
        }
        if (!motoristaAntigo.getCpf().equals(motoristaSalvo.getCpf())) {
            descricao += "CPF: " + motoristaSalvo.getCpf() + "; ";
        }
        if (motoristaAntigo.getAtivo() != motoristaSalvo.getAtivo()) {
            if (motoristaSalvo.getAtivo()) {
                descricao += "Status: ativo; ";
            } else {
                descricao += "Status: inativo; ";
            }
        }
        trilhaAuditoriaService.salvar(descricao, "Edição", TipoTrilhaAuditoria.MOTORISTA, motoristaSalvo.getId(), usuarioLogado);
    }

    public Motorista buscarMotoristaPeloId(Long id) {
        Motorista motoristaSalvo = motoristaRepository.findOne(id);
        if (motoristaSalvo == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return motoristaSalvo;
    }
}
