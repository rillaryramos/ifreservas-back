package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.sendgrid.EmailData;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class EmailService {

    public void EnviarEmail(String nomeDestinatario, String emailDestinatario, String assunto, Map<String, String> templateParams, String templateId){
        SendgridService sendgridService = new SendgridService();
        EmailData emailData = new EmailData();
        emailData.setSenderName("IF Reserva");
        emailData.setSenderEmail("rillaryramos@gmail.com");
        emailData.setHtml(true);
        emailData.setReceiverName(nomeDestinatario);
        emailData.setReceiverEmail(emailDestinatario);
        emailData.setSubject(assunto);
        emailData.setTemplateParams(templateParams);
        emailData.setTemplateId(templateId);
        emailData.setContent("");
        sendgridService.sendEmail(emailData);
    }

}
