package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.sendgrid.EmailData;
import com.ifnmg.ifreserva.api.sendgrid.Property;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SendgridService implements ISendgridService {

    @Override
    public void sendEmail(EmailData emailData) {
        try {
            Email senderEmail = null;
            Email receiverEmail = null;

            if(emailData.getSenderName() != null && !emailData.getSenderName().replaceAll("\\s+", "").equals("")){
                senderEmail = new Email(emailData.getSenderEmail(), emailData.getSenderName());
            } else {
                senderEmail = new Email(emailData.getSenderEmail());
            }

            if(emailData.getReceiverName() != null && !emailData.getReceiverName().replaceAll("\\s+", "").equals("")){
                receiverEmail = new Email(emailData.getReceiverEmail(), emailData.getReceiverName());
            } else {
                receiverEmail = new Email(emailData.getReceiverEmail());
            }

            if(emailData.getTemplateParams() != null){
                send(senderEmail, receiverEmail, new Content("text/html", emailData.getContent()), true, emailData );
            } else{
                if(emailData.getHtml() != null && emailData.getHtml()){
                    send(senderEmail, receiverEmail, new Content("text/html", emailData.getContent()), false, emailData);
                } else {
                    send(senderEmail, receiverEmail, new Content("text/plain", emailData.getContent()), false, emailData);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private static void send(Email from, Email to, Content content,  boolean templateEmail, EmailData emailData){
        Mail mail = null;
        if(templateEmail){
            mail = new Mail(from, emailData.getSubject(), to, new Content("text/html", "EMAIL"));
            mail.setTemplateId(emailData.getTemplateId());
            if(emailData.getTemplateParams() != null){
                for(String key: emailData.getTemplateParams().keySet()){
                    mail.personalization.get(0).addSubstitution(key, emailData.getTemplateParams().get(key));
                }
            }

        } else{
            mail = new Mail(from, emailData.getSubject(), to, content);
        }
        SendGrid sg = new SendGrid(Property.SENDGRID_API_KEY.key());
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            //request.addHeader("authorization", "Bearer " + Property.SENDGRID_API_KEY);
            // request.addHeader("content-Type", "application/json");
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
