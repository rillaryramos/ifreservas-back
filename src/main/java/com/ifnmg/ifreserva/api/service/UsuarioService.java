package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.mail.Mailer;
import com.ifnmg.ifreserva.api.model.RecuperacaoSenha;
import com.ifnmg.ifreserva.api.model.TipoUsuario;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.repository.UsuarioRepository;
import com.ifnmg.ifreserva.api.service.exception.*;
import com.ifnmg.ifreserva.api.utils.DateUtils;
import com.ifnmg.ifreserva.api.utils.EmailUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private RecuperacaoSenhaService recuperacaoSenhaService;
    @Autowired
    private BCryptPasswordEncoder pe;
    @Autowired
    private Mailer mailer;

    private Random rand = new Random();

    public Usuario salvar(Usuario usuario, Long idUsuarioResponsavel) {
        //VERIFICAR E-MAIL
        if(usuarioRepository.findByEmail(usuario.getEmail()).isPresent()) {
            throw new EmailJaCadastradoException();
        }
        if(usuarioRepository.findByLogin(usuario.getLogin()).isPresent()) {
            throw new LoginJaCadastradoException();
        }
        if(usuario.getRole().name().equals("MASTER")) {
            Usuario usuarioMaster = usuarioRepository.findByUserMaster();
            if(usuarioMaster != null) {
                throw new SalvarUsuarioMasterException();
            }
        }
        String senha = newPassword();
        System.out.println("Senha gerada: " + senha);
        usuario.setSenha(pe.encode(senha));
        usuario.setAtivo(true);
        usuario.setDataCadastro(new Date());
        Usuario usuarioSalvo = usuarioRepository.save(usuario);
        mailer.boasVindas(usuario.getLogin(), senha, usuario.getEmail());
        return usuarioSalvo;
    }

    public List<Usuario> findAprovadoresAtivos() { //VERIFICAR
        return usuarioRepository.findAprovadoresAtivos();
    }

    public Usuario atualizar(Long id, Usuario usuario) {
        Usuario usuarioSalvo = buscarUsuarioPeloId(id);
        BeanUtils.copyProperties(usuario, usuarioSalvo, "id", "email", "role", "login", "senha", "ativo", "dataCadastro");
        return usuarioRepository.save(usuarioSalvo);
    }

    public Usuario atualizarRoleAtivo(Long idUsuario, String role, Boolean ativo) {
        Usuario usuarioMasterAntigo = null;
        Usuario usuarioSalvo = buscarUsuarioPeloId(idUsuario);
        usuarioSalvo.setAtivo(ativo);
        Boolean trocaMaster = false;
        TipoUsuario novaRole;
        if(role.equals("ROLE_ADMIN")) {
            novaRole = TipoUsuario.ROLE_ADMIN;
        } else if(role.equals("ROLE_USER")){
            novaRole = TipoUsuario.ROLE_USER;
        } else if(role.equals("ROLE_APPROVER")){
            novaRole = TipoUsuario.ROLE_APPROVER;
        } else {
            novaRole = TipoUsuario.ROLE_MASTER;
            trocaMaster = true;
            usuarioMasterAntigo = usuarioRepository.findByUserMaster();
        }
        usuarioSalvo.setRole(novaRole);
        Usuario usuarioAlterado = usuarioRepository.save(usuarioSalvo);
        if(trocaMaster) {
            usuarioMasterAntigo.setRole(TipoUsuario.ROLE_USER);
            usuarioRepository.save(usuarioMasterAntigo);
        }
        return usuarioAlterado;
    }

    public void atualizarPropriedadeAtivo(Long id, Boolean ativo) {
        Usuario usuarioSalvo = buscarUsuarioPeloId(id);
        usuarioSalvo.setAtivo(ativo);
        usuarioRepository.save(usuarioSalvo);
    }

    public void alterarPropriedadeSenha(String senhaAtual, String novaSenha, Long id) {
        if(novaSenha.equals("") || novaSenha == null || novaSenha.length() < 6) {
            throw new NovaSenhaInvalidaException();
        }
        Usuario usuarioLogado = buscarUsuarioPeloId(id);
        if(pe.matches(senhaAtual, usuarioLogado.getSenha())) {
            usuarioLogado.setSenha(pe.encode(novaSenha));
            usuarioRepository.save(usuarioLogado);
        } else {
            throw new SenhaAtualInvalidaException();
        }
    }

    public void recuperarPropriedadeSenha(String token, String email, String novaSenha) {
//        Optional<Usuario> usuarioSalvo = buscarUsuarioPeloEmail(email);
        RecuperacaoSenha recuperacaoSenha = recuperacaoSenhaService.buscarRecuperacaoPeloToken(token);
        if (recuperacaoSenha == null || recuperacaoSenha.getDataExpiracao().isBefore(LocalDateTime.now(ZoneId.of("UTC-3")))) {
            throw new TokenInativoException();
        }
        if (!recuperacaoSenha.getUsuario().getEmail().equals(email)) {
            throw new TokenInativoException(); // ALTERAR
        }
        if(novaSenha.equals("") || novaSenha == null || novaSenha.length() < 6) {
            throw new NovaSenhaInvalidaException();
        }
        Usuario usuario = recuperacaoSenha.getUsuario();
        usuario.setSenha(pe.encode(novaSenha));
        usuarioRepository.save(usuario);
        if (recuperacaoSenha != null) {
            recuperacaoSenhaService.excluir(recuperacaoSenha);
        }
    }

    public void enviarLinkParaAlteracaoDeSenha(String email) {
        Optional<Usuario> usuarioSalvo = buscarUsuarioPeloEmail(email);
        RecuperacaoSenha ultimaRecuperacaoSenhaAtiva = recuperacaoSenhaService.buscarRecuperacaoPeloIdUsuario(usuarioSalvo.get().getId());
        if (ultimaRecuperacaoSenhaAtiva != null) {
            recuperacaoSenhaService.excluir(ultimaRecuperacaoSenhaAtiva);
        }
        RecuperacaoSenha recuperacaoSenha = new RecuperacaoSenha();
        recuperacaoSenha.setUsuario(usuarioSalvo.get());
        String tokenGerado = randomStringUUID();
        while (recuperacaoSenhaService.buscarRecuperacaoPeloToken(tokenGerado) != null) {
            tokenGerado = randomStringUUID();
        }
        recuperacaoSenha.setToken(tokenGerado);
        recuperacaoSenha.setDataExpiracao(LocalDateTime.now(ZoneId.of("UTC-3")).plusHours(1));
        recuperacaoSenhaService.salvar(recuperacaoSenha);
        mailer.enviarLinkParaAlteracaoDeSenha(recuperacaoSenha);
//        EmailUtils emailUtils = new EmailUtils();
//        emailUtils.enviarLinkParaAlteracaoDeSenha(recuperacaoSenha);
    }

    public void trocarUserMaster(Long idNovoMaster, Boolean antigoMasterIsNovoAdmin) {
        if(idNovoMaster == null || antigoMasterIsNovoAdmin == null) {
            throw new ParametrosParaTrocarMasterException();
        }
        Usuario usuarioNovoMaster = buscarUsuarioPeloId(idNovoMaster);
        if(!usuarioNovoMaster.getAtivo()) {
            throw new NovoMasterNaoEstaAtivoException();
        }
        TipoUsuario novaRole;
        if(antigoMasterIsNovoAdmin) {
            novaRole = TipoUsuario.ROLE_ADMIN;
        } else {
            novaRole = TipoUsuario.ROLE_USER;
        }
        Usuario usuarioMasterAtual = usuarioRepository.findByUserMaster();
        usuarioMasterAtual.setRole(novaRole);
        usuarioRepository.save(usuarioMasterAtual);
        usuarioNovoMaster.setRole(TipoUsuario.ROLE_MASTER);
        usuarioRepository.save(usuarioNovoMaster);
        //ENCERAR SESSÃO
    }

    public Usuario buscarUsuarioPeloId(Long id) {
        Usuario usuarioSalvo = usuarioRepository.findOne(id);
        if (usuarioSalvo == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return usuarioSalvo;
    }

    public Optional<Usuario> buscarUsuarioPeloEmail(String email) {
        Optional<Usuario> usuarioSalvo = usuarioRepository.findByEmail(email);
        if (!usuarioSalvo.isPresent()) {
            throw new EmptyResultDataAccessException(1);
        }
        return usuarioSalvo;
    }

    public Optional<Usuario> buscarUsuarioPeloLogin(String login) {
        Optional<Usuario> usuarioSalvo = usuarioRepository.findByLogin(login);
        if (!usuarioSalvo.isPresent()) {
            throw new EmptyResultDataAccessException(1);
        }
        return usuarioSalvo;
    }

    private String newPassword() {
        char[] vet = new char[10];
        for (int i=0; i<10; i++) {
            vet[i] = randomChar();
        }
        return new String(vet);
    }

    private char randomChar() {
        int opt = rand.nextInt(3);
        if (opt == 0) { // gera um dígito
            return (char) (rand.nextInt(10) + 48);
        } else if (opt == 1) { // gera letra maiúscula
            return (char) (rand.nextInt(26) + 65);
        } else { // gera letra minúscula
            return (char) (rand.nextInt(26) + 97);
        }
    }

    private String randomStringUUID() {
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        return randomUUIDString.replaceAll("/","&");
    }
}
