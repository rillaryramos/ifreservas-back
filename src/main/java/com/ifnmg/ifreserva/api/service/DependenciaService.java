package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.model.Dependencia;
import com.ifnmg.ifreserva.api.repository.DependenciaRepository;
import com.ifnmg.ifreserva.api.service.exception.NomeDependenciaJaCadastradoException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class DependenciaService {

    @Autowired
    private DependenciaRepository dependenciaRepository;

    public Dependencia salvar(Dependencia dependencia) {
        if(dependenciaRepository.findByNomeAndBlocoId(dependencia.getNome(), dependencia.getBloco().getId()) != null) {
            throw new NomeDependenciaJaCadastradoException();
        }
        dependencia.setAtivo(true);
        Dependencia dependenciaSalva = dependenciaRepository.save(dependencia);
        return dependenciaSalva;
    }

    public Dependencia atualizar(Long id, Dependencia dependencia) {
        if(dependenciaRepository.findByNomeUpdate(dependencia.getNome(), dependencia.getBloco().getId(), id) != null) {
            throw new NomeDependenciaJaCadastradoException();
        }
        Dependencia dependenciaSalva = buscarDependenciaPeloId(id);
        BeanUtils.copyProperties(dependencia, dependenciaSalva, "id", "bloco");
        return dependenciaRepository.save(dependenciaSalva);
    }

    public void atualizarPropriedadeAtivo(Long id, Boolean ativo) {
        Dependencia dependenciaSalva = buscarDependenciaPeloId(id);
        dependenciaSalva.setAtivo(ativo);
        dependenciaRepository.save(dependenciaSalva);
    }

    public Dependencia buscarDependenciaPeloId(Long id) {
        Dependencia dependenciaSalva = dependenciaRepository.findOne(id);
        if (dependenciaSalva == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return dependenciaSalva;
    }
}
