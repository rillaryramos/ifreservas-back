package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.model.TipoTrilhaAuditoria;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.model.Veiculo;
import com.ifnmg.ifreserva.api.repository.VeiculoRepository;
import com.ifnmg.ifreserva.api.service.exception.PlacaVeiculoJaCadastradaException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class VeiculoService {

    @Autowired
    private VeiculoRepository veiculoRepository;
    @Autowired
    private TrilhaAuditoriaService trilhaAuditoriaService;

    public Veiculo salvar(Veiculo veiculo, Usuario usuarioLogado) {
        if(veiculoRepository.findByPlaca(veiculo.getPlaca()) != null) {
            throw new PlacaVeiculoJaCadastradaException();
        }
        veiculo.setAtivo(true);
        Veiculo veiculoSalvo = veiculoRepository.save(veiculo);
        this.salvarTrilhaAuditoriaCadastro(veiculoSalvo, usuarioLogado, "Cadastro");
        return veiculoSalvo;
    }

    public Veiculo atualizar(Long id, Veiculo veiculo, Usuario usuarioLogado) {
        if(veiculoRepository.findByPlacaUpdate(veiculo.getPlaca(), id) != null) {
            throw new PlacaVeiculoJaCadastradaException();
        }
        Veiculo veiculoAntigo = new Veiculo();
        Veiculo veiculoSalvo = buscarVeiculoPeloId(id);
        BeanUtils.copyProperties(veiculoSalvo, veiculoAntigo);
        veiculoSalvo.setModelo(veiculo.getModelo());
        veiculoSalvo.setMarca(veiculo.getMarca());
        veiculoSalvo.setPlaca(veiculo.getPlaca());
        veiculoSalvo.setObservacao(veiculo.getObservacao());
        veiculoSalvo.setAtivo(veiculo.getAtivo());
        Veiculo veiculoAlterado = veiculoRepository.save(veiculoSalvo);
        this.salvarTrilhaAuditoriaEdicao(veiculoAntigo, veiculoAlterado, usuarioLogado);
        return veiculoAlterado;
    }

    public void atualizarPropriedadeAtivo(Long id, Boolean ativo, Usuario usuarioLogado) {
        Veiculo veiculoAntigo = new Veiculo();
        Veiculo veiculoSalvo = buscarVeiculoPeloId(id);
        BeanUtils.copyProperties(veiculoSalvo, veiculoAntigo);
        veiculoSalvo.setAtivo(ativo);
        Veiculo veiculoAlterado = veiculoRepository.save(veiculoSalvo);
        this.salvarTrilhaAuditoriaEdicao(veiculoAntigo, veiculoAlterado, usuarioLogado);
    }

    public void salvarTrilhaAuditoriaCadastro(Veiculo veiculoSalvo, Usuario usuarioLogado, String categoria) {
        String descricao = "";
        descricao += "Modelo: " + veiculoSalvo.getModelo() + "; ";
        descricao += "Marca: " + veiculoSalvo.getMarca() + "; ";
        descricao += "Placa: " + veiculoSalvo.getPlaca() + "; ";
        if (veiculoSalvo.getObservacao() != null) {
            descricao += "Observação: " + veiculoSalvo.getObservacao() + "; ";
        } else {
            descricao += "Observação: ; ";
        }
        if (veiculoSalvo.getAtivo()) {
            descricao += "Status: disponível; ";
        } else {
            descricao += "Status: indisponível; ";
        }
        trilhaAuditoriaService.salvar(descricao, categoria, TipoTrilhaAuditoria.VEICULO, veiculoSalvo.getId(), usuarioLogado);
    }

    public void salvarTrilhaAuditoriaEdicao(Veiculo veiculoAntigo, Veiculo veiculoSalvo, Usuario usuarioLogado) {
        String descricao = "";
        if (!veiculoAntigo.getModelo().equals(veiculoSalvo.getModelo())) {
            descricao += "Modelo: " + veiculoSalvo.getModelo() + "; ";
        }
        if (!veiculoAntigo.getMarca().equals(veiculoSalvo.getMarca())) {
            descricao += "Marca: " + veiculoSalvo.getMarca() + "; ";
        }
        if (!veiculoAntigo.getPlaca().equals(veiculoSalvo.getPlaca())) {
            descricao += "Placa: " + veiculoSalvo.getPlaca() + "; ";
        }
        if (veiculoAntigo.getObservacao() == null) {
            if (veiculoSalvo.getObservacao() != null) {
                descricao += "Observação: " + veiculoSalvo.getObservacao() + "; ";
            }
        } else if (!veiculoAntigo.getObservacao().equals(veiculoSalvo.getObservacao())){
            descricao += "Observação: " + veiculoSalvo.getObservacao() + "; ";
        }
        if (veiculoAntigo.getAtivo() != veiculoSalvo.getAtivo()) {
            if (veiculoSalvo.getAtivo()) {
                descricao += "Status: disponível; ";
            } else {
                descricao += "Status: indisponível; ";
            }
        }
        trilhaAuditoriaService.salvar(descricao, "Edição", TipoTrilhaAuditoria.VEICULO, veiculoSalvo.getId(), usuarioLogado);
    }

    public Veiculo buscarVeiculoPeloId(Long id) {
        Veiculo veiculoSalvo = veiculoRepository.findOne(id);
        if (veiculoSalvo == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return veiculoSalvo;
    }
}
