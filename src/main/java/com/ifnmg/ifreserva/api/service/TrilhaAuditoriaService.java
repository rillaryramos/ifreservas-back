package com.ifnmg.ifreserva.api.service;

import com.ifnmg.ifreserva.api.model.TipoTrilhaAuditoria;
import com.ifnmg.ifreserva.api.model.TrilhaAuditoria;
import com.ifnmg.ifreserva.api.model.ReservaDependencia;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.repository.TrilhaAuditoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Service
public class TrilhaAuditoriaService {

    @Autowired
    private TrilhaAuditoriaRepository trilhaAuditoriaRepository;

    public void salvar(String descricao, String categoria, TipoTrilhaAuditoria tipo, Long idEntidade, Usuario usuario) {
        TrilhaAuditoria trilhaAuditoria = new TrilhaAuditoria();
        trilhaAuditoria.setDescricao(descricao);
        trilhaAuditoria.setCategoria(categoria);
        trilhaAuditoria.setTipo(tipo);
        trilhaAuditoria.setData(LocalDateTime.now(ZoneId.of("UTC-3")));
        trilhaAuditoria.setIdEntidade(idEntidade);
        trilhaAuditoria.setUsuario(usuario);
        trilhaAuditoriaRepository.save(trilhaAuditoria);
    }
}
