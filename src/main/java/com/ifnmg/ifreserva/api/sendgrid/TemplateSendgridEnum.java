package com.ifnmg.ifreserva.api.sendgrid;

public enum TemplateSendgridEnum {

    BOAS_VINDAS("fdbc9044-1fae-4645-8490-ca6214cefc75"),
    RECUPERAR_SENHA("11c72712-c6ea-4d4c-9ef7-d5ad35eb9a6b");

    private final String templateId;

    private TemplateSendgridEnum(String id){
        templateId = id;
    }

    public String getTemplateId(){
        return templateId;
    }
}
