package com.ifnmg.ifreserva.api.model;

public enum TipoTrilhaAuditoria {

    RESERVA_DEPENDENCIA,
    RESERVA_VEICULO,
    MOTORISTA,
    VEICULO;

}
