package com.ifnmg.ifreserva.api.model;

public enum TipoDependencia {

    USO_CONTINUO,
    USO_BREVE,
    AMBOS

}
