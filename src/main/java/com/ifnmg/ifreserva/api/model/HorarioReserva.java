package com.ifnmg.ifreserva.api.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "horario_reserva")
public class HorarioReserva {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @NotNull
    @Column(name = "dia_semana")
    private String diaSemana;
    @NotNull
    @Column(name = "horario_inicio")
    private LocalTime horarioInicio;
    @NotNull
    @Column(name = "horario_termino")
    private LocalTime horarioTermino;
    @ManyToOne
    @JoinColumn(name = "id_reserva_dependencia")
    private ReservaDependencia reservaDependencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    public LocalTime getHorarioInicio() {
        return horarioInicio;
    }

    public void setHorarioInicio(LocalTime horarioInicio) {
        this.horarioInicio = horarioInicio;
    }

    public LocalTime getHorarioTermino() {
        return horarioTermino;
    }

    public void setHorarioTermino(LocalTime horarioTermino) {
        this.horarioTermino = horarioTermino;
    }

    public ReservaDependencia getReservaDependencia() {
        return reservaDependencia;
    }

    public void setReservaDependencia(ReservaDependencia reservaDependencia) {
        this.reservaDependencia = reservaDependencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HorarioReserva that = (HorarioReserva) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
