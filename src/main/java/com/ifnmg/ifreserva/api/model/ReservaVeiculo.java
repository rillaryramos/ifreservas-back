package com.ifnmg.ifreserva.api.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Table(name = "reserva_veiculo")
public class ReservaVeiculo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @NotNull
    @Size(max = 25)
    @Column(name = "title")
    private String title;
    @NotNull
    @Column(name = "start")
    private LocalDateTime start;
    @NotNull
    @Column(name = "end")
    private LocalDateTime end;
    @NotNull
    @Size(max = 70)
    @Column(name = "saida")
    private String saida;
    @NotNull
    @Size(max = 70)
    @Column(name = "destino")
    private String destino;
    @NotNull
    @Size(max = 300)
    @Column(name = "motivo")
    private String motivo;
    @NotNull
    @OneToOne
    @JoinColumn(name = "id_veiculo")
    private Veiculo veiculo;
    @OneToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    @OneToOne
    @JoinColumn(name = "id_status")
    private Status status;
    @OneToOne
    @JoinColumn(name = "id_motorista")
    private Motorista motorista;
    @Size(max = 300)
    @Column(name = "motivo_rejeicao")
    private String motivoRejeicao;
    @Column(name = "data_envio_aprovacao")
    private LocalDateTime dataEnvioAprovacao;
    @OneToOne
    @JoinColumn(name = "id_administrador_reserva")
    private Usuario administradorReserva;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public String getSaida() {
        return saida;
    }

    public void setSaida(String saida) {
        this.saida = saida;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Motorista getMotorista() {
        return motorista;
    }

    public void setMotorista(Motorista motorista) {
        this.motorista = motorista;
    }

    public String getMotivoRejeicao() {
        return motivoRejeicao;
    }

    public void setMotivoRejeicao(String motivoRejeicao) {
        this.motivoRejeicao = motivoRejeicao;
    }

    public LocalDateTime getDataEnvioAprovacao() {
        return dataEnvioAprovacao;
    }

    public void setDataEnvioAprovacao(LocalDateTime dataEnvioAprovacao) {
        this.dataEnvioAprovacao = dataEnvioAprovacao;
    }

    public Usuario getAdministradorReserva() {
        return administradorReserva;
    }

    public void setAdministradorReserva(Usuario administradorReserva) {
        this.administradorReserva = administradorReserva;
    }
}
