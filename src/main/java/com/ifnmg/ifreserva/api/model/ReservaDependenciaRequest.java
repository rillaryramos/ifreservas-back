package com.ifnmg.ifreserva.api.model;

import java.util.List;

public class ReservaDependenciaRequest {

    private List<ReservaDependencia> reservas;

    public List<ReservaDependencia> getReservas() {
        return reservas;
    }

    public void setReservas(List<ReservaDependencia> reservas) {
        this.reservas = reservas;
    }
}
