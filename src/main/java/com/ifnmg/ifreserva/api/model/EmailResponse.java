package com.ifnmg.ifreserva.api.model;

public class EmailResponse {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
