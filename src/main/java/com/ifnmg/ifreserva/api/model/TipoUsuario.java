package com.ifnmg.ifreserva.api.model;

public enum TipoUsuario {

    ROLE_MASTER,
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_APPROVER

}
