package com.ifnmg.ifreserva.api.model;

public enum StatusEnum {

    APROVADA               (1l),
    EM_APROVACAO         (2l),
    REJEITADA             (3l),
    CANCELADA            (4l);

    private final Long idStatus;

    private StatusEnum(Long id){
        idStatus = id;
    }

    public Long getIdStatus(){
        return idStatus;
    }
}
