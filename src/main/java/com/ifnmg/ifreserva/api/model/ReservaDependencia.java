package com.ifnmg.ifreserva.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "reserva_dependencia")
public class ReservaDependencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @NotNull
    @Size(max = 25)
    @Column(name = "title")
    private String title;
    @NotNull
    @OneToOne
    @JoinColumn(name = "id_dependencia")
    private Dependencia dependencia;
    @OneToOne
    @JoinColumn(name = "id_usuario_solicitante")
    private Usuario usuarioSolicitante;
    @OneToOne
    @JoinColumn(name = "id_usuario_utilizador")
    private Usuario usuarioUtilizador;
    @OneToOne
    @JoinColumn(name = "id_turma")
    private Turma turma;
    @NotNull
    @Column(name = "start")
    private LocalDateTime start;
    @NotNull
    @Column(name = "end")
    private LocalDateTime end;
    @Size(max = 250)
    @Column(name = "observacao")
    private String observacao;
    @Column(name = "uso_continuo")
    private Boolean usoContinuo;
    @NotNull
    @Column(name = "aula")
    private Boolean aula;
    @OneToOne
    @JoinColumn(name = "id_status")
    private Status status;
    @JsonIgnoreProperties("reservaDependencia")
    @Valid
    @OneToMany(mappedBy = "reservaDependencia", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<HorarioReserva> horarioReserva;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Dependencia getDependencia() {
        return dependencia;
    }

    public void setDependencia(Dependencia dependencia) {
        this.dependencia = dependencia;
    }

    public Usuario getUsuarioSolicitante() {
        return usuarioSolicitante;
    }

    public void setUsuarioSolicitante(Usuario usuarioSolicitante) {
        this.usuarioSolicitante = usuarioSolicitante;
    }

    public Usuario getUsuarioUtilizador() {
        return usuarioUtilizador;
    }

    public void setUsuarioUtilizador(Usuario usuarioUtilizador) {
        this.usuarioUtilizador = usuarioUtilizador;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Boolean getUsoContinuo() {
        return usoContinuo;
    }

    public void setUsoContinuo(Boolean usoContinuo) {
        this.usoContinuo = usoContinuo;
    }

    public Boolean getAula() {
        return aula;
    }

    public void setAula(Boolean aula) {
        this.aula = aula;
    }

    public List<HorarioReserva> getHorarioReserva() {
        return horarioReserva;
    }

    public void setHorarioReserva(List<HorarioReserva> horarioReserva) {
        this.horarioReserva = horarioReserva;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservaDependencia that = (ReservaDependencia) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
