package com.ifnmg.ifreserva.api.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "trilha_auditoria")
public class TrilhaAuditoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @NotNull
    @Column(name = "descricao")
    private String descricao;
    @NotNull
    @Column(name = "categoria")
    private String categoria;
    @NotNull
    @Column(name = "tipo")
    @Enumerated(EnumType.STRING)
    private TipoTrilhaAuditoria tipo;
    @NotNull
    @Column(name = "data")
    private LocalDateTime data;
    @NotNull
    @Column(name = "id_entidade")
    private Long idEntidade;
    @NotNull
    @JoinColumn(name = "id_usuario")
    @ManyToOne
    private Usuario usuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public TipoTrilhaAuditoria getTipo() {
        return tipo;
    }

    public void setTipo(TipoTrilhaAuditoria tipo) {
        this.tipo = tipo;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Long getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Long idEntidade) {
        this.idEntidade = idEntidade;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
