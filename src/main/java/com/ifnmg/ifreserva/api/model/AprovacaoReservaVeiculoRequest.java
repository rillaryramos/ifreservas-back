package com.ifnmg.ifreserva.api.model;

import java.util.List;

public class AprovacaoReservaVeiculoRequest {

    private ReservaVeiculo reservaParaAprovar;
    private List<ReservaVeiculo> reservasParaReprovar;

    public ReservaVeiculo getReservaParaAprovar() {
        return reservaParaAprovar;
    }

    public void setReservaParaAprovar(ReservaVeiculo reservaParaAprovar) {
        this.reservaParaAprovar = reservaParaAprovar;
    }

    public List<ReservaVeiculo> getReservasParaReprovar() {
        return reservasParaReprovar;
    }

    public void setReservasParaReprovar(List<ReservaVeiculo> reservasParaReprovar) {
        this.reservasParaReprovar = reservasParaReprovar;
    }
}
