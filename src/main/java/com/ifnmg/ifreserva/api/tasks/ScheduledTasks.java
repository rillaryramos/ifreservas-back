package com.ifnmg.ifreserva.api.tasks;

import com.ifnmg.ifreserva.api.mail.Mailer;
import com.ifnmg.ifreserva.api.model.ReservaVeiculo;
import com.ifnmg.ifreserva.api.model.Status;
import com.ifnmg.ifreserva.api.repository.RecuperacaoSenhaRepository;
import com.ifnmg.ifreserva.api.repository.ReservaVeiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@EnableScheduling
public class ScheduledTasks {

    @Autowired
    private RecuperacaoSenhaRepository recuperacaoSenhaRepository;
    @Autowired
    private ReservaVeiculoRepository reservaVeiculoRepository;
    @Autowired
    private Mailer mailer;

    private static final String TIME_ZONE = "America/Sao_Paulo";

    @Scheduled(cron = "0 0 1 * * *", zone = TIME_ZONE)
    public void excluirRecuperacoesSenhaExpiradas(){
        System.out.println("Exclusão de recuperações de senha expiradas. Hora: " + new Date());
        recuperacaoSenhaRepository.removerRecuperacoesExpiradas();
    }

    @Scheduled(cron = "0 30 * * * *", zone = TIME_ZONE)
    @Scheduled(cron = "0 0 * * * *", zone = TIME_ZONE)
    public void expirarReservasVeiculo() {
        System.out.println("Expirando reservas." + new Date());
        List<ReservaVeiculo> reservasExpiradas = reservaVeiculoRepository.findReservasExpiradas();
        if (reservasExpiradas != null) {
            Status statusExpirado = new Status();
            statusExpirado.setId(6l);
            for (ReservaVeiculo reserva : reservasExpiradas) {
                reserva.setStatus(statusExpirado);
                reservaVeiculoRepository.save(reserva);
                mailer.expiracaoReserva(reserva);
            }
        }
    }
}
