package com.ifnmg.ifreserva.api.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public Date addHours(Date date, int hours)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, hours);
        return cal.getTime();
    }
}
