package com.ifnmg.ifreserva.api.utils;

import com.ifnmg.ifreserva.api.model.RecuperacaoSenha;
import com.ifnmg.ifreserva.api.model.Usuario;
import com.ifnmg.ifreserva.api.sendgrid.TemplateSendgridEnum;
import com.ifnmg.ifreserva.api.service.EmailService;

import java.util.HashMap;
import java.util.Map;

public class EmailUtils {

    public void enviarEmailBoasVindas(Usuario usuario) {
        EmailService emailService = new EmailService();
        Map<String, String> templateParams = new HashMap<>();
        templateParams.put("-login-", usuario.getLogin());
        templateParams.put("-senha-", usuario.getSenha());

        emailService.EnviarEmail(usuario.getNome(), usuario.getEmail(), "Boas vindas - IF Reserva", templateParams, TemplateSendgridEnum.BOAS_VINDAS.getTemplateId());
    }

    public void enviarLinkParaAlteracaoDeSenha(RecuperacaoSenha recuperacaoSenha) {
        EmailService emailService = new EmailService();
        Map<String, String> templateParams = new HashMap<>();
        templateParams.put("-token-", recuperacaoSenha.getToken());
        emailService.EnviarEmail(recuperacaoSenha.getUsuario().getNome(), recuperacaoSenha.getUsuario().getEmail(), "Alteração de senha - IF Reserva", templateParams, TemplateSendgridEnum.RECUPERAR_SENHA.getTemplateId());
    }
}
