package com.ifnmg.ifreserva.api.dto;

import java.time.LocalDateTime;

public class RelatorioMinhasReservasVeiculo {

    private String diaInicio;
    private String diaTermino;
    private String veiculo;
    private String saida;
    private String destino;
    private String motivo;
    private String status;
    private LocalDateTime start;

    public String getDiaInicio() {
        return diaInicio;
    }

    public void setDiaInicio(String diaInicio) {
        this.diaInicio = diaInicio;
    }

    public String getDiaTermino() {
        return diaTermino;
    }

    public void setDiaTermino(String diaTermino) {
        this.diaTermino = diaTermino;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getSaida() {
        return saida;
    }

    public void setSaida(String saida) {
        this.saida = saida;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }
}
