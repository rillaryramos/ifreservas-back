package com.ifnmg.ifreserva.api.dto;

import java.time.LocalDateTime;

public class RelatorioReservasVeiculoAprovadas {

    private String diaInicio;
    private String diaTermino;
    private String veiculo;
    private String servidor;
    private String motorista;
    private LocalDateTime start;

    public String getDiaInicio() {
        return diaInicio;
    }

    public void setDiaInicio(String diaInicio) {
        this.diaInicio = diaInicio;
    }

    public String getDiaTermino() {
        return diaTermino;
    }

    public void setDiaTermino(String diaTermino) {
        this.diaTermino = diaTermino;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getServidor() {
        return servidor;
    }

    public void setServidor(String servidor) {
        this.servidor = servidor;
    }

    public String getMotorista() {
        return motorista;
    }

    public void setMotorista(String motorista) {
        this.motorista = motorista;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }
}
