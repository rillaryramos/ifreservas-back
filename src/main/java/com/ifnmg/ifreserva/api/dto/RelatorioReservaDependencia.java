package com.ifnmg.ifreserva.api.dto;

import com.ifnmg.ifreserva.api.model.Dependencia;

import java.time.LocalDateTime;

public class RelatorioReservaDependencia {

    private String diaHorario;
    private String dependencia;
    private String utilizador;
    private String aula;
    private LocalDateTime start;

//    public RelatorioReservaDependencia(String diaHorario, String dependencia, String utilizador, String aula) {
//        this.diaHorario = diaHorario;
//        this.dependencia = dependencia;
//        this.utilizador = utilizador;
//        this.aula = aula;
//    }

    public String getDiaHorario() {
        return diaHorario;
    }

    public void setDiaHorario(String diaHorario) {
        this.diaHorario = diaHorario;
    }

    public String getDependencia() {
        return dependencia;
    }

    public void setDependencia(String dependencia) {
        this.dependencia = dependencia;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }
}
