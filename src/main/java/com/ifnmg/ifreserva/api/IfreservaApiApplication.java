package com.ifnmg.ifreserva.api;

import com.ifnmg.ifreserva.api.config.property.ReservasApiProperty;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(ReservasApiProperty.class)
public class IfreservaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IfreservaApiApplication.class, args);
	}

}
