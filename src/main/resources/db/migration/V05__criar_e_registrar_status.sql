CREATE TABLE status (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO status (nome) values ('ABERTA');
INSERT INTO status (nome) values ('EM APROVAÇÃO');
INSERT INTO status (nome) values ('APROVADA');
INSERT INTO status (nome) values ('REPROVADA');
INSERT INTO status (nome) values ('CANCELADA');
INSERT INTO status (nome) values ('EXPIRADA');