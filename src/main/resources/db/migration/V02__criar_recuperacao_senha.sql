CREATE TABLE recuperacao_senha (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    token VARCHAR(500) NOT NULL,
    data_expiracao DATETIME NOT NULL,
    id_usuario BIGINT(20) NOT NULL,
    CONSTRAINT fk_recuperacao_senha_id_usuario FOREIGN KEY(id_usuario)
    REFERENCES usuario(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;