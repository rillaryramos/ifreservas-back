CREATE TABLE horario_reserva (
     id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
     dia_semana VARCHAR(10) NOT NULL,
     horario_inicio TIME NOT NULL,
     horario_termino TIME NOT NULL,
     id_reserva_dependencia BIGINT(20) NOT NULL,
     CONSTRAINT fk_reserva_dependencia_id_reserva_dependencia FOREIGN KEY (id_reserva_dependencia) REFERENCES reserva_dependencia(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;