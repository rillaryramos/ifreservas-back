CREATE TABLE reserva_veiculo (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(25) NOT NULL,
    start DATETIME NOT NULL,
    end DATETIME NOT NULL,
    saida VARCHAR(70) NOT NULL,
    destino VARCHAR(70) NOT NULL,
    motivo VARCHAR(300) NOT NULL,
    id_veiculo BIGINT(20) NOT NULL,
    id_usuario BIGINT(20) NOT NULL,
    id_status BIGINT(20) NOT NULL,
    id_motorista BIGINT(20) NULL,
    motivo_rejeicao VARCHAR(300) NULL,
    data_envio_aprovacao DATETIME NULL,
    id_administrador_reserva BIGINT(20) NULL,
    CONSTRAINT fk_reserva_veiculo_id_veiculo FOREIGN KEY(id_veiculo)
    REFERENCES veiculo(ID),
    CONSTRAINT fk_reserva_veiculo_id_usuario FOREIGN KEY(id_usuario)
    REFERENCES usuario(ID),
    CONSTRAINT fk_reserva_veiculo_id_status FOREIGN KEY(id_status)
    REFERENCES status(ID),
    CONSTRAINT fk_reserva_veiculo_id_motorista FOREIGN KEY(id_motorista)
    REFERENCES motorista(ID),
    CONSTRAINT fk_reserva_veiculo_id_administrador_reserva FOREIGN KEY(id_administrador_reserva)
    REFERENCES usuario(ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;