CREATE TABLE bloco (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO bloco (nome) values ('Prédio do médio');
INSERT INTO bloco (nome) values ('Complexo do superior');