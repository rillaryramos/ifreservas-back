CREATE TABLE motorista (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(70) NOT NULL,
    sobrenome VARCHAR(70) NOT NULL,
    cpf VARCHAR(14) NOT NULL,
    ativo BIT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO motorista (nome,sobrenome,cpf,ativo) values ('Bernardo', 'Benício Vieira', '07672078828', 1);
INSERT INTO motorista (nome,sobrenome,cpf,ativo) values ('Julio Yago', 'André Fogaça', '06492749464', 1);