CREATE TABLE dependencia (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(100) NOT NULL,
    ativo BIT NOT NULL,
    id_bloco BIGINT(20) NULL,
    CONSTRAINT fk_dependencia_id_bloco FOREIGN KEY(id_bloco) REFERENCES bloco(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO dependencia (nome, ativo, id_bloco) values ('Sala 1', 1, 1);
INSERT INTO dependencia (nome, ativo, id_bloco) values ('Sala 2', 1, 1);
INSERT INTO dependencia (nome, ativo, id_bloco) values ('Sala 3', 1, 1);

INSERT INTO dependencia (nome, ativo, id_bloco) values ('Sala 1', 1, 2);
INSERT INTO dependencia (nome, ativo, id_bloco) values ('Sala 2', 1, 2);
INSERT INTO dependencia (nome, ativo, id_bloco) values ('Sala 3', 1, 2);