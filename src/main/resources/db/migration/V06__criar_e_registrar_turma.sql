CREATE TABLE turma (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(100) NOT NULL,
    ativo BIT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO turma (nome, ativo) VALUES("BSI I", 0);
INSERT INTO turma (nome, ativo) VALUES("BSI II", 1);
INSERT INTO turma (nome, ativo) VALUES("BSI III", 0);
INSERT INTO turma (nome, ativo) VALUES("BSI IV", 1);
INSERT INTO turma (nome, ativo) VALUES("BSI V", 0);
INSERT INTO turma (nome, ativo) VALUES("BSI VI", 1);
INSERT INTO turma (nome, ativo) VALUES("BSI VII", 0);
INSERT INTO turma (nome, ativo) VALUES("BSI VIII", 1);
INSERT INTO turma (nome, ativo) VALUES("ADM I", 1);
INSERT INTO turma (nome, ativo) VALUES("ADM II", 1);
INSERT INTO turma (nome, ativo) VALUES("ADM III", 1);
INSERT INTO turma (nome, ativo) VALUES("ADM IV", 1);
INSERT INTO turma (nome, ativo) VALUES("ADM V", 1);
INSERT INTO turma (nome, ativo) VALUES("ADM VI", 1);
INSERT INTO turma (nome, ativo) VALUES("ADM VII", 1);
INSERT INTO turma (nome, ativo) VALUES("ADM VIII", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA I", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA II", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA III", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA IV", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA V", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA VI", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA VII", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA VIII", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA IX", 1);
INSERT INTO turma (nome, ativo) VALUES("AGRONOMIA X", 1);
INSERT INTO turma (nome, ativo) VALUES("1º INFO I", 1);
INSERT INTO turma (nome, ativo) VALUES("1º INFO II", 1);
INSERT INTO turma (nome, ativo) VALUES("2º INFO I", 1);
INSERT INTO turma (nome, ativo) VALUES("2º INFO II", 1);
INSERT INTO turma (nome, ativo) VALUES("3º INFO I", 1);
INSERT INTO turma (nome, ativo) VALUES("3º INFO II", 1);
INSERT INTO turma (nome, ativo) VALUES("1º AGRO I", 1);
INSERT INTO turma (nome, ativo) VALUES("1º AGRO II", 1);
INSERT INTO turma (nome, ativo) VALUES("2º AGRO I", 1);
INSERT INTO turma (nome, ativo) VALUES("2º AGRO II", 1);
INSERT INTO turma (nome, ativo) VALUES("3º AGRO I", 1);
INSERT INTO turma (nome, ativo) VALUES("3º AGRO II", 1);
INSERT INTO turma (nome, ativo) VALUES("1º MEIO AMBIENTE I", 1);
INSERT INTO turma (nome, ativo) VALUES("1º MEIO AMBIENTE II", 1);
INSERT INTO turma (nome, ativo) VALUES("2º MEIO AMBIENTE I", 1);
INSERT INTO turma (nome, ativo) VALUES("2º MEIO AMBIENTE II", 1);
INSERT INTO turma (nome, ativo) VALUES("3º MEIO AMBIENTE I", 1);
INSERT INTO turma (nome, ativo) VALUES("3º MEIO AMBIENTE II", 1);