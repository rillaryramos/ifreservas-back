CREATE TABLE veiculo (
     id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
     modelo VARCHAR(100) NOT NULL,
     marca VARCHAR(100) NOT NULL,
     placa VARCHAR(10) NOT NULL,
     observacao VARCHAR(250),
     ativo BIT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO veiculo (modelo,marca,placa,observacao,ativo) values ('Voyage', 'Volkswagen', 'WDR-5841', NULL, 1);
INSERT INTO veiculo (modelo,marca,placa,observacao,ativo) values ('Ônibus', 'Marcopolo', 'JFF-7015', NULL, 1);