CREATE TABLE reserva_dependencia (
     id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
     id_dependencia BIGINT(20) NOT NULL,
     id_usuario_solicitante BIGINT(20) NOT NULL,
     id_usuario_utilizador BIGINT(20) NULL,
     start DATETIME NOT NULL,
     end DATETIME NOT NULL,
     observacao VARCHAR(250),
     uso_continuo BIT NOT NULL,
     id_turma BIGINT(20),
     aula BIT NOT NULL,
     title VARCHAR(25) NOT NULL,
     id_status BIGINT(20) NOT NULL,
     CONSTRAINT fk_reserva_dependencia_id_dependencia FOREIGN KEY(id_dependencia)
     REFERENCES dependencia(ID),
     CONSTRAINT fk_reserva_dependencia_id_usuario_solicitante FOREIGN KEY(id_usuario_solicitante)
     REFERENCES usuario(ID),
     CONSTRAINT fk_reserva_dependencia_id_usuario_utilizador FOREIGN KEY(id_usuario_utilizador)
     REFERENCES usuario(ID),
     CONSTRAINT fk_reserva_dependencia_id_turma FOREIGN KEY(id_turma) REFERENCES turma(ID),
     CONSTRAINT fk_reserva_dependencia_id_status FOREIGN KEY(id_status) REFERENCES status(ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;