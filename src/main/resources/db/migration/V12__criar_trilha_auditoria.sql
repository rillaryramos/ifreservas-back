CREATE TABLE trilha_auditoria (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    descricao VARCHAR(500) NOT NULL,
    categoria VARCHAR(100) NOT NULL,
    tipo VARCHAR(100),
    `data` DATETIME NOT NULL,
    id_entidade BIGINT(20) NOT NULL,
    id_usuario BIGINT(20) NOT NULL,
    CONSTRAINT fk_log_reserva_dependencia_id_usuario FOREIGN KEY(id_usuario)
    REFERENCES usuario(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;